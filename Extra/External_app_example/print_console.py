import socketio

sio = socketio.Client()

@sio.event
def connect():
    print('connection established')

@sio.event
def microgesture(data):
    # This function is called when a event of type 'microgesture' is received
    name = data['name']

    if name == "TAP":
        print("tap detected")
    elif name == "DOUBLE_TAP":
        print("double_tap detected")
    else:
        print("unknown gesture detected : ", name)

@sio.event
def disconnect():
    print('disconnected from server')

while not sio.connected:
    try:
        sio.connect('http://localhost:9994')
    except socketio.exceptions.ConnectionError:
        pass
sio.wait()
