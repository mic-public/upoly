const { app, BrowserWindow, ipcMain } = require('electron');
const path = require('path');
const fs = require('fs');

function createWindow() {
    // Create the browser window
    const win = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            preload: path.join(__dirname, 'preload.js'), // Ajoute cette ligne
            contextIsolation: true, // Active la séparation entre le contexte de Node.js et celui du front-end
            enableRemoteModule: false // Désactive l'accès au module Remote pour plus de sécurité
        }
    });

    // Load the HTML file
    win.loadFile('spotifu/index.html');

    //Scan folder
    // Scan the songs folder when the app is ready
    ipcMain.handle('get-songs', () => {
        const songsDir = path.join(__dirname, 'spotifu/songs');
        const files = fs.readdirSync(songsDir).filter(file => file.endsWith('.mp3'));

        return files.map(file => {
            // Format the song info from the filename
            const [band, song] = file.replace('.mp3', '').split(' - ');
            return { song, band, file };
        });
    });
}

// When Electron has finished initializing
app.whenReady().then(() => {
    createWindow();

    // Quit when all windows are closed
    app.on('window-all-closed', () => {
        if (process.platform !== 'darwin') {
            app.quit();
        }
    });
});

// On macOS, re-create a window when the dock icon is clicked and no other windows are open
app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow();
    }
});