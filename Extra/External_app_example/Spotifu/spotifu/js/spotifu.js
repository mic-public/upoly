let songs = []

songs = songs
    .map(value => ({ value, sort: Math.random() }))
    .sort((a, b) => a.sort - b.sort)
    .map(({ value }) => value)

window.onload = async () => {
    // Récupère la liste des chansons depuis le dossier
    songs = await window.electronAPI.getSongs();

    // Shuffle and initialize the player as before
    songs = songs
        .map(value => ({ value, sort: Math.random() }))
        .sort((a, b) => a.sort - b.sort)
        .map(({ value }) => value);

    init();
};

function init() {
    let player = document.querySelector('#player');
    let audios = '';
    let playlist = '<div class="clipper">';
    for (let k = 0; k < songs.length; k++) {
        audios += `
            <audio s${k} src="songs/${songs[k].file}" hide></audio>
        `;
        playlist += `
            <sl-card variant="neutral" s${k}>
                <sl-button variant="default" size="large"
                    circle onclick="set_song(${k})">
                    <sl-icon name="play-fill" label="Play"></sl-icon>
                </sl-button>
                <span>${songs[k].song} - <strong>${songs[k].band}</strong></span>
            </sl-card>
        `;
    }
    playlist += '</div>';
    player.innerHTML += playlist;

    document.body.innerHTML += audios;

    set_song(0, false);
    setInterval(() => {
        update_ui();
    }, 250);
}



let current = 0;
let audio = null;
let card = null;

function set_song(k, start = true) {
    k = ((k + songs.length) % songs.length);
    current = k;
    if (audio !== null) {
        card.setAttribute('variant', 'neutral');
        stop_song();
    }
    audio = document.querySelector(`audio[s${k}]`);
    card = document.querySelector(`sl-card[s${k}]`);

    document.querySelector('#song').innerHTML = `
        ${songs[k].song} - <strong>${songs[k].band}</strong>
        (<span id="time">0:00 / 0:00</span>)
    `;

    card.setAttribute('variant', 'success');
    card.scrollIntoViewIfNeeded();

    if (start) { play_song(); }
}

function update_ui() {
    if (audio !== null) {
        if (audio.paused) {
            document.querySelector('sl-button[play]').classList.remove('hide');
            document.querySelector('sl-button[pause]').classList.add('hide');
        } else {
            document.querySelector('sl-button[play]').classList.add('hide');
            document.querySelector('sl-button[pause]').classList.remove('hide');
        }
        if (audio.volume > 0) {
            document.querySelector('sl-button[mute]').classList.remove('hide');
            document.querySelector('sl-button[unmute]').classList.add('hide');
        } else {
            document.querySelector('sl-button[mute]').classList.add('hide');
            document.querySelector('sl-button[unmute]').classList.remove('hide');
        }
        document.querySelector('sl-range.volume').value = 100 * audio.volume;
        document.querySelector('sl-range.track').value = 100 * audio.currentTime / audio.duration;

        document.querySelector('#time').innerHTML = `${format_time()} / ${format_time(audio.duration)}`;
    }
}

function play_song() {
    if (audio !== null) {
        audio.play();
    }
    update_ui();
}

function pause_song() {
    if (audio !== null) {
        audio.pause();
    }
    update_ui();
}

function toogle_play() {
    if (audio !== null && audio.paused) {
        play_song();
    } else {
        pause_song();
    }
}

function stop_song() {
    if (audio !== null) {
        audio.pause();
        audio.currentTime = 0;
    }
    update_ui();
}

function move_to_in_song(val, inseconds = false) {
    if (audio !== null) {
        if (inseconds) {
            let newval = audio.currentTime + val;
            if (newval < 0) { newval = 0; }
            if (newval > audio.duration) { newval = audio.currentTime; }
            audio.currentTime = newval;
        } else {
            audio.currentTime = val * audio.duration / 100;
        }
    }
    update_ui();
}

let master_vol = null;
function change_volume(val, relative = false) {
    let vol = audio.volume;
    if (relative) {
        vol += val / 100;
    } else {
        vol = val / 100;
    }
    audio.volume = (vol < 0 ? 0 : (vol > 1 ? 1 : vol));
    master_vol = null
    update_ui();
}

function unmute() {
    if (audio !== null && master_vol) {
        audio.volume = master_vol;
        master_vol = null;
    }
    update_ui();
}

function mute() {
    if (audio !== null) {
        master_vol = audio.volume;
        audio.volume = 0;
    }
    update_ui();
}

function toogle_mute() {
    if (audio !== null && audio.volume == 0) {
        unmute();
    } else {
        mute();
    }
}

function format_time(time = null) {
    if (time !== null) {
        return `${parseInt(time / 60)}:${`${parseInt(time) % 60}`.padStart(2, '0')}`;
    }
    if (audio === null) {
        return '0:00';
    }
    return `${parseInt(audio.currentTime / 60)}:${`${parseInt(audio.currentTime) % 60}`.padStart(2, '0')}`;
}

function init() {
    let player = document.querySelector('#player');
    let audios = '';
    let playlist = '<div class="clipper">';
    for (let k = 0; k < songs.length; k++) {
        audios += `
            <audio s${k} src="songs/${songs[k].file}" hide></audio>
        `;
        playlist += `
            <sl-card variant="neutral" s${k}>
                <sl-button variant="default" size="large"
                    circle onclick="set_song(${k})">
                    <sl-icon name="play-fill" label="Play"></sl-icon>
                </sl-button>
                <span>${songs[k].song} - <strong>${songs[k].band}</strong></span>
            </sl-card>
        `;
    }
    playlist += '</div>';
    player.innerHTML += playlist;

    document.body.innerHTML += audios;

    document.querySelector('sl-range.track').addEventListener('sl-input', (evt) => {
        move_to_in_song(evt.target.value);
    })
    document.querySelector('sl-range.volume').addEventListener('sl-input', (evt) => {
        change_volume(evt.target.value);
    })
    document.querySelector('sl-range.track').tooltipFormatter = (value) => {
        return format_time();
    };

    set_song(0, false);
    setInterval(() => {
        update_ui();
    }, 250);
}

window.onload = async () => {
    // Récupère la liste des chansons depuis le dossier
    songs = await window.electronAPI.getSongs();

    // Shuffle and initialize the player as before
    songs = songs
        .map(value => ({ value, sort: Math.random() }))
        .sort((a, b) => a.sort - b.sort)
        .map(({ value }) => value);
    init();

    window.socket.on("microgesture", (data) => {
        let fun = data.name.toLowerCase();
        if (fun == 'toogle_play') {
            toogle_play();
        } else if (fun == 'play') {
            play_song();
        } else if (fun == 'pause') {
            pause_song();
        } else if (fun == 'stop') {
            stop_song();
        } else if (fun == 'next') {
            set_song(current + 1, !audio.paused);
        } else if (fun == 'prev') {
            set_song(current - 1, !audio.paused);
        } else if (fun == 'mute') {
            mute();
        } else if (fun == 'unmute') {
            unmute();
        } else if (fun == 'toogle_mute') {
            toogle_mute();
        } else if (fun.indexOf('volume_up_') !== -1) {
            let k = parseInt(fun.split('_')[2]);
            change_volume(k, true);
        } else if (fun.indexOf('volume_down_') !== -1) {
            let k = parseInt(fun.split('_')[2]);
            change_volume(-k, true);
        } else if (fun.indexOf('song_fwd_') !== -1) {
            let k = parseInt(fun.split('_')[2]);
            move_to_in_song(k, true);
        } else if (fun.indexOf('song_bck_') !== -1) {
            let k = parseInt(fun.split('_')[2]);
            move_to_in_song(-k, true);
        } else if (fun.indexOf('volume_') !== -1) {
            let k = parseInt(fun.split('_')[1]);
            change_volume(k, false);
        } else if (fun.indexOf('song_move_') !== -1) {
            let k = parseInt(fun.split('_')[2]);
            move_to_in_song(k, false)
        }
    });

    window.socket.on("connect", (data) => {
        console.log(data);
    });
    window.socket.on("disconnect", (data) => {
        console.log(data);
    });
};

function tryme(msg) {
    window.socket.emit('tryme', msg);
}