# SPOTIFU

Music player from local files, inspired by Spotify

## 1. Installation
``` npm install ```

## 2. Copy your music
 
 Copy your favorite mp3 files intro ```spotifu/songs``` <br>
 mp3 files should be names as "BandName - Song .mp3"<br>
 example : "Queen - I Want To Break Free.mp3"

 ## 3. Start Application ( without compilation)
 ``` npm run start ```

## 4. Create a exectuable application
``` npm run start-forge ```<br>
``` npm run package ```<br>
``` npm run make ```<br>

Application is located in "out" folder


