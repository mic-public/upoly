
import socketio, requests
from requests.auth import HTTPBasicAuth


#You need to configure VLC first
#1. Open VLC
#2. Tools -> Preferences -> Activate HTTP server with password "upoly" ( line 17)
#3. Close VLC and open config file vlcrc. 
# File location : %APPDATA%\vlc\vlcrc (Windows)    | ~/Library/Preferences/org.videolan.vlc/vlcrc ( Mac os)
# 4. Uncomment the setting http-port in vlcrc file ,and set it to 9090 (line 35)
# 5. Restart VLC and uPoly application
# 6. In uPoly application, create a Generic Subscriber, port on 9955, and subscribe to microgesture name
# 7. Connection status will change to Connected after running this python script.

sio = socketio.Client()

@sio.event
def connect():
    print('connection established')

@sio.event
def microgesture(data):
    auth = HTTPBasicAuth('', 'upoly')
    name = data['name']
    if name == "TAP":
        print("tap detected")
        requests.get(vlc('pl_play'), auth=auth)
    elif name == "DOUBLE_TAP":
        print("double_tap detected")
        requests.get(vlc('pl_pause'), auth=auth)
    else:
        print("unknown gesture detected : ", name)


def vlc(cmd):
  vlc_url = 'http://127.0.0.1:9090/requests/status.xml'
  return '{}?command={}'.format(vlc_url,cmd)


@sio.event
def disconnect():
    print('disconnected from server')

while not sio.connected:
    try:
        sio.connect('http://localhost:9995')
    except socketio.exceptions.ConnectionError:
        pass
sio.wait()