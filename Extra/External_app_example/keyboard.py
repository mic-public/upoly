# Install Dependencies:
# pip install "python-socketio"
# pip install keyboard

# Start
# python keyboard.py

#Inside uPoly application
# 1. Create Generic Subscriber
# 2. Set Port to 9994
# 3. Subscribe to microgesture name
# 4. Link these name to your python code
import socketio, keyboard

sio = socketio.Client()

@sio.event
def connect():
    print('connection established')

@sio.event
def microgesture(data):
    # "microgesture" websocket event
    name = data['name']
    timestamp = data['timestamp']

    if name == "TAP":
        print("tap detected")
        keyboard.press_and_release('space')
    elif name == "DOUBLE_TAP":
        print("double_tap detected")
        keyboard.press_and_release('ctrl+s')
    else:
        print("unknown gesture detected : ", name)

@sio.event
def disconnect():
    print('disconnected from server')

while not sio.connected:
    try:
        sio.connect('http://localhost:9994')
    except socketio.exceptions.ConnectionError:
        pass
sio.wait()
