class RawDataEvent {
  // Here is the "real" definition of what is microglyphe
  constructor() {
    this.ACTION = {
      FLEXION: "Flexion",
      EXTENSION: "Extension",
      ADDUCTION: "Adduction",
      ABDUCTION: "Abduction",
      CIRCLES: "Circles",
      SQUARES: "Squares",
      TRIANGLES: "Triangles",
      ZIGZAG: "Zigzag",
      NOMOVEMENT: "no movement",
      ANY: "any"
    }

    this.CONTEXT = {
      AIR: "Air",
      CONTACT: "Contact",
    }

    this.HAND = {
      LEFT: "LeftHand",
      RIGHT: "RightHand",
      ANY: "any"
    };

    this.FINGER = {
      INDEX: "index",
      MIDDLE: "middle",
      RING: "ring",
      PINKY: "pinky",
      THUMB: "thumb",
      ANY: "any"
    };

    this.PHALANX = {
      SEGMENT: {
        TIP: "tip",
        MIDDLE: "middle",
        BASE: "base",
        ANY: "any"
      },
      SIDE: {
        FRONT: "front",
        DORSAL: "dorsal",
        RIGHT: "right",
        LEFT: "left",
        ANY: "any"
      }
    };

    this.CONTACT = {
      ACTION: {
        FINGER: "Contact",
        OBJECT: "Object",
        AIR: "Air",
        PLUS: "Plus",
      }
    }


    this.deviceId = "";
    this.action = "this.ACTION.ANY";     // this.ACTION.FLEXION
    this.context = [];   // 1 value means to deplacement, 2 values means deplacement
    this.actuator = [[this.FINGER.ANY]]; // This an array of array of ONE unique value ( in order to match microglyph format). There is no OR, or AND here. 
    this.phalanx = [];       // THis an array of array of ONE unique object
    this.hand = this.HAND.ANY;
    this.parameters = { // maybe this is an array
      pressure: {
        "start": null,
        "end": null,
      },
      amplitude: {
        "start": null,
        "end": null,
      },
      time: {
        "duration": null,
      }
    };
    this.contact = {
      action: null,
      parameters: null,
      actuator: [],
      phalanx: [],
    }

    this.timestamp = Date.now();




  }

  //Static method to create rawdataevent from glyph
  static fromGlyph(glyphObject) {

    /*
    This is an example of microgesture
    microgesture: {
            type: "glyph",
            action: "Abduction",
            context: ["Contact", "Contact"],
            actuator: [["index"]],
            contact: {
              action: "Contact",
              actuator: [["index"]],
            },
          },
    */

    //check that type is glyph
    if (glyphObject.type != "glyph") {
      console.log("ERROR : glyphObject.type is not glyph");
      return;
    }


    let rawdataevent = new RawDataEvent();

    // Make a loop all all the keys of the glyphObject, and fill rawdataevent with the corresponding value
    //console.log("converting glyph to rawdataevent")
    Object.keys(glyphObject).forEach((key) => {
      //console.log(key);
      //if the key of glyphObject exist in rawdataevent, copy the value
      if (rawdataevent.hasOwnProperty(key)) {
        //everything but not "type" key
        if (key != "type") {
          rawdataevent[key] = glyphObject[key];
        }
      }
    });


    return rawdataevent;
  }


  static cleanRawDataEvent(rawDataEvent) {

    //remove all the keys that are uppercase FINGERS, ACTION ...etc ...

    Object.keys(rawDataEvent).forEach((key) => {
      if (key == key.toUpperCase()) {
        delete rawDataEvent[key];
      }
    }
    );

    //dont need to return because it is a reference

  }


}




module.exports = RawDataEvent;
