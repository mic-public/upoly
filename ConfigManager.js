
const Controller = require("./Controller/Controller.js");
const fs = require("fs");

const path = require('path');

const appPath =
  process.env.NODE_ENV === 'production'
    ? `${process.resourcesPath}/app`
    : __dirname;


//this class allow the open and save configuration from a json file.
// this class is in the charge to liste all possible configuration, open, save, save as, configuration.

/*
the configuration file is a json file with 3 parts : devices, analyzers, and subscribers
{
  "devices": [
    {
      "name": "teensy LIG 3.2",
      "type": "RawHID_test"
    }
  ],
  "analyzers": [
    {
      "type": "MeanAnalyser_test"
    }
  ],
  "subscribers": [
    {
      "name": "Unity VR APP",
      "desiredGesture": ["swipe", "touch"],
      "port": 12345
    }
  ]
}

*/

class ConfigManager {
  constructor(dm, am, gsm) {

    this.controller = Controller.getInstance();
    this.dm = dm; //device manager
    this.am = am; //analyzer manager
    this.gsm = gsm; //gesture subscriber manager


    const defautConfigName = "config.json";

    // Configuration file
    //1st check that config.json exist, if not, check is config_template.json exist.
    //if config_template.json exist, copy it to config.json


    if (!fs.existsSync(path.join(appPath, 'Config', defautConfigName))) {
      if (fs.existsSync(path.join(appPath, 'Config', 'config_template.json'))) {
        fs.copyFileSync(path.join(appPath, 'Config', 'config_template.json'), path.join(appPath, 'Config', defautConfigName));
      }
    }

    this.actualConfig = JSON.parse(fs.readFileSync(path.join(appPath, 'Config', defautConfigName)));
    this.actualConfigName = defautConfigName;

    //open defaut config at start
    this.openConfig(this.actualConfig);

    console.log("actual config name  : " + this.actualConfigName);

    //create empty list of config
    this.listOfConfig = [];

  }

  openConfigFromFile(fileName) {



    let configFile = JSON.parse(fs.readFileSync(path.join(appPath, 'Config', fileName)));
    this.openConfig(configFile);
    this.actualConfigName = fileName;
    this.controller.updateConfigName(this.actualConfigName);
  }

  openConfig(configFile) {

    console.log("open config from object : " + configFile);


    this.dm.configFromFile(configFile); //this should be delay until the list of port is available
    this.am.configFromFile(configFile);
    this.gsm.configFromFile(configFile);

    //update the controller
    this.controller.updateAnalyzersUIState();
    this.controller.updateDeviceUIState();
    this.controller.updateSubscriberUIState();



  }

  updateListOfConfig() {
    this.listOfConfig = fs.readdirSync(path.join(appPath, 'Config'));
    //console.log(this.listOfConfig);
    //remove all hidden files starting by .
    this.listOfConfig = this.listOfConfig.filter((file) => {
      return !file.startsWith(".");
    });
    // filter only file with .json extension
    this.listOfConfig = this.listOfConfig.filter((file) => {
      return file.endsWith(".json");
    });

    return this.listOfConfig;
  }

  saveConfig() {

    this.actualConfig.devices = this.dm.exportConfig();
    this.actualConfig.analyzers = this.am.exportConfig();
    this.actualConfig.subscribers = this.gsm.exportConfig();

  }

  saveConfigToFile(fileName) {
    //export the this.actualConfig to a json file, inside the Config folder, which name is fileName

    //check if the file name is not empty
    if (fileName.length > 0) {
      //check if the file name is not already used
      //save the file
      let finalFileName = path.join(appPath, 'Config', fileName)

      fs.writeFileSync(finalFileName, JSON.stringify(this.actualConfig));
      console.log("save config to file : " + finalFileName);
      //update the list of config
      this.updateListOfConfig();

    }

  }

  saveActualConfig() {

    //console.log("save actual config");
    this.saveConfig();
    this.saveConfigToFile(this.actualConfigName);
  }




}


module.exports = ConfigManager;