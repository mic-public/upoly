/*
This UI class is more a Controller than a View.
It is the bridge between the UI and the other classes.
It is able to operate any operation on the server, and the websocket message to the Web Page made with Vue.js

*/

//change import to require
const express = require("express");
const http = require("http");
const { createServer } = require("http");
const { Server } = require("socket.io");
const path = require("path");
const { fileURLToPath } = require("url");
const fs = require('fs');
const e = require("express");


class Controller {
  constructor(m) {
    //maybe add socket port as parameter
    const __dirname = path.join(path.dirname(__filename), '../');

    this.app = express();
    this.server = http.Server(this.app);
    this.io = new Server(3000, {
      cors: {
        origin: "http://localhost:8080", // be careful : 127.0.0.1 is reporting error
      },
    });

    this.mode = m;

    if (this.mode == "prod") {
      this.port = 8080; //disable server during dev
      const indexPath = path.join(__dirname, 'Gui', 'dist', 'index.html');
      console.log(indexPath)
      if (!fs.existsSync(indexPath)) {
        throw new Error('GUI is not build. Please run "npm run buildgui" before starting the server.');
      } else {
        this.app.use(
          express.static(__dirname + "/Gui/dist/")
        );
      }



    }

    if (this.mode == "dev") {
      this.port = 3000; //disable server during dev
      console.log("UI.js : Development mode");
    }

    this.io.on("connection", (socket) => {
      console.log("UI.js a user connected");
      this.socket = socket;
      //console.log(socket.id);

      //--------- RECEIVE FROM DEVICE -------

      this.socket.on("devices/refresh", () => {
        this.updateDeviceUIState();
      });

      this.socket.on("devices/updateportlist", () => {
        console.log("User ask to update port list");
        this.socket.emit("devices/portlist", this.dm.getAvailablePorts());
      });

      //On message received from UI /devices/delete
      this.socket.on("devices/delete", (data) => {
        //call deviceManager deleteDevice with the index of the device
        console.log("User ask to delete device " + data.index);
        this.dm.deleteDevice(data.index);
        this.updateDeviceUIState();
      });

      //On message received from UI /devices/add
      this.socket.on("devices/add", (data) => {
        // Data structure 
        // data = {type: "CommunicationProtocol", name: "NameOfTheDevice", port: "COM3"}
        //call deviceManager addDevice with the name of the device

        console.log("User ask to add device " + data.name);
        if (data.port) {
          this.dm.addDevice(data.type, data.name, data.port);
        } else {
          this.dm.addDevice(data.type, data.name);
        }

        this.updateDeviceUIState();
      });

      this.socket.on("devices/updateport", (data) => {
        if (data.port) {
          this.dm.updateDevicePort(data.index, data.port);
        }
        this.updateDeviceUIState();
      });

      this.socket.on("devices/manualtrigger", (data) => {
        this.dm.manualTriggerFromGlyph(data) // data contain the name of the device
      });

      this.socket.on("devices/restartconnection", (data) => {
        console.log("User ask to restart connection " + data.index);
        this.dm.restartConnection(data.index);
      });


      //----------- RECEIVE FROM ANALYZERS -------

      this.socket.on("analyzers/refresh", () => {
        this.updateAnalyzersUIState();
      });

      this.socket.on("analyzers/add", (data) => {
        //call deviceManager addDevice with the name of the device
        console.log("User ask to add analyzer " + data.name);
        this.am.addAnalyzer(data.type, data.name, data.microgesture);
        let lastindex = this.am.analyzersList.length - 1;
        let lastAnalyzer = this.am.analyzersList[lastindex];
        // at this point, last analyzer microgesture is empty
        this.addAnalyzersUI(
          lastAnalyzer.name,
          lastAnalyzer.index,
          lastAnalyzer.microgesture,
          lastAnalyzer.autoReload,
          true
        );
        this.sm.updateGestureList(this.am.getListOfAnalyzersName());

      });

      this.socket.on("analyzers/askPreview", (data) => {
        console.log("User ask preview from Library " + data.filename);
        this.am.askPreviewFromLibrary(data.filename);
      });

      this.socket.on("analyzers/editor/askPreview", (data) => {
        console.log("User Editor ask preview from Library " + data.filename);
        this.am.askPreviewFromLibrarySpecificEditor(data.index, data.filename);
      });

      this.socket.on("analyzers/addFromLibrary", (data) => {
        console.log("User ask to add analyzer from Library " + data.filename);
        this.am.addAnalyzerFromLibrary(data.filename);
        this.updateAnalyzersUIState();
        this.sm.updateGestureList(this.am.getListOfAnalyzersName());
      });

      this.socket.on("analyzers/delete", (data) => {
        this.am.removeAnalyzer(data.index);
        this.updateAnalyzersUIState();
        this.sm.updateGestureList(this.am.getListOfAnalyzersName());

      });

      this.socket.on("analyzers/restart", (data) => {
        console.log("User ask to restart analyzer " + data.index);
        this.am.restartSpecificAnalyzer(data.index);
      });

      this.socket.on("analyzers/stop", (data) => {
        console.log("User ask to stop analyzer " + data.index);
        this.am.stopSpecificAnalyzer(data.index);
      });


      this.socket.on("analyzers/update", (data) => {
        //console.log("User ask to update analyzer " + data.index);
        this.am.updateAnalyzerMicogesture(data.index, data.microgesture);
        this.am.updateAnalyzerName(data.index, data.name);
        this.updateSingleAnalyzerGesture(data.index, data.name, this.am.analyzersList[data.index].microgesture);
        this.sm.updateGestureList(this.am.getListOfAnalyzersName());

      });

      this.socket.on("analyzers/addvariable", (data) => {
        this.am.addVariableToAnalyzer(data.index);
        this.updateAnalyzersUIState();
      });

      this.socket.on("analyzers/manualtrigger", (data) => {
        console.log("UI : User ask to manual trigger analyzer " + data.name);
        this.am.manualTrigger(data.name) // data contain the name of the analyzer
      });

      // socket on : autoreload
      this.socket.on("analyzers/autoreload", (data) => {
        console.log("User ask to autoreload analyzer " + data.index);
        this.am.setAnalyzerAutoReload(data.index, data.state);
      });




      this.socket.on("analyzers/editor/askgesturelibrary", (data) => {
        //console.log("User ask to autoreload analyzer " + data.index);
        this.updateLibraryToEditor(data.index);
      });

      //-------------- RECEIVE FROM SUBSCRIBERS ------------
      this.socket.on("subscribers/refresh", () => {
        console.log("User ask to refresh subscriber view");
        this.updateSubscriberUIState();
      });

      this.socket.on("subscribers/updategesturelist", () => {
        console.log("User ask to refresh gesture list");
        this.sm.updateGestureList(this.am.getListOfAnalyzersName());
      });

      this.socket.on("subscribers/add", (data) => {
        console.log("User ask to add subscriber type=" + data.type);
        this.sm.addSubscriber(data.name, data.type, {});
        this.updateSubscriberUIState();
      });

      this.socket.on("subscribers/edit", (data) => {
        console.log("User ask to edit subscriber " + data.index);
        this.sm.editSubscriber(data.index, data.name, data.config);
        this.updateSubscriberUIState();
      });

      this.socket.on("subscribers/delete", (data) => {
        console.log("User ask to delete subscriber " + data.index);
        this.sm.deleteSubscriber(data.index);
        this.updateSubscriberUIState();
      });

      //-------------- RECEIVE FROM CONFIG ------------
      this.socket.on("config/refreshlist", () => {
        console.log("User ask to refresh config list");
        let refreshed = this.cm.updateListOfConfig();
        this.updateConfigList(refreshed);
      });

      this.socket.on("config/save", () => {
        console.log("User ask to save");
        this.cm.saveActualConfig();
      });

      this.socket.on("config/saveas", (filename) => {
        console.log("User ask to save as : " + filename);
        this.cm.saveConfigToFile(filename);
      });

      this.socket.on("config/open", (filename) => {
        console.log("User ask to open : " + filename);
        this.cm.openConfigFromFile(filename);
      });



      //when user is connected, refresh the UI : No, let component ask for themself
      //this.updateDeviceUIState();
      //this.updateAnalyzersUIState();
      this.updateConfigName(this.cm.actualConfigName);

    });


    //add all manager instance
    this.dm = null;
    this.am = null;
    this.sm = null;
    this.cm = null;  // configuration manager

    //singleton, return the instance of UI
    if (!Controller.instance) {
      Controller.instance = this;
    }
    return Controller.instance;


  }



  /********************
   *  EMIT TO Devices
   ********************/
  resetDeviceUI() {
    if (this.socket) {
      this.socket.emit("devices/reset", {});
    }
  }

  updateDeviceUIState() {
    if (this.socket) {
      this.socket.emit("devices/reset", {});

      //read the actual state of all the devices, and send it to the UI
      this.dm.devicesList.forEach((device) => {
        this.addDeviceUI(
          device.constructor.protocol ? device.constructor.protocol : "none",
          device.name,
          device.indexDevice,
          device.description,
          device.isOpen,
          device.port
        );
      });
      this.socket.emit("devices/typelist", this.dm.getAvailableDevicesType());
    }
  }



  addDeviceUI(protocolType, n, index, d, o, p) {
    //check if socket is defined
    if (this.socket) {
      //emit a socket event with the deviceType, name and index
      this.socket.emit("devices/add", {
        name: n,
        index: index,
        protocol: protocolType,
        description: d,
        open: o,
        port: p ? p : "none"
      });
      console.log("addDeviceUI : " + protocolType + " " + n + " " + index + " " + " open" + o);
    } else {
      //emit socket message when socket is defined, and ready to operate.

      console.log("UI socket is not defined");
    }
  }

  updateDeviceRawDataEventUI(data, indexDevice) {
    // Data is juste the data part of the RawDataEvent
    //check is socket is defined
    if (this.socket) {
      let url = "devices/" + indexDevice + "/rawdataevent";
      //console.log("updateRawDataEventUI : " + url);
      this.socket.emit(url, data);
    } else {
      //emit socket message when socket is defined, and ready to operate.
      console.log("UI socket is not defined");
    }
  }

  //create new methode call updateChartUI
  updateDeviceChartUI(data, channel, indexDevice) {
    //check is socket is defined
    if (this.socket) {
      let url = "devices/" + indexDevice + "/chart";
      //console.log("updateChartUI : " + data + " " + url);
      this.socket.emit(url, { channel: channel, value: data });
    }
  }

  updateDeviceConsoleUI(data, index) {
    //check is socket is defined
    if (this.socket) {
      //console.log("updateConsoleUI : " + data);
      let url = "devices/" + index + "/console";
      this.socket.emit(url, data);
    }
  }
  //state means is Open or closed
  updateDeviceStateUI(state, index) {
    if (this.socket) {
      //console.log("updateConsoleUI : " + data);
      let url = "devices/" + index + "/state";
      this.socket.emit(url, state);
    }

  }

  /********************
   *  EMIT TO ANALYZERS
   ********************/

  // emit analyzerS means to all analyzers
  // emit analyzer means to a specific analyzer

  resetAnalyzersUI() {
    if (this.socket) {
      this.socket.emit("analyzers/reset", {});
    }
  }

  previewAnalyzerUI(microgesture) {
    if (this.socket) {
      console.log("previewAnalyzerUI : send it now");
      this.socket.emit("analyzers/preview", microgesture);
    }
  }

  previewAnalyzerUISpecificEditor(index, microgesture) {
    if (this.socket) {
      console.log("previewAnalyzerUI : send it now to editor : " + index);
      this.socket.emit("analyzer/" + String(index) + "/editor/previewLibrary", microgesture);
    }
  }

  updateAnalyzersUIState() {
    if (this.socket) {

      //This method is overkill, because it delete all and re-create all.
      //TODO : a a lighter method, that only updagte a specific analyzer
      this.socket.emit("analyzers/reset", {});

      //read the actual state of all the analyzers, and send it to the UI
      this.am.analyzersList.forEach((analyzer) => {
        this.addAnalyzersUI(
          analyzer.name,
          analyzer.index,
          analyzer.microgesture,
          analyzer.autoReload
        );
      });
      this.socket.emit("analyzers/gestureLibrary", this.am.gestureLibrary)
      //repeat this for anaylyzers and subscribers.
    }
  }

  updateLibraryToEditor(index) {
    if (this.socket) {
      console.log("updateLibraryToEditor : " + index);
      this.socket.emit("analyzer/" + index + "/editor/gestureLibrary", this.am.gestureLibrary);
    }
  }

  updateSingleAnalyzerGesture(index, name, gest) {
    if (this.socket) {
      this.socket.emit("analyzer/" + index + "/updateGesture", { name: name, microgesture: gest });
    }
  }

  updateSingleAnalyzerState(index, state) {
    if (this.socket) {
      this.socket.emit("analyzer/" + index + "/active", state);
    }
  }

  updateStartSingleAnalyzerDuration(index) {
    if (this.socket) {
      this.socket.emit("analyzer/" + index + "/startduration", "");
    }
  }

  addAnalyzersUI(name, index, micro, reload, directedit = false) {
    //check if socket is defined
    if (this.socket) {
      //emit a socket event with the deviceType, name and index
      this.socket.emit("analyzers/add", {
        name: name,
        index: index,
        microgesture: micro,
        autoreload: reload,
        directedit: directedit
      });
      console.log("addAnalyzerUI : " + name + " " + index);
    } else {
      //emit socket message when socket is defined, and ready to operate.
      console.log("UI socket is not defined");
    }
  }

  updateAnalyzersConsoleUI(data, index) {
    //check is socket is defined
    if (this.socket) {
      console.log("updateAnalysersUI : " + data);
      let url = "analyzers/" + index + "/console";
      this.socket.emit(url, data);
    }
  }

  updateAnalyzerGlyphActiveState(index, indextree, state) {
    //index is index of the analyzer
    //indextree is the position of the analyzer inside the tree. It is a string "1-2-3", not an array
    //state 0:inactive  1:listening  2:activated ( a match has been found)

    if (this.socket) {
      //replace any - in indextree into / and finish the string with a /
      let endofurl = indextree.replaceAll("-", "/");
      endofurl = endofurl + "/";

      let url = "analyzers/" + index + "/" + endofurl + "active";
      this.socket.emit(url, state);
      //console.log("updateAnalyzerActiveState : " + url + " " + state);
    }
  }

  updateAnalyzerGlyphProgress(index, indextree, progressvalue) {
    if (this.socket) {
      //replace any - in indextree into / and finish the string with a /
      let endofurl = indextree.replaceAll("-", "/");
      endofurl = endofurl + "/";

      let url = "analyzers/" + index + "/" + endofurl + "progress";
      this.socket.emit(url, progressvalue);
      //console.log("updateAnalyzerActiveState : " + url + " " + state);
    }
  }


  updateAnalyzerGlyphVariable(index, newListOfVariable) {

    if (this.socket) {
      let url = "analyzers/" + index + "/0/variables";
      this.socket.emit(url, newListOfVariable);
      //console.log("updateAnalyzerVariable : ");
    }

  }

  incomingRawDataEvent(rawDataEvent) {

    if (this.socket) {
      let url = "analyzers/incoming";
      this.socket.emit(url, rawDataEvent);
    }
  }

  /**********************
 *  EMITS TO SUBSCRIBERS
 ***********************/


  updateGestureListUI(list) {
    //this just an array of string
    if (this.socket) {
      this.socket.emit("subscribers/gesturelist", list);
    }
  }

  addSubscriberUI(object) {
    if (this.socket) {
      this.socket.emit("subscribers/add", object);
    }
  }

  resetSubscriberUI() {
    if (this.socket) {
      this.socket.emit("subscribers/reset", {});
    }

  }



  updateSubscriberUIState() {
    //Clear the entire Subscriber page and add subscriber one by one.
    // This method is not optimal and should be replaced by a methode socket.emit("subscribers/refreshall", list)
    if (this.socket) {
      this.socket.emit("subscribers/reset", {});
      console.log("updateSubscriberUIState");
      this.sm.gestureSubscriberList.forEach((sub) => {
        let object = { // TODO . Think about on other method, because this copy paste method is not optimal. Why not directly use JSON.stringify(sub) ?
          name: sub.name,
          index: sub.index,
          type: sub.type,
          config: sub.config,
          active: sub.active,
        };
        console.log(object)
        this.addSubscriberUI(object);
      });

      //repeat this for anaylyzers and subscribers.
    }
  }

  updateSpecificSubscriberOnGesture(subscriberIndex, gestureEvent) {
    if (this.socket) {
      this.socket.emit("subscriber/" + subscriberIndex + "/ongesture", { name: gestureEvent.name }); // this the specific subscriber page
      console.log(" UI gesture triggered : " + gestureEvent.name);
    }
  }

  updateSingleSubscriberConnection(index, connected) {
    if (this.socket) {
      this.socket.emit("subscriber/" + index + "/active", connected);
    }
  }

  notifyOnGestureEmit(gestureEvent) {
    if (this.socket) {
      this.socket.emit("analyzer/" + gestureEvent.analyzerIndex + "/sent", { name: gestureEvent.name }); // This is adding a v-chip "sent" on the analyzer page
      this.socket.emit("subscribers/ongesture", { name: gestureEvent.name }); // This is the main page of subscribers, that will display the event on v-snackbar on top
    }
  }


  /********************
   *  EMIT TO CONFIG
   ********************/

  updateConfigList(newlist) {
    if (this.socket) {
      this.socket.emit("config/list", newlist);
    }
  }

  updateConfigName(name) {
    if (this.socket) {
      this.socket.emit("config/name", name);
    }
  }


  /***********
   *  SINGLETON
   **********/

  //singleton, getInstance method
  static getInstance() {
    if (!Controller.instance) {
      Controller.instance = new Controller();
    }
    return Controller.instance;
  }
}

//export module
module.exports = Controller;
