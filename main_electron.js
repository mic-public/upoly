const electron = require('electron');
const path = require('path');

//Choose renderer in dev or production mode
/*
************************************************
CHANGE THIS VALUE BETWEEN DEV / PRODUCTION MODE
************************************************
*/
const webmode = "prod"; // dev or production mode



const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

function createWindow() {
    const win = new BrowserWindow({
        width: 1200,
        height: 800,
        titleBarStyle: 'visible',
        tranparerent: true,
        webPreferences: {
            //preload: path.join(__dirname, 'preload.js'),
            nodeIntegration: true,
            contextIsolation: false,
        },
        //change the window name
        title: "µPoly",
        //change the icon
        icon: path.join(__dirname, './Gui/src/assets/mic.svg')
    })

    if (webmode == "dev") {
        win.loadURL('http://localhost:8080')
    } else {
        win.loadFile(path.join(__dirname, './Gui/dist/index.html')).then(() => { win.show() });
    }


}

app.whenReady().then(() => {
    createWindow()

    app.on('activate', function () {
        if (BrowserWindow.getAllWindows().length === 0) createWindow()
    })
});

// The goal of this project is an server app, able to handle mutliple devices, from Arduino Gloves, to teensy HID, or even usb Camera .
// Theses devices can send raw data to the server.
// The server will analyse these data, using mutiple ways of analysing, then conclude that a result is detected.
// External app can suscribe to specific result, and will be notified when a result is detected.

//change all the import to require
const DeviceManager = require("./DeviceManager.js");
const AnalyzerManager = require("./AnalyzerManager.js");
const GestureSubscriberManager = require("./SubscriberManager.js");
const ConfigManager = require("./ConfigManager.js");
const Controller = require("Controller/Controller.js");

// Go back to master branch.
//Create UI object
const controller = new Controller("dev"); // dev or production mode

// Create instance Manager
const gsm = new GestureSubscriberManager(); // Instanciate external apps according to config file
const am = new AnalyzerManager(gsm); // Instanciate analyzers according to config file
const dm = new DeviceManager(am); // Instanciate devices according to config file
const cm = new ConfigManager(dm, am, gsm); // Instanciate config manager

// Controller have to know all the instance created
controller.dm = dm;
controller.am = am;
controller.sm = gsm;
controller.cm = cm;


