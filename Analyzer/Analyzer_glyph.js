/*

This is a subClass.
This is the most critical part of this entire software.
This class can compare one glyph from an event, to one glyph from a goal, and conclude the state of the current machine

*/

const RawDataEvent = require("../RawDataEvent.js");
const AnalyzerInterface = require("./AnalyzerInterface.js");
const Controller = require("../Controller/Controller.js");
const { raw } = require("express");


class AnalyzerGlyph extends AnalyzerInterface {
    //add a constructor without argument because GestureEventManager is a singleton
    constructor(i, m, position, parent, listOfVariable = {}) {
        super();
        this.index = i;
        this.position = position; // position inside index tree. [ 0 , 1, 2] for example.
        this.microgesture = m;
        this.parent = parent;
        this.controller = Controller.getInstance();

        // use of variable
        this.useVariableFingerOnActuator = undefined;
        this.useVariableFingerOnReceiver = undefined;
        this.useVariablePhalanxOnReceiver = undefined;
        this.variableFingerActuatorIndex = undefined;  // x1 is the index 0, x2 is the index 1, etc..
        this.variableFingerReceiverIndex = undefined;  // x1 is the index 0, x2 is the index 1, etc..
        this.variablePhalanxReceiverIndex = undefined;  // y1 is the index 0, y2 is the index 1, etc..


        this.variableAndStart = false;  // special case when variable is used, and the variable is defined with a AND operator. This is the start of the AND operator. One elememt has match, wait for the others.
        this.memoryOfAndOperator = []; // in the special case that variable containe a AND operator, validate some glyph and wait for sequence validation. This is the only moment when analyzeOnEvent can return 1

        this.parameterHistory = {
            name: undefined, // name of the parameter we are looking for, in case of parameter goal
            value: [], // array of value, in case of parameter goal
            goal: undefined, // length or startend     . This is two goal different.  Startend define two points, while length define a range to accomplish
            startgoal: undefined, // start or end
            endgoal: undefined, // start or end
            lengthgoal: undefined, // length goal
            progression: 0 // progression to the final result.
        }

        //What is really triggering the glyph. The goal is deciding what raw data event is ignored
        this.GOAL = {
            CONTEXT: "changeOfContext",
            PARAMETERS: "thresholdParameters",
            DURATION: "endOfDuration"
        };

        //Some constant
        this.DEFAULT_END_FLEXION = 70;
        this.DEFAULT_START_FLEXION = 0;
        this.THRESH_DISTANCE_GOAL = 5;
        this.DEFAULT_LENGTH_GOAL = 40; //Defaut value of the length of a swipe ( flexion or extension)

        this.goal = undefined;

        if (this.isEmpty()) {
            // DO NOT throw an error. Maybe glyph formula is not defined yet, and user is still editing it.
            console.log("Analyzer_glyph : constructor : microgesture is empty");
        } else {

            // Check if the formula is correct
            this.cleanFormula(this.microgesture);
            // Arbitrary decide what is the goal according to the glyph
            this.setGoalFromGlyph();
            // Check if there is use of variable
            this.setUseOfVariable(listOfVariable);

            this.timer = undefined; // use only when the goal is duration


            this.start();
        }

    }


    /*
    MAIN FUNCTION : GLYPH TO GLYPH COMPARISON
    */
    analyzeOnEvent(rawDataEvent, listOfVariable) {
        // this function return the state of glyph tracker, or a negative value that mean : IGNOERD
        let IGNORED = -1;  // ignored is not a state, but a value that return instead of state, when the event is ignored

        //FIRST OF ALL, check if the microgesture is empty
        if (this.goal == undefined) {
            this.setState(this.STATE.REJECTED);
            return this.state;
        }

        // CLEAN RAW DATA EVENT
        this.cleanFormula(rawDataEvent);


        //check if the deviceId match
        let isDeviceIdMatch = 1;
        if (this.microgesture.deviceId != undefined) {
            if (rawDataEvent.deviceId == undefined) {
                isDeviceIdMatch = 0;
            } else {
                if (this.microgesture.deviceId != rawDataEvent.deviceId) {
                    isDeviceIdMatch = 0;
                }
            }
        }
        // Ignore if the deviceId does not match
        if (!isDeviceIdMatch) {
            return IGNORED;
        }


        // Get an eye on actuators
        let isActuatorMatch = this.isActuatorMatch(rawDataEvent, listOfVariable);

        let isReceiverMatch = this.isReceiverMatch(rawDataEvent, listOfVariable);

        if (isActuatorMatch) {
            if (!isReceiverMatch) {

                this.setState(this.STATE.REJECTED);
                return this.state;
            }

        } else {
            return IGNORED;
        }

        let isContextMatch = this.isContextMatch(rawDataEvent);


        // At this point the actuator match, and the receiver match, and device ID match.
        switch (this.goal) {
            case this.GOAL.CONTEXT:
                if (isContextMatch) {
                    this.setState(this.STATE.ACCEPTED);
                    this.updateActuatorVariableFromEvent(rawDataEvent, listOfVariable); //update variable if necessary
                    this.updateReceiverFingerVariableFromEvent(rawDataEvent, listOfVariable);
                    this.updateReceiverPhalanxVariableFromEvent(rawDataEvent, listOfVariable);

                } else {
                    // IGNORED is the context match the "input" context. Contextually, authorise small movement in touch-touch before touch-release.
                    if (rawDataEvent.context.length == 1 && rawDataEvent.context[0] == this.microgesture.context[0]) {
                        return IGNORED;
                    } else if (rawDataEvent.context.length == 2 && rawDataEvent.context[0] == this.microgesture.context[0] && rawDataEvent.context[1] == this.microgesture.context[0]) {
                        return IGNORED;
                    } else {
                        // otherwise, it's REJECTED
                        this.setState(this.STATE.REJECTED);

                    }

                }

                break;
            case this.GOAL.PARAMETERS:
                // Check if the context match
                if (this.state != this.STATE.DONE) {

                    if (isContextMatch) {

                        if (this.isDirectionMatch(rawDataEvent)) {

                            //Update parameter, and update the state
                            this.updateParameterHistory(rawDataEvent);
                            if (this.state == this.STATE.ACCEPTED) {
                                this.updateActuatorVariableFromEvent(rawDataEvent, listOfVariable); //update variable if necessary
                            }

                        } else {
                            return IGNORED;   // why ignored. Maybe it is a noise of the sensor. // Impletement TOLERENCE mechanism. this should happen once.
                        }

                    }
                    // special case, when waiting for a context change, but there is still some small motion in the same context
                    else if (this.microgesture.context == 2 && rawDataEvent.context.length == 1 && rawDataEvent.context[0] == this.microgesture.context[0]) {
                        return IGNORED;
                    }
                    else {
                        // directly REJECTED
                        this.setState(this.STATE.REJECTED);
                    }


                } else {
                    // Here is the case where the state is DONE, the motion is already done, but there is still some motion.
                    if (isContextMatch) {
                        // cas tres particulier, dans lequel on est passé au glyph suivant, mais il n'y a pas match, on regarde alors si ce n'est pas un reste du mouvement qui n'est pas finit.
                        // on update les parametre meme si on sait que c'est DONE, et peut potentiellement sortir du done. 
                        this.updateParameterHistory(rawDataEvent);
                    } else {

                        this.setState(this.STATE.DONE);
                        return this.STATE.REJECTED;  // strange case, but indicate to the next event that it's not a match
                    }


                }


                break;
            case this.GOAL.DURATION:
                // Check if everything matches, including the context
                // if it doesn't match and the time is not up, it's REJECTED
                // and normally it will automatically change to ACCEPTED.
                if (!isContextMatch) {
                    this.setState(this.STATE.REJECTED);
                    //stop the timer
                } else {
                    // check if there is a match for the action
                }





        }

        // END OF ALGORITHM

        return this.state;
    }

    /*
    SETUP METHODS : CALLED IN THE CONSTRUCTOR
    */


    cleanFormula(microg) {
        //CLEAN ACTUATOR
        if (microg.actuator == undefined) {
            microg.actuator = [["any"]];
        } else {
            if (microg.actuator.length == 0) {
                microg.actuator = [["any"]];
            }
        }
        //CLEAN PHALANX
        if (microg.phalanx == undefined) {
            microg.phalanx = [[{ segment: "any", side: "any" }]];
        } else {
            if (microg.phalanx.length > 0) {
                if (microg.phalanx[0][0].segment == undefined || microg.phalanx[0][0].segment == "") {
                    microg.phalanx[0][0].segment = "any";
                }
                if (microg.phalanx[0][0].side == undefined || microg.phalanx[0][0].side == "") {
                    microg.phalanx[0][0].side = "any";
                }
            } else {
                //Empty array of phalanx
                microg.phalanx = [[{ segment: "any", side: "any" }]];
            }
        }

        // CLEAN CONTACT
        if (microg.contact != undefined) {
            //CLEAN CONTACT ACTUATOR
            if (microg.contact.actuator == undefined) {
                microg.contact.actuator = [["any"]];
            } else {
                if (microg.contact.actuator.length == 0) {
                    microg.contact.actuator = [["any"]];
                }
            }
            //CLEAN CONTACT PHALANX
            if (microg.contact.phalanx == undefined) {
                microg.contact.phalanx = [[{ segment: "any", side: "any" }]];
            } else {
                if (microg.contact.phalanx.length > 0) {
                    if (microg.contact.phalanx[0][0].segment == undefined || microg.contact.phalanx[0][0].segment == "") {
                        microg.contact.phalanx[0][0].segment = "any";
                    }
                    if (microg.contact.phalanx[0][0].side == undefined || microg.contact.phalanx[0][0].side == "") {
                        microg.contact.phalanx[0][0].side = "any";
                    }
                } else {
                    //Empty array of phalanx
                    microg.contact.phalanx = [[{ segment: "any", side: "any" }]];
                }
            }
        }

        return microg;
    }

    setUseOfVariable(listOfVariable) {
        //check if microgesture has an key called actuator
        // exemple : "actuator": [["x1"]], or [["thumb"]]
        // Adrien talk about Canonique. When Canonical format it true, [["thumb"],["index"]] is not valid. You have to use [["thumb", "index"]], with a AND operator. OR are allowed only with variable.
        // you have to defined a variable x1 = thumb or index, and then use [["x1"]]
        if (this.microgesture.actuator != undefined) {
            if (this.microgesture.actuator.length == 1) {
                if (this.microgesture.actuator[0].length == 1) {
                    this.useVariableFingerOnActuator = this.microgesture.actuator[0][0].startsWith("x");
                    if (this.useVariableFingerOnActuator) {
                        let variableName = this.microgesture.actuator[0][0];
                        //this.variableActuatorIndex = parseInt(variableName.substring(1)) - 1; // -1 because x1 is the index 0. 
                        // Find the index of the variable in the listOfVariable, where the name is equal to variableName
                        listOfVariable.filter((variable, index) => {
                            if (variable.name == variableName) {
                                this.variableFingerActuatorIndex = index;

                            }
                        });

                        //At this point, if this.variableActuatorIndex is still undefined, it means that the variable is not defined in the listOfVariable
                        if (this.variableFingerActuatorIndex == undefined) {
                            //throw an error, the variable is not defined in the listOfVariable
                            throw new Error("Variable is not defined in the listOfVariable");
                        }



                    } else {
                        let fingerName = this.microgesture.actuator[0][0];
                        // check that fingerName is a valid finger name
                    }
                }
            }

            if (this.useVariableFingerOnActuator == undefined) {
                //trow an error, something is wrong with the mask
                throw new Error("Mask actuator is badly defined.  Not canonical format ?");
            }

        }
        if (this.microgesture.contact != undefined) {
            if (this.microgesture.contact.actuator != undefined) {
                if (this.microgesture.contact.actuator.length == 1) {
                    if (this.microgesture.contact.actuator[0].length == 1) {
                        this.useVariableFingerOnReceiver = this.microgesture.contact.actuator[0][0].startsWith("x");
                        if (this.useVariableFingerOnReceiver) {
                            let variableName = this.microgesture.contact.actuator[0][0];
                            //Check if variable is a pattern x1, x2, or just x.
                            // if it's just x, force variableReceivedIndex to 0, otherwise, extract the number
                            listOfVariable.filter((variable, index) => {
                                if (variable.name == variableName) {
                                    this.variableFingerReceiverIndex = index;
                                }
                            });
                            if (this.variableFingerReceiverIndex == undefined) {
                                throw new Error("Variable Receiver is not defined in the listOfVariable");
                            }
                            //this.microgesture.contact.actuator[1] = variableName; //TODO useless
                        } else {
                            let fingerName = this.microgesture.contact.actuator[0][0];
                        }
                    }
                } else {
                    this.useVariableFingerOnReceiver = false;
                }
            } else {
                this.useVariableFingerOnReceiver = false;
            }

            if (this.useVariableFingerOnReceiver == undefined) {
                //trow an error, something is wrong with the mask
                throw new Error("Mask actuator is badly defined.  Not canonical format ?");
            }

        }
        if (this.microgesture.contact != undefined) {
            if (this.microgesture.contact.phalanx != undefined) {
                if (this.microgesture.contact.phalanx.length == 1) {
                    if (this.microgesture.contact.phalanx[0].length == 1) {
                        this.useVariablePhalanxOnReceiver = this.microgesture.contact.phalanx[0][0].segment.startsWith("y");
                        if (this.useVariablePhalanxOnReceiver) {
                            let variableName = this.microgesture.contact.phalanx[0][0].segment
                            //Check if variable is a pattern x1, x2, or just x.
                            // if it's just x, force variableReceivedIndex to 0, otherwise, extract the number
                            listOfVariable.filter((variable, index) => {
                                if (variable.name == variableName) {
                                    this.variablePhalanxReceiverIndex = index;
                                }
                            });
                            if (this.variablePhalanxReceiverIndex == undefined) {
                                throw new Error("Variable Phalanx Receiver is not defined in the listOfVariable");
                            }
                            //this.microgesture.contact.actuator[1] = variableName; //TODO useless
                        }
                    }
                } else {
                    this.useVariablePhalanxOnReceiver = false;
                }
            } else {
                this.useVariablePhalanxOnReceiver = false;
            }

            if (this.useVariablePhalanxOnReceiver == undefined) {
                //trow an error, something is wrong with the mask
                throw new Error("Mask actuator is badly defined.  Not canonical format ?");
            }

        }
    }


    setGoalFromGlyph() {

        if (this.isTargetContextChange()) {
            this.goal = this.GOAL.CONTEXT;
        } else {
            if (this.isTargetTimeLimited()) {
                this.goal = this.GOAL.DURATION;
            } else {
                // look for a parameter value inside this.microgesture.parameters
                this.goal = this.GOAL.PARAMETERS;

                if (this.microgesture.parameters?.pressure != undefined) {
                    this.parameterHistory.name = "pressure";
                    this.parameterHistory.goal = "startend";
                    this.parameterHistory.startgoal = this.microgesture.parameters.pressure.start;
                    this.parameterHistory.endgoal = this.microgesture.parameters.pressure.end;


                } else if (this.microgesture.parameters?.amplitude != undefined) {
                    this.parameterHistory.name = "amplitude";
                    if (this.microgesture.parameters.amplitude.start != undefined || this.microgesture.parameters.amplitude.end != undefined) {
                        this.parameterHistory.goal = "startend";
                        this.parameterHistory.startgoal = this.microgesture.parameters.amplitude.start;
                        this.parameterHistory.endgoal = this.microgesture.parameters.amplitude.end;
                    }
                    if (this.microgesture.parameters.amplitude.amplitude != undefined) {
                        this.parameterHistory.goal = "length";
                        this.parameterHistory.lengthgoal = this.microgesture.parameters.amplitude.amplitude;
                        console.log("Analyzer_glyph : setGoalFromGlyph : Parameter is defined as length");
                    }
                } else {
                    // This is case of flexion or extension, where the goal is to reach a certain value, a certain amplitude, but implicite value.
                    // Probably in this casem, microgesture.parameters does not exist
                    this.parameterHistory.name = "amplitude";
                    this.parameterHistory.goal = "length";
                    let uselessRDE = new RawDataEvent();
                    // This should be 2D array, but for now, it's just a value
                    switch (this.microgesture.action) {
                        case undefined:
                            throw new Error("Analyzer_glyph: setGoalFromGlyph : action is not defined or ");
                            break;
                        case uselessRDE.ACTION.FLEXION:
                            this.parameterHistory.lengthgoal = this.DEFAULT_LENGTH_GOAL;
                            break;
                        case uselessRDE.ACTION.EXTENSION:
                            this.parameterHistory.lengthgoal = this.DEFAULT_LENGTH_GOAL;
                            break;
                        default:
                            //throw error and stop the program
                            throw new Error("Analyzer_glyph : setGoalFromGlyph : action is not supported ");
                            break;

                    }

                }
            }

        }

    }

    /*
    UTILITARY METHODS
    */

    isEmpty() {
        //count the number of key in microgesture object
        // Context must be defined, and its absence should  considered itself as empty
        //Globally some parameters should be marked as mandatory.
        // There is already the type key = glyph, so it's not empty
        let count = 0;
        for (let key in this.microgesture) {
            count++;
        }
        return count < 2;
    }

    isTargetContextChange() {
        let result = false;
        if (this.microgesture.context != undefined) {
            if (this.microgesture.context.length == 2) {
                if (this.microgesture.context[0] != this.microgesture.context[1]) {
                    result = true;
                }

            }
        }
        return result;
    }

    isInputContextChange(rawDataEvent) {
        let result = false;
        if (rawDataEvent.context != undefined) {
            if (rawDataEvent.context.length == 2) {
                if (rawDataEvent.context[0] != rawDataEvent.context[1]) {
                    result = true;
                }

            }
        }
        return result;
    }

    isTargetTimeLimited() {
        let result = false;
        //check if microgesture has key "minimalDuration"
        if (this.microgesture.parameters?.duration != undefined) {
            if (this.microgesture.parameters.duration.force) {   //force means that only time can make this glyph out
                result = true;
            }
        }
        return result;
    }


    /*
    GLYPH TO GLYPH COMPARISON METHODS
    */

    isActuatorMatch(rawDataEvent, listOfVariable) {



        if (this.useVariableFingerOnActuator) {     // why 1 : because 0 is the hand (right or left), 1 is the finger (index, middle, etc), 2 is the phalanx
            //check if this variable is defined inside listOfVariable, according to the key name
            // listOfVariable is an array of object. Each object has a name, a isDefined, a value, and a flagHasChanged

            return this.checkVariableFromEvent(rawDataEvent.actuator[0][0], this.variableFingerActuatorIndex, listOfVariable, rawDataEvent.timestamp);

        }

        // Check actuator in a normal way ( without variable), if this.microgesture.actuators[2] does NOT start with x , or if this variable is NOT defined in listOfVariable.
        else {

            if (rawDataEvent.actuator[0][0] != rawDataEvent.FINGER.ANY) {

                if (this.microgesture.actuator[0][0] != rawDataEvent.actuator[0][0]) {
                    return false; // it's not any, and it's not the same finger
                } else {
                    //Here is finger is OK, let's check if phalanx is OK
                    return this.isActuatorPhalanxMatch(rawDataEvent.phalanx, this.microgesture.phalanx, listOfVariable, rawDataEvent.timestamp);
                }

            } else {
                return true; // if actuator is any, it's a match
            }
        }

        return result;

    }



    isReceiverMatch(rawDataEvent, listOfVariable) {



        if (this.useVariableFingerOnReceiver) {
            //check if this variable is defined inside listOfVariable, according to the key name
            // listOfVariable is an array of object. Each object has a name, a isDefined, a value, and a flagHasChanged
            if (this.checkVariableFromEvent(rawDataEvent.contact.actuator[0][0], this.variableFingerReceiverIndex, listOfVariable, rawDataEvent.timestamp)) {
                return this.isReceiverPhalanxMatch(rawDataEvent.contact.phalanx, this.microgesture.contact.phalanx, listOfVariable);
            } else {
                return false;
            }
        }
        else {


            if (this.microgesture.contact?.action != undefined) {
                if (rawDataEvent.contact.action == null) {
                    return false;
                } else {
                    if (this.microgesture.contact.action != rawDataEvent.contact.action) {
                        return false;
                    } else {
                        // Both action exist and match. Let's check the actuator
                        if (this.microgesture.contact.actuator[0][0] != rawDataEvent.FINGER.ANY) {

                            if (this.microgesture.contact.actuator[0][0] != rawDataEvent.contact.actuator[0][0]) {
                                return false;
                            } else {
                                return this.isReceiverPhalanxMatch(rawDataEvent.contact.phalanx, this.microgesture.contact.phalanx, listOfVariable, rawDataEvent.timestamp);
                            }


                        } else {
                            return true; // if actuator is any, it's a match
                        }
                    }
                }

            } else {
                return true; // Contact is undefined, so it's a match
            }

        }



    }

    isActuatorPhalanxMatch(rawDataEventPhalanx, microgesturePhalanx, listOfVariable, timestamp) {
        let segmentMatch = this.isActuatorPhalanxSegmentMatch(rawDataEventPhalanx[0][0].segment, microgesturePhalanx[0][0].segment, listOfVariable, timestamp);
        let sideMatch = this.isPhalanxSideMatch(rawDataEventPhalanx[0][0].side, microgesturePhalanx[0][0].side);

        return sideMatch && segmentMatch;


    }

    isReceiverPhalanxMatch(rawDataEventPhalanx, microgesturePhalanx, listOfVariable, timestamp) {
        let segmentMatch = this.isReceiverPhalanxSegmentMatch(rawDataEventPhalanx[0][0].segment, microgesturePhalanx[0][0].segment, listOfVariable, timestamp);
        let sideMatch = this.isPhalanxSideMatch(rawDataEventPhalanx[0][0].side, microgesturePhalanx[0][0].side);

        return sideMatch && segmentMatch;


    }


    isPhalanxSideMatch(rawDataEventPhalanxSide, microgesturePhalanxSide) {

        if (microgesturePhalanxSide == "any" || microgesturePhalanxSide == "front") {
            return true;
        } else {
            if (microgesturePhalanxSide == rawDataEventPhalanxSide) {
                return true;
            }
            else {
                return false;
            }
        }


    }

    isActuatorPhalanxSegmentMatch(rawDataEventPhalanxSegment, microgesturePhalanxSegment, listOfVariable, timestamp) {

        // In the case of Segment, "front" is the same as "any"
        if (microgesturePhalanxSegment == "any") {
            return true;
        } else {
            if (microgesturePhalanxSegment == rawDataEventPhalanxSegment) {
                return true;
            }
            else {
                return false;
            }
        }

    }

    isReceiverPhalanxSegmentMatch(rawDataEventPhalanxSegment, microgesturePhalanxSegment, listOfVariable, timestamp) {

        if (this.useVariablePhalanxOnReceiver) {
            //check if this variable is defined inside listOfVariable, according to the key name
            // listOfVariable is an array of object. Each object has a name, a isDefined, a value, and a flagHasChanged
            return this.checkVariableFromEvent(rawDataEventPhalanxSegment, this.variablePhalanxReceiverIndex, listOfVariable, timestamp);
        } else {

            // In the case of Segment, "front" is the same as "any"
            if (microgesturePhalanxSegment == "any") {
                return true;
            } else {
                if (microgesturePhalanxSegment == rawDataEventPhalanxSegment) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }

    }

    isDirectionMatch(rawDataEvent) {
        let result = true;

        if (this.microgesture.context != undefined && this.microgesture.context.length < 2) {
            result = false;
        }
        //same for the rawDataEvent
        else if (rawDataEvent.context != undefined && rawDataEvent.context.length < 2) {
            result = false;
        }

        else if (this.microgesture.action != undefined && rawDataEvent.action != undefined) {
            if (this.microgesture.action != rawDataEvent.action) {
                result = false;
            }
        }

        return result;
    }

    isContextMatch(rawDataEvent) {


        if (this.microgesture.context != undefined && rawDataEvent.context != undefined) {
            if (this.microgesture.context.length == 2 && rawDataEvent.context.length == 2) {
                if (this.microgesture.context[0] == rawDataEvent.context[0] && this.microgesture.context[1] == rawDataEvent.context[1]) {
                    return true;
                }
            } else if (this.microgesture.context.length == 1 && rawDataEvent.context.length == 1) {
                if (this.microgesture.context[0] == rawDataEvent.context[0]) {
                    return true;
                }
            }
        }
        return false;

    }

    checkVariableFromEvent(fingerOfEvent, variableIndex, listOfVariable, timestamp) {


        let variable = this.getVariableFromList(listOfVariable, variableIndex);

        if (variable.isDefined) {
            //check is there is a match
            //if variable value is one string, just check it
            if (variable.value.length == 1) {
                if (variable.value[0] != fingerOfEvent) {
                    return false;
                }
            }

            if (variable.value.length > 1) {
                // this is a AND situation 
                // first the "and" is already started
                if (!this.variableAndStart) {
                    //check if there is a match, in the uncheck list
                    // let uncheckList = this.uncheckFingerAndOperator(listOfVariable);

                    // //check if uncheckList contains rawDataEvent.actuator[0]
                    // let matchisfound = false;
                    // uncheckList.forEach((variable, index) => {

                    //     if (variable == fingerOfEvent) {
                    //         this.memoryOfAndOperator[index] = timestamp;
                    //         matchisfound = true;
                    //     }
                    // });

                    // if (matchisfound) {
                    //     result = true; // this is the special case, when result = 1;
                    // } else {
                    //     result = false; // looking for a match but is not;
                    // }

                } else {
                    //AND variable did not start yet
                    //check if there is a match in the entire list
                    // initAndOperator(listOfVariable); // even if there is not match, init the memoryOfAndOperator, in case there is a match.
                    // variable.value.forEach((variable, index) => {
                    //     if (variable == rawDataEvent.actuator[0][0]) {
                    //         this.variableAndStart = true;
                    //         this.memoryOfAndOperator[index] = rawDataEvent.timestamp;
                    //     }
                    // });

                    // if (this.variableAndStart == true) {
                    //     result = true; // this is the special case, when result = 1;
                    // } else {
                    //     result = false; // looking for a match but is not;
                    // }

                }//AND not started

            }//AND situation




        } else {
            //if the variable is NOT defined ( means that there is a OR x1 = finger OR ring , but or is not defined yet)
            // check is this event is "compatible" as a definition of the variable
            // Note that this function is only checking is the value is "compatible", and is not updating the value
            //Until the entire gesture is accepted.
            if (fingerOfEvent != undefined) {
                let isFingerIncludedInFormula = false;
                variable.formula.forEach((subformula) => {
                    if (subformula[0] == fingerOfEvent) {
                        isFingerIncludedInFormula = true;
                    }
                });

                if (!isFingerIncludedInFormula) {
                    return false;
                }
            }
        }

        return true;


    }

    updateActuatorVariableFromEvent(rawdataevent, listOfVariable) {

        if (this.useVariableFingerOnActuator) {
            let fingerOfEvent = rawdataevent.actuator[0][0];
            let timestamp = rawdataevent.timestamp;
            let variableIndex = this.variableFingerActuatorIndex;
            let variable = this.getVariableFromList(listOfVariable, variableIndex);
            if (!variable.isDefined) {
                //if NOT defined ( means that there is a OR x1 = finger OR ring , but or is not defined yet)
                // check is this event is working as a definition of the variable
                // TODO : change that ! maybe the new finger is a good one, but we need to validate the entire gesture. 
                // In this situation, the function need to return a maybe, and not a 0 or 1
                if (fingerOfEvent != undefined) {
                    variable.formula.forEach((subformula) => {
                        if (subformula[0] == fingerOfEvent) {
                            //update the listofvariable object
                            variable.isDefined = true;
                            variable.value = [];
                            variable.value.push(fingerOfEvent);
                            variable.flagHasChanged = true;
                        }
                    });

                }
            }
        }




    }

    updateReceiverFingerVariableFromEvent(rawdataevent, listOfVariable) {

        if (this.useVariableFingerOnReceiver && rawdataevent.contact?.actuator.length > 0) {
            let fingerOfEvent = rawdataevent.contact.actuator[0][0];
            let variableIndex = this.variableFingerReceiverIndex;
            let variable = this.getVariableFromList(listOfVariable, variableIndex);
            if (!variable.isDefined) {
                //if NOT defined ( means that there is a OR x1 = finger OR ring , but or is not defined yet)
                // check is this event is working as a definition of the variable
                // TODO : change that ! maybe the new finger is a good one, but we need to validate the entire gesture. 
                // In this situation, the function need to return a maybe, and not a 0 or 1
                if (fingerOfEvent != undefined) {
                    variable.formula.forEach((subformula) => {
                        if (subformula[0] == fingerOfEvent) {
                            //update the listofvariable object
                            variable.isDefined = true;
                            variable.value = [];
                            variable.value.push(fingerOfEvent);
                            variable.flagHasChanged = true;
                        }
                    });

                }
            }
        }




    }

    updateReceiverPhalanxVariableFromEvent(rawdataevent, listOfVariable) {
        if (this.useVariablePhalanxOnReceiver) {
            if (rawdataevent.contact?.phalanx.length > 0 && rawdataevent.contact.phalanx[0].length > 0 && rawdataevent.contact.phalanx[0][0].segment != undefined) {
                let phalanxOfEvent = rawdataevent.contact.phalanx[0][0].segment;
                let variableIndex = this.variablePhalanxReceiverIndex;
                let variable = this.getVariableFromList(listOfVariable, variableIndex);
                if (!variable.isDefined) {
                    if (phalanxOfEvent != undefined) {
                        variable.formula.forEach((subformula) => {
                            if (subformula[0] == phalanxOfEvent) {
                                //update the listofvariable object
                                variable.isDefined = true;
                                variable.value = [];
                                variable.value.push(phalanxOfEvent);
                                variable.flagHasChanged = true;
                            }
                        });

                    }
                }

            }
        }
    }



    updateParameterHistory(rawDataEvent) {
        let result = 0;

        if (this.parameterHistory.name != undefined && rawDataEvent.parameters[this.parameterHistory.name] != undefined) {
            if (rawDataEvent.parameters[this.parameterHistory.name].start == null && rawDataEvent.parameters[this.parameterHistory.name].end == null) {
                // In the case that rawDataEvent does not specify parameters value. Let's give arbitrary value
                if (rawDataEvent.action == rawDataEvent.ACTION.FLEXION) {
                    rawDataEvent.parameters[this.parameterHistory.name].start = this.DEFAULT_START_FLEXION
                    rawDataEvent.parameters[this.parameterHistory.name].end = this.DEFAULT_END_FLEXION
                }
                else if (rawDataEvent.action == rawDataEvent.ACTION.EXTENSION) {
                    rawDataEvent.parameters[this.parameterHistory.name].start = this.DEFAULT_START_FLEXION
                    rawDataEvent.parameters[this.parameterHistory.name].end = this.DEFAULT_END_FLEXION
                }
            }

            this.parameterHistory.value.push(rawDataEvent.parameters[this.parameterHistory.name].start);
            this.parameterHistory.value.push(rawDataEvent.parameters[this.parameterHistory.name].end);

            switch (this.parameterHistory.goal) {
                case "startend":
                    if (this.parameterHistory.value.length > 1) {
                        // At this point, we should have theoricly a array like this [start1, end1, start2, end2, start3, end3, etc...], 
                        //[ 10, 25, 27, 35, 36, 60] . In a perfect world  the endpoint the the next startpoint should be the same value.
                        // First way to check the parameter is done, is the mesaure the difference between the end and the start, and check if it's the same for all the pair.
                        //check if the parameter curve go to an DONE state
                        let closenessToStart = Math.abs(this.parameterHistory.value[0] - this.parameterHistory.startgoal);
                        let closenessToEnd = Math.abs(this.parameterHistory.value[this.parameterHistory.value.length - 1] - this.parameterHistory.endgoal);
                        this.parameterHistory.progression = 100 - closenessToEnd;
                        if (closenessToStart < this.THRESH_DISTANCE_GOAL && closenessToEnd < this.THRESH_DISTANCE_GOAL) {
                            this.setState(this.STATE.ACCEPTED);
                        }

                    }
                    break;
                case "length":
                    if (this.parameterHistory.value.length > 1) {
                        //check if the parameter curve go to an DONE state
                        // At this point, we should have theoricly a array like this [start1, end1, start2, end2, start3, end3, etc...], 
                        //[ 10, 25, 27, 35, 36, 60] . In a perfect world  the endpoint the the next startpoint should be the same value.
                        // First way to check the parameter is done, is the mesaure the difference between the end and the start, and check if it's the same for all the pair.
                        const lastValue = this.parameterHistory.value[this.parameterHistory.value.length - 1];
                        const firstValue = this.parameterHistory.value[0];
                        const difference = lastValue - firstValue;

                        this.parameterHistory.progression = Math.round((difference / this.parameterHistory.lengthgoal) * 100);

                        //console.log("firstValue : " + firstValue + " lastValue : " + lastValue + " difference : " + difference);
                        if (difference > this.parameterHistory.lengthgoal) {
                            this.setState(this.STATE.ACCEPTED);
                        }


                    }
                    break;
            }

            this.refreshUIProgression();



        } else {
            //throw error
            return this.STATE.REJECTED;
        }



    }


    /*
    ANALYZERS INTERFACE METHODS : reset, start, setStart.
    + init of And Operator ( usage of "and" inside the variable)
    */



    setState(newState) {

        this.state = newState;
        this.refreshUIState();

    }

    refreshUIState() {

        this.controller.updateAnalyzerGlyphActiveState(
            this.index,
            this.position,
            this.state
        );

    }

    refreshUIProgression() {
        this.controller.updateAnalyzerGlyphProgress(
            this.index,
            this.position,
            this.parameterHistory
        );

    }

    reset() {
        //this.setActive(0);
        this.setState(this.STATE.INACTIVE);
        this.parameterHistory.value = [];
        this.parameterHistory.progression = 0;
        this.variableAndStart = false;
        this.memoryOfAndOperator = [];
    }

    start() {
        this.setState(this.STATE.LISTENING);
        this.parameterHistory.value = [];
        this.parameterHistory.progression = 0;
        if (this.goal == this.GOAL.DURATION) {
            //start the timer
            this.timer = setTimeout(() => {
                this.setState(this.STATE.ACCEPTED);
                this.parent.notifyEndOfTimer(this.index)
            }, this.microgesture.parameters.duration);
        }
    }

    uncheckFingerAndOperator(listOfVariable) {
        result = [];
        listOfVariable.value.forEach((variable, index) => {
            if (this.memoryOfAndOperator[index] != 0) {
                result.push(variable);
            }
        });
        return result;


    }

    oldestFingerAndOperator() {
        let minValue = Number.MAX_VALUE;

        this.memoryOfAndOperator.forEach((value) => {
            if (value > 0 && value < minValue) {
                minValue = value;
            }
        });

        if (minValue == Number.MAX_VALUE) {
            minValue = 0;
        }
        return minValue;

    }

    initAndOperator(listOfVariable) {
        this.memoryOfAndOperator = [];
        if (this.variableIntegrity(listOfVariable, this.variableFingerActuatorIndex)) {
            listOfVariable[this.variableFingerActuatorIndex].value.forEach((variable) => {
                this.memoryOfAndOperator.push(0);
            });
        }

    }

    getVariableFromList(listOfVariable, variableIndex) {
        if (this.variableIntegrity(listOfVariable, variableIndex)) {
            return listOfVariable[variableIndex];
        } else {
            throw new Error("Variable index is not defined in listOfVariable");
        }
    }

    validityOfAndOperator(listOfVariable, variableIndex) {
        if (this.variableIntegrity(listOfVariable, variableIndex)) {
            if (this.memoryOfAndOperator.length != listOfVariable[variableIndex].value.length) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    variableIntegrity(listOfVariable, variableIndex) {
        // check if variable index as a proper entry in listOfVariable
        if (variableIndex >= listOfVariable.length) {
            throw new Error("Variable index is not defined in listOfVariable");
        } else {
            if (listOfVariable[variableIndex]?.name == undefined) {
                throw new Error("Variable index is not defined in listOfVariable : " + variableIndex);
            }
        }
        return true;
    }



}

module.exports = AnalyzerGlyph;
