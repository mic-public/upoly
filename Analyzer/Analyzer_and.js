/*

This is subclass AND of the superclass Analyzer
Analyzer_AND

Each subclass need to implement : 

testMaskOnRawDataEvent and return a boolean.



The AND subclass implement multiple subclasses, and each need to return true in a short amount of time, to return true to the parent class.


*/


const Controller = require("../Controller/Controller.js");
const AnalyzerGlyph = require("./Analyzer_glyph.js");
const AnalyzerInterface = require("./AnalyzerInterface.js");
//const AnalyzerOr = require("./Analyzer_or.js");
//const AnalyzerThen = require("./Analyzer_then.js");

class AnalyzerAnd extends AnalyzerInterface {
    //add a constructor without argument because GestureEventManager is a singleton
    constructor(i, m, position, listOfVariable = {}) {
        //constructor of parent
        super();
        this.index = i;
        this.microgesture = m;
        this.length = m.operands.length;
        this.listOfOperands = [];
        this.position = position; // position inside index tree. [ 0 , 1, 2] for example.
        this.controller = Controller.getInstance(); // TODO : should be replace by controller. 
        this.duration = 1000; //AND means that all operants have to result 2 in a short amount of time. This is the duration of this time.
        this.resetTimeWhenRejected = 0; // Time when 1 subresult is reject, before reset. 0 is the reight value, 1000 help for debug.
        this.startTime = undefined; // time of the first event
        this.actualDuration = undefined; // time of the first event
        //create a array of boolean, size of this.length
        this.listOfAccepted = []; //Array of boolean, size of this.length


        //create  a for each on m.operands
        m.operands.forEach((operand, index) => {
            let newpos = position + "-" + index;
            switch (operand.type) {
                case "glyph":
                    this.listOfOperands.push(
                        new AnalyzerGlyph(this.index, operand, newpos, null, listOfVariable) //parent is null in case of AND
                    );
                    break;
                case "or":
                    // this.listOfOperands.push(new AnalyzerOr(this.index, operand, newpos));
                    console.log("AnalyzerAND : AnalyzerOr not implemented : circular dependency issue");
                    break;

                case "and":
                    this.listOfOperands.push(
                        new AnalyzerAnd(this.index, operand, newpos, listOfVariable)
                    );
                    break;
                case "then":
                    // this.listOfOperands.push(
                    //   new AnalyzerThen(this.index, operand, newpos)
                    // );
                    console.log("AnalyzerAND : AnalyzerThen not implemented : circular dependency issue");
                    break;
            }
        });



        this.reset();
    }

    // Check if the mask match the rawDataEvent
    analyzeOnEvent(rawDataEvent, listOfVariable) {
        //TODO : check time. These AND event should be activated in a short amount of time.
        // In conclusion this more a THEN composition, but without order, and with a time limit.
        // As THEN composition, the result can be 0 ( nothing match or not enough time), 1 there is match but the AND is not complete, 2, there is a match


        if (this.startTime == undefined) {
            this.startTime = Date.now();
        } else {
            this.actualDuration = Date.now() - this.startTime;
        }
        // create a for each on the listOfOperands


        this.listOfOperands.forEach((operand, index) => {
            if (this.listOfAccepted[index] == false) {
                // If they are already accepted, we don't need to test them again.
                this.listOfResult[index] = operand.analyzeOnEvent(rawDataEvent, listOfVariable);
            }

        });

        return this.processResultAndReturn();
    }

    processResultAndReturn() {

        //check if one at least is accepted
        if (this.oneOrMoreIsAccepted()) {
            for (let i = 0; i < this.listOfResult.length; i++) {
                if (this.listOfAccepted[i] == false && this.listOfResult[i] == this.STATE.ACCEPTED) {
                    this.listOfAccepted[i] = true;
                }

                if (this.listOfAccepted[i] == false && this.listOfResult[i] == this.STATE.REJECTED) {
                    if (this.resetTimeWhenRejected == 0) {
                        this.listOfOperands[i].reset();
                    } else {

                        setTimeout(() => {
                            this.listOfOperands[i].reset();

                        }, 500);
                    }
                }
            }
        }

        if (this.allAreRejected()) {
            this.setState(this.STATE.REJECTED);
        }

        //In any situation, even if is Accomplished i
        if (this.startTime != undefined && this.actualDuration > this.duration) {
            this.setState(this.STATE.REJECTED);
        } else {

            if (this.allAreAccepted()) {
                this.setState(this.STATE.ACCEPTED);
            }

        }

        console.log("result of AND is " + this.state);

        return this.state;

    }

    setState(newState) {
        this.controller.updateAnalyzerGlyphActiveState(this.index, this.position, newState);
        if (newState != this.state) {
            //TODO notify controller
            this.state = newState;
        }
    }

    start() {
        this.listOfOperands.forEach((operand) => {
            operand.start();
        });
        this.setState(this.STATE.LISTENING);
        this.listOfResult = [];
        this.listOfAccepted = [];
        for (let i = 0; i < this.listOfOperands.length; i++) {
            this.listOfResult.push(1);
            this.listOfAccepted.push(false);
        }
        this.startTime = undefined;
        this.actualDuration = undefined;
    }

    reset() {
        this.listOfOperands.forEach((operand) => {
            operand.reset();
        });
        //set list of result to false this.listOfResult = this.listOfAccepted  
        //erase these list and fill them with false, at the size of this.listOfOperands.length
        this.listOfResult = [];
        this.listOfAccepted = [];
        for (let i = 0; i < this.listOfOperands.length; i++) {
            this.listOfResult.push(1);
            this.listOfAccepted.push(false);
        }


        this.setState(this.STATE.INACTIVE)
        this.startTime = undefined;
        this.actualDuration = undefined;
    }
}

module.exports = AnalyzerAnd;
