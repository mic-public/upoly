/*
This is the interface, set of usefull tool for all analyzer and subanalyzer.

This is likely the state machine management.

this.state
this.result
this.setState // remplacement of setActive. : yes. 




*/



class AnalyzerInterface {
    //add a constructor without argument because GestureEventManager is a singleton
    constructor() {

        this.state = 0;
        this.STATE = {
            INACTIVE: 0,
            LISTENING: 1,
            DONE: 2,
            ACCEPTED: 3,
            REJECTED: 4
        };

        this.IGNORED = -1;

        this.listOfResult = [];   // in glyph analyzer, only one result.

    }

    setState(newState) {
        throw new Error("setState is not implemented : should be impletented by sub class");
    }

    reset() {
        //throw an arror as it should be called only by sub class
        throw new Error("reset() is not implemented : should be impletented by sub class");
    }

    start() {
        //throw an arror as it should be called only by sub class
        throw new Error("start() is not implemented : should be impletented by sub class");
    }

    analyzeOnEvent(rawDataEvent, listOfVariable) {
        throw new Error("analyzeOnEvent() is not implemented : should be impletented by sub class");
    }

    processResultAndReturn() {
        //analyze listOfResult and conclude what should be returned.
        //throw an arror as it should be called only by sub class
        throw new Error("processResult() is not implemented : should be impletented by sub class");
    }


    oneOrMoreIsRejected() {
        let result = false;
        this.listOfOperands.forEach((operand) => {
            if (operand.state == this.STATE.REJECTED) {
                result = true;
            }
        });
        return result;

    }

    oneOrMoreIsAccepted() {
        let result = false;
        this.listOfOperands.forEach((operand) => {
            if (operand.state == this.STATE.ACCEPTED) {
                result = true;
            }
        });
        return result;

    }

    allAreAccepted() {
        let result = true;
        this.listOfOperands.forEach((operand) => {
            if (operand.state != this.STATE.ACCEPTED) {
                result = false;
            }
        });
        return result;

    }

    allAreRejected() {
        let result = true;
        this.listOfOperands.forEach((operand) => {
            if (operand.state != this.STATE.REJECTED) {
                result = false;
            }
        });
        return result;

    }

    oneOrMoreIsDone() {
        let result = false;
        this.listOfOperands.forEach((operand) => {
            if (operand.state == this.STATE.DONE) {
                result = true;
            }
        });
        return result;

    }

    updateIndex(i) {
        this.index = i;
        if (this.listOfOperands?.length > 0) {

            this.listOfOperands.forEach((operand) => {
                operand.updateIndex(i);
            });
        }

    }






}



module.exports = AnalyzerInterface;
