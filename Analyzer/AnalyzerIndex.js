// export { default as AnalyzerThen } from "./Analyzer_then.js";
// export { default as AnalyzeOr } from "./Analyzer_or.js";
// export { default as AnalyzeAnd } from "./Analyzer_and.js";
// export { default as AnalyzeGlyph } from "./Analyzer_glyph.js";
// export { default as AnalyzeVariable } from "./Analyzer_Variable.js";
//change to require syntax
const AnalyzerThen = require("./Analyzer_then.js");
const AnalyzeOr = require("./Analyzer_or.js");
const AnalyzeAnd = require("./Analyzer_and.js");
const AnalyzeGlyph = require("./Analyzer_glyph.js");
const AnalyzeVariable = require("./Analyzer_variable.js");
const AnalyzeInterface = require("./AnalyzerInterface.js");


// TODO : change names of file to AnalzerOr , and keep consistency with the other files.