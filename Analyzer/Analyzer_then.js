/*

This is subclass THEN of the superclass Analyzer
Analyzer_then

Each subclass need to implement : 

analyzeOnEvent and return a boolean.



The THEN subclass implement and index, which correspond to the timeline index of the microgesture.


*/

const Controller = require("../Controller/Controller.js");
const AnalyzerGlyph = require("./Analyzer_glyph.js");
const AnalyzerOr = require("./Analyzer_or.js");
const AnalyzerAndInsideThen = require("./Analyzer_and.js");
const AnalyzerInterface = require("./AnalyzerInterface.js");


class AnalyzerThen extends AnalyzerInterface {
    //add a constructor without argument because GestureEventManager is a singleton
    constructor(i, m, position, listOfVariable={}) {
        //parent constructor
        super();
        this.index = i;
        this.indexOfThen = 0;
        this.indexOfEvent = 0;
        this.microgesture = m;
        this.length = m.operands.length;
        this.listOfOperands = [];
        this.duration = 2000;
        this.position = position; // position inside index tree. "0-3-2" for example.
        this.controller = Controller.getInstance();


        //create  a for each on m.operands
        m.operands.forEach((operand, index) => {
            let newpos = position + "-" + index;
            switch (operand.type) {
                case "glyph":
                    this.listOfOperands.push(
                        new AnalyzerGlyph(this.index, operand, newpos, this, listOfVariable) // this is a parent function
                    );
                    break;
                case "or":
                    this.listOfOperands.push(new AnalyzerOr(this.index, operand, newpos, listOfVariable));
                    break;
                case "and":
                    this.listOfOperands.push(
                        new AnalyzerAndInsideThen(this.index, operand, newpos, listOfVariable)
                    );
                    break;
                case "then":
                    this.listOfOperands.push(
                        new AnalyzerThen(this.index, operand, newpos, listOfVariable)
                    );
                    break;
            }
        });

        this.reset();
    }

    // Check if the mask match the rawDataEvent
    analyzeOnEvent(rawDataEvent, listOfVariable) {
        //TODO
        //check if this.active == 1 before testing the mask.
        // or at least, send a warning / error msg, but this should not happens.

        let result = 0; // means no match

        //Regarder ici s'il faut transmettre 

        let subresult =
            this.listOfOperands[this.indexOfThen].analyzeOnEvent(
                rawDataEvent, listOfVariable
            );

        //add subresult to the list of result push method
        this.listOfResult[this.indexOfThen] = subresult;
        //check the size of the list of result is the size of indexThen


        return this.processResultAndReturn();
    }

    processResultAndReturn() {

        if (this.state == this.STATE.LISTENING || this.state == this.STATE.DONE) {

            //In case of then, only the last result is important.

            let valueToReturn = 0; // means no match
            //Should be a switch of the this.state here, but not implemented yet.
            let lastResult = this.listOfResult[this.indexOfThen];

            if (lastResult == this.IGNORED) {
                return this.IGNORED;
            }

            if (lastResult == this.STATE.ACCEPTED) {
                this.matchIsFound();
            }

            if (lastResult == this.STATE.DONE) {
                this.setState(this.STATE.DONE);
            }

            if (lastResult == this.STATE.REJECTED) {
                this.setState(this.STATE.REJECTED);
            }



            return this.state;

        } else {
            this.setState(this.STATE.REJECTED);
            // throw new Error("Analyzer_Then : processResultAndReturn() : not in a state to listen");
        }

    }


    matchIsFound() {
        this.indexOfThen++;
        // console.log(
        //     "Analyzer_Then : A match is found ! go to next index :" +
        //     this.indexOfThen +
        //     " / " +
        //     this.length
        // );

        if (this.indexOfThen >= this.length) {
            //console.log("Analyzer_Then : All match are found !");
            this.setState(this.STATE.ACCEPTED);
        } else {
            this.listOfOperands[this.indexOfThen].start();
        }
    }

    setState(newState) {
        this.controller.updateAnalyzerGlyphActiveState(this.index, this.position, newState);
        if (newState != this.state) {
            //TODO notify controller
            this.state = newState;
        }
    }


    reset() {
        this.indexOfThen = 0;
        this.indexOfEvent = 0;
        this.listOfResult = [];
        this.listOfOperands.forEach((operand) => {
            operand.reset();
            this.listOfResult.push(0);
        });

        this.setState(this.STATE.INACTIVE);
    }

    start() {
        this.indexOfThen = 0;
        this.listOfOperands[this.indexOfThen].start();
        this.setState(this.STATE.LISTENING);
    }


    //This is a real special function, when a analyzerGlyph child has a duration goal, and need to notify the parent when the duration is over.
    notifyEndOfTimer(index) {
        if (index == this.indexOfThen) {
            // this is the ongoing operand.

        }

    }
}

module.exports = AnalyzerThen;
