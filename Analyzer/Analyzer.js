/*
This is not a interface anymore.
This is a machine state, related to microgesture.
There is subclasses that can help the machine state , like helping for "and, "or", sequence, 
but the list of analyzer are a list of instance of Analyzer class, and each instance contain a microgesture object.
*/
/*
What is the list of subclasses : 

Analyzer_then
Analyzer_or
Analyzer_and
analyzer_glyph

Example : then : container multiple subclasses
testMaskOnRawDataEvent : send to the subclasses, which send them to them subclasses. 
The final result is a 0, 1, 2.

then : mask with subclasses(index)
or : mask with subclasses(0) OR mask with subclasses(1), OR ...
and : mask with subclasses(0) AND mask with subclasses(1), AND ...
glyph : mask with the glyph itself ( end of the cascade)

switch to french :

Gestions des variables : comment savoir si l'analyzer contient des variables ou non?
Comment ajouter des variables si il n'y en a pas ?
Comment enlever des variables si il y en a ?

*/


const RawDataEvent = require("../RawDataEvent.js");
const GestureEvent = require("../GestureEvent.js");
const Controller = require("../Controller/Controller.js");
const AnalyzerGlyph = require("./Analyzer_glyph.js");
const AnalyzerOr = require("./Analyzer_or.js");
const AnalyzerAnd = require("./Analyzer_and.js");
const AnalyzerVariable = require("./Analyzer_Variable.js");
const AnalyzerThen = require("./Analyzer_then.js");
const AnalyzerInterface = require("./AnalyzerInterface.js");

class Analyzer extends AnalyzerInterface {
    //add a constructor without argument because GestureEventManager is a singleton
    constructor(n, i, m, gsmanager) {
        super();
        this.gsm = gsmanager; // gesture subscriber manager
        this.controller = Controller.getInstance();
        this.name = n;
        this.index = i;
        this.microgesture = m;
        this.subanalyzer = undefined;
        this.autoReload = true;
        this.autoReloadDuration = 200;
        this.nbOfEvent = 0; // this number increase each time subresult return 1. allow to know when the first glyph is found, and mesure time progression.
        this.maxDuration = 2000;
        this.startTime = undefined;
        this.timerMaxDuration = undefined; // This is the setTimeout for the maxDuration. Put in variable in order to remove it.
        this.timerAutoReload = undefined; // This is the setTimeout for the autoReload


        this.loadMicrogesture();
        this.reset();


    }

    // ***** Debug function **** NOT USEFUL
    //every 2 seconds, send a "hello" message to the UI
    hello() {
        console.log("start hello function");
        setInterval(() => {
            let random = Math.random() > 0.5 ? true : false;
            this.subanalyzer.setActive(random);
            console.log("hello test function : " + random);
        }, 1000);
    }

    loadMicrogesture() {

        if (this.microgesture.type != undefined) {
            this.createSubAnalyzer();
        }



        this.autoReload = this.microgesture.autoReload ? this.microgesture.autoReload : true;
        if (!this.microgesture.autoReloadDuration) {
            this.microgesture.autoReloadDuration = 200;
            this.autoReloadDuration = 200;
        } else {
            this.autoReloadDuration = this.microgesture.autoReloadDuration;
        }
        if (!this.microgesture.duration) {
            this.microgesture.duration = 2000;
            this.maxDuration = 2000;
        } {
            this.maxDuration = this.microgesture.duration;
        }

    }

    updateIndexOnParent(i) {
        if (this.microgesture.type != undefined && this.subanalyzer != undefined) {
            this.subanalyzer.updateIndex(i);

        }
        this.index = i;

    }

    restart() {

        //this not reset() then start() because we want to skeep the autoreload part
        if (this.microgesture.type != undefined && this.subanalyzer != undefined) {
            this.subanalyzer.reset();
            this.listOfResult = [];
            //console.log("reset analyzer : " + this.index);
            this.start();
        }
    }

    reset(now = false) {
        if (this.microgesture.type != undefined && this.subanalyzer != undefined) {

            this.nbOfEvent = 0;
            this.listOfResult = [];
            if (this.timerMaxDuration != undefined) {
                clearTimeout(this.timerMaxDuration);
                //console.log("clear time out max duration");
            }
            if (this.timerAutoReload != undefined) {
                clearTimeout(this.timerAutoReload);
                //console.log("clear time out auto reload");
            }


            //console.log("reset analyzer : " + this.index);
            if (this.autoReload) {
                if (now) {
                    this.subanalyzer.reset();
                    this.start();
                } else {
                    //restart in x second later
                    this.timerAutoReload = setTimeout(() => {
                        //console.log("TIME OUT auto reload");
                        this.subanalyzer.reset();
                        this.start();
                    }, this.autoReloadDuration);
                }
            } else {
                this.subanalyzer.reset();
                this.setState(this.STATE.INACTIVE);
            }
        }
    }

    start() {
        if (this.microgesture.type != undefined && this.subanalyzer != undefined) {
            this.subanalyzer.start();
            this.nbOfEvent = 0;
            //console.log("start analyzer : " + this.index);
            this.setState(this.STATE.LISTENING);
            if (this.microgesture.duration != undefined) {
                this.maxDuration = this.microgesture.duration;
            }

        }
    }

    stop() {
        if (this.microgesture.type != undefined && this.subanalyzer != undefined) {
            this.subanalyzer.reset()
            this.nbOfEvent = 0;
            //console.log("start analyzer : " + this.index);
            this.setState(this.STATE.INACTIVE);
        }
    }


    updateMicrogesture(microg) {
        //copy microgesture via json encode and decode
        this.microgesture = JSON.parse(JSON.stringify(microg));
        console.log("update microgesture : " + this.microgesture.type);
        console.log("auto reload duration:" + this.microgesture.autoReloadDuration);
        this.loadMicrogesture();
        this.reset();
    }

    reloadRefreshFromUI() {
        this.controller.updateSingleAnalyzerGesture(this.index, this.name, this.microgesture);
    }

    createSubAnalyzer() {
        //Create a subanalyzer.
        switch (this.microgesture.type) {
            case "glyph":
                this.subanalyzer = new AnalyzerGlyph(this.index, this.microgesture, "0", this);
                break;
            case "or":
                this.subanalyzer = new AnalyzerOr(this.index, this.microgesture, "0");
                break;
            case "and":
                this.subanalyzer = new AnalyzerAnd(this.index, this.microgesture, "0");
                break;
            case "then":
                this.subanalyzer = new AnalyzerThen(this.index, this.microgesture, "0");
                break;
            case "seq":
                //console.log(" there is a seq here")
                // TODO  : SEQ does not mean that there is variable ! 
                if (this.microgesture.listOfVariable != undefined) {
                    // be sur that everything is initialized
                    this.microgesture.listOfVariable.forEach((variable) => {
                        variable.isDefined = false;
                        variable.value = undefined;
                        variable.flagHasChanged = false;
                    });
                } else {
                    // Use static method to create this list of Variable.
                    let listOfVariable = AnalyzerVariable.createObjectOfVariable(this.microgesture.quantificators);
                    //add listOfVariable to the microgesture, inside a new key close to quantificator
                    //same line, but using a clean copy, using json encode and decode
                    this.microgesture.listOfVariable = JSON.parse(JSON.stringify(listOfVariable));
                }
                // When calling constructor, the listOfVariable is  already created.
                this.subanalyzer = new AnalyzerVariable(this.index, this.microgesture);
                break;
        }
    }

    analyzeOnEvent(rawDataEvent, emptyListOfVariable) {

        if (this.state == this.STATE.LISTENING) {
            if (this.subanalyzer != undefined) {
                //console.log("analyzer analyzing ....");

                //Save the result of the subanalyzer
                this.listOfResult.push(this.subanalyzer.analyzeOnEvent(rawDataEvent, this.microgesture.listOfVariable));


            }
        }

        return this.processResultAndReturn();
    }


    processResultAndReturn() {

        let result = undefined;
        if (this.listOfResult.length > 0) {
            // get the last result
            result = this.listOfResult[this.listOfResult.length - 1];

        }
        if (result == undefined) {
            //throw new Error("Analyzer.js result is undefined");
            this.setState(this.STATE.REJECTED);
        }

        switch (result) {
            case this.STATE.ACCEPTED:
                //no match
                this.setState(this.STATE.ACCEPTED);
                this.emitGestureEvent();
                this.reset();
                break;

            case this.STATE.REJECTED:
                this.setState(this.STATE.REJECTED);
                this.reset();
                break;
            case this.STATE.DONE:
                this.reset(); // special case because the analyzer is finished but not the mouvement
                break;
            case this.STATE.LISTENING:
                if (this.nbOfEvent == 0) {
                    //first time
                    this.startTime = Date.now();
                    this.controller.updateStartSingleAnalyzerDuration(this.index);
                    //First check time progression
                    if (this.maxDuration != undefined) {
                        this.timerMaxDuration = setTimeout(() => {
                            //console.log("Time OUT max duration");
                            this.reset(true);
                        }, this.maxDuration);
                        //console.log("max duration : " + this.maxDuration);
                    }
                }
                this.nbOfEvent++;
                break;
            case this.IGNORED:
                //console.log("Analyzer : IGNORED");
                break;



        }
    }

    emitGestureEvent() {
        let gestureEvent = new GestureEvent(
            "microgesture",
            this.name,
            this.index,
            undefined,
            this.microgesture);

        console.log("emitGestureEvent : " + this.name);

        this.emit(gestureEvent);
    }

    //emit a GestureEvent to the GestureEventManager
    emit(gestureEvent) {
        //check if the gestureEvent is an instance of GestureEvent
        if (gestureEvent instanceof GestureEvent) {
            //send it to the GestureEventManager
            this.gsm.emit(gestureEvent);
            //console log : Analyzer : emitting gesture event
            //console.log("Analyzer : emitting gesture event");
            //console.log(gestureEvent);
        } else {
            // if it is not, throw an error
            throw new Error("Only GestureEvent instances can be emitted");
        }
    }

    setState(newState) {

        this.state = newState;
        this.controller.updateSingleAnalyzerState(this.index, this.state);


    }

    setActive(state) {
        if (this.microgesture.type != undefined && this.subanalyzer != undefined) {
            this.active = state;
            if (state == 1) {
                //start listening
                this.subanalyzer.setActive(state);
            }
            this.controller.updateSingleAnalyzerState(this.index, this.active);
        }
    }

    //TODO need to clean this ...
    toUI(adress, data) { }

    toUIConsole(data) {
        this.controller.updateAnalyzersConsoleUI(data, this.index);
    }


    testMaskOnRawDataEvent(rawDataEvent) {
        // check if the rawDataEvent is intersting for this analyzer
        // this not impleted yet, so return true
        return true;
    }

    isVariable() {
        //There is 2 different way to check. look a the microgesture, if there is a "seq" key. Or
        // look at the subanalyzer, if it is a instance of AnalyzerVariable.

        if (this.subanalyzer != undefined) {
            //check if the class of the subanalyzer is AnalyzerVariable
            if (this.subanalyzer instanceof AnalyzerVariable) {
                //check if the variable is defined
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    addVariable() {

        if (!this.isVariable()) {
            // change the microgesture .
            // rebuild the analyzer and the UI.

            //put the actual microgesture in a temp variable
            let tempMicrogesture = JSON.parse(JSON.stringify(this.microgesture));
            //remove the actual microgesture, and create a new one
            this.microgesture = {};
            this.microgesture.type = "seq";
            this.microgesture.quantificators = { "x": [[["index"]]] }
            this.microgesture.seq = tempMicrogesture;
            //rebuild the analyzer
            this.loadMicrogesture();

        }

    }

}

module.exports = Analyzer;
