/*

This is subclass OR of the superclass Analyzer
Analyzer_OR

Each subclass need to implement : 

testMaskOnRawDataEvent and return a boolean.
The OR subclass test raw data event on each operand. If one operand return true, the OR return true to the parent class.
*/

const Controller = require("../Controller/Controller.js");
const AnalyzerGlyph = require("./Analyzer_glyph.js");
//const AnalyzerThen = require("./Analyzer_then.js"); //resolve circular dependency
const AnalyzerAnd = require("./Analyzer_and.js");
const AnalyzerInterface = require("./AnalyzerInterface.js");

class AnalyzerOr extends AnalyzerInterface {
    //add a constructor without argument because GestureEventManager is a singleton
    constructor(i, m, position, listOfVariable = {}) {
        //parent constructor
        super();
        this.index = i;
        this.microgesture = m;
        this.listOfOperands = [];
        this.position = position; // position inside index tree. [ 0 , 1, 2] for example.
        this.controller = Controller.getInstance();


        //create  a for each on m.operands
        m.operands.forEach((operand, index) => {
            let newpos = position + "-" + index;
            switch (operand.type) {
                case "glyph":
                    this.listOfOperands.push(
                        new AnalyzerGlyph(this.index, operand, newpos, null, listOfVariable) //parent is null in case of OR
                    );
                    break;
                case "or":
                    this.listOfOperands.push(new AnalyzerOr(this.index, operand, newpos, listOfVariable));
                    break;
                case "and":
                    this.listOfOperands.push(
                        new AnalyzerAnd(this.index, operand, newpos, listOfVariable)
                    );
                    break;
                case "then":
                    // this.listOfOperands.push(
                    //   new AnalyzerThen(this.index, operand, newpos)
                    // );
                    console.log("AnalyzerOr : AnalyzerThen not implemented : circular dependency issue");
                    break;
            }
        });

        this.reset();
    }

    // Check if the mask match the rawDataEvent
    analyzeOnEvent(rawDataEvent, listOfVariable) {
        // At least, one operand need to return true to return true to the parent class.
        // Return 0 : no match at all. Return 2 : match found and composition is over . Return 1 : match found but composition is not over ( not happening in OR)

        let result = 0;

        this.listOfOperands.forEach((operand) => {
            let subresult = operand.analyzeOnEvent(rawDataEvent, listOfVariable);
            this.listOfResult.push(subresult);

        });

        return this.processResultAndReturn();

    }

    processResultAndReturn() {

        if (this.oneOrMoreIsAccepted()) {
            this.setState(this.STATE.ACCEPTED);
        }

        if (this.allAreRejected()) {
            this.setState(this.STATE.REJECTED);
        }

        return this.state;
    }

    setState(newState) {
        this.controller.updateAnalyzerGlyphActiveState(this.index, this.position, newState);
        if (newState != this.state) {
            //TODO notify controller
            this.state = newState;
        }
    }


    reset() {
        this.listOfOperands.forEach((operand) => {
            operand.reset();
        });
        this.setState(this.STATE.INACTIVE);
    }

    start() {
        this.listOfOperands.forEach((operand) => {
            operand.start();
        });
        this.setState(this.STATE.LISTENING);
    }
}

module.exports = AnalyzerOr;
