/*

This is subclass AND of the superclass Analyzer
Analyzer_AND

Each subclass need to implement : 

testMaskOnRawDataEvent and return a boolean.



The AND subclass implement multiple subclasses, and each need to return true in a short amount of time, to return true to the parent class.


*/


const Controller = require("../Controller/Controller.js");
const AnalyzerGlyph = require("./Analyzer_glyph.js");
const AnalyzerOr = require("./Analyzer_or.js");
const AnalyzerThen = require("./Analyzer_then.js");
const AnalyzerAnd = require("./Analyzer_and.js");

class AnalyzerVariable {
    //there is no position here, because variable is the root of the index tree, not part of it.
    // When constructor is called, this list of Variable is already created, because we use the static function createObjectOfVariableq at the root analyzer, and then add the result inside the microgesture.
    // Thus, the root analyzer know the clean version of listOfVariable, and can send it to UI in a easy way.
    constructor(i, m) {
        this.index = i;
        this.microgesture = m;
        this.subanalyzer = undefined;
        this.listOfVariable = []; // this object is share with all sub analyzer

        /*
        this.listOfVariable.push({
            name: "x1",
            isDefined: false,
            value: undefined,   // "index"
            flagHasChanged: false,
            formula: [["index"], ["ring", middle"]]   // this means index OR ( ring AND middle)
        });

        note1 that flagHasChange is really important. The glyph analyzer set the flag to true if there is a match while isDefined is false.
        Then, thanks to the flag, variableAnalyzer is aware of the change, and update the controller, then put the flag back to false.

        note2 that the AND according to variable is really hard to manage

        note3 : the formula is not supposed to change. It is set at the beginning, and then never change. It is read by the editor.

        */



        this.controller = Controller.getInstance();

        this.timeoffirstevent = undefined;

        if (this.microgesture.type != undefined) {
            this.listOfVariable = this.microgesture.listOfVariable;
            this.createSubAnalyzer();
        }
    }

    updateIndex(i) {
        this.index = i;
        if (this.subanalyzer != undefined) {
            this.subanalyzer.updateIndex(i);
        }

    }

    // Check if the mask match the rawDataEvent
    analyzeOnEvent(rawDataEvent, variableEmptyObject) {
        //variableEmptyObject is {} send by Analyzer to its subanalyzer, without knowing what is inside.
        // Analyzer_variable replace this empty object by this.listOfVariable, which is shared with all subanalyzer.
        if (this.subanalyzer != undefined) {


            let result = this.subanalyzer.analyzeOnEvent(rawDataEvent, this.listOfVariable);

            //check if some variable has changed, and update the UI
            this.listOfVariable.forEach((variable) => {
                if (variable.flagHasChanged) {
                    this.controller.updateAnalyzerGlyphVariable(this.index, this.listOfVariable)
                    variable.flagHasChanged = false;
                }
            });

            return result;
        } else {
            return 0;
        }
    }

    start() {
        if (this.microgesture.seq.type != undefined && this.subanalyzer != undefined) {
            //start listening
            this.subanalyzer.start();
            this.controller.updateAnalyzerGlyphVariable(this.index, this.listOfVariable);
        }
    }


    reset() {

        //for each variable, : isDefined: false, value: undefined, flagHasChanged: false,
        this.listOfVariable.forEach((variable) => {
            variable.isDefined = false;
            variable.value = undefined;
            variable.flagHasChanged = false;
        });

        if (this.subanalyzer != undefined) {
            this.subanalyzer.reset();
        }

        this.controller.updateAnalyzerGlyphVariable(this.index, this.listOfVariable)

    }

    createSubAnalyzer() {
        //Create a subanalyzer.
        //check if the microgesture.seq is not empty
        if (this.microgesture.seq.type == undefined) {
            console.log("ERROR : microgesture.seq.type is undefined");
            this.subanalyzer = undefined;
            return;
        }

        switch (this.microgesture.seq.type) {
            case "glyph":
                this.subanalyzer = new AnalyzerGlyph(
                    this.index,
                    this.microgesture.seq,
                    "0",
                    this,
                    this.listOfVariable
                );
                break;
            case "or":
                this.subanalyzer = new AnalyzerOr(this.index, this.microgesture.seq, "0", this.listOfVariable);
                break;
            case "and":
                this.subanalyzer = new AnalyzerAnd(this.index, this.microgesture.seq, "0", this.listOfVariable);
                break;
            case "then":
                this.subanalyzer = new AnalyzerThen(this.index, this.microgesture.seq, "0", this.listOfVariable);
                break;
        }
    }



    //copy the same function createObjectOfVariable from the code above, but tranform it to static function, that take the microgesture.quantificators.x as input, and return the object.
    static createObjectOfVariable(quantificators) {
        // this.microgesture.seq.quantificators.x is array of "array in array". There first layer correspond to the number of variable ( x1, x2, x3, etc). The second layer is the OR, the thirs layer is the AND.
        //change this strange notation to a more simple object.
        //first check that there is a quantificator.x
        let listOfVariable = [];
        if (quantificators.x != undefined) {
            if (quantificators.x.length == 1) {
                //if there is only one variable, we don't need to create a list of variable
                if (AnalyzerVariable.checkFormulaFinger(quantificators.x[0]))
                    listOfVariable.push({
                        name: "x",
                        isDefined: false,
                        value: undefined,
                        flagHasChanged: false,
                        formula: quantificators.x[0]
                    });
            } else {

                quantificators.x.forEach((variable, index) => {
                    if (AnalyzerVariable.checkFormulaFinger(variable))
                        listOfVariable.push({
                            name: "x" + String(index + 1),
                            isDefined: false,
                            value: undefined,
                            flagHasChanged: false,
                            formula: variable
                        });
                });
            }
        }
        if (quantificators.y != undefined) {
            if (quantificators.y.length == 1) {
                if (AnalyzerVariable.checkFormulaPhalanx(quantificators.y[0]))
                    listOfVariable.push({
                        name: "y",
                        isDefined: false,
                        value: undefined,
                        flagHasChanged: false,
                        formula: quantificators.y[0].map((phalanx) => [(phalanx[0]).segment])
                    });
            }
            else {


                quantificators.y.forEach((variable, index) => {
                    if (AnalyzerVariable.checkFormulaPhalanx(variable))
                        listOfVariable.push({
                            name: "y" + String(index + 1),
                            isDefined: false,
                            value: undefined,
                            flagHasChanged: false,
                            formula: variable.map((phalanx) => [(phalanx[0]).segment])
                        });
                });
            }
        }
        return listOfVariable;
    }


    static checkFormulaFinger(formula) {
        //formula is an array of array. The first layer correspond to the OR, the second layer correspond to the AND.
        // AND is not supported yet, so basically, the template is [ ["index"], ["ring"] ]
        // verify that each string contains the following list : "thumb", "index", "middle", "ring", "pinky"
        //if not true, return false and leave a console.log message

        let result = true;
        formula.forEach((subformula) => {
            subformula.forEach((subsubformula) => {
                if (
                    subsubformula != "thumb" &&
                    subsubformula != "index" &&
                    subsubformula != "middle" &&
                    subsubformula != "ring" &&
                    subsubformula != "pinky"
                ) {
                    console.log("ERROR : formula not valid");
                    result = false;
                }
            });
        });
        return result;
    }

    static checkFormulaPhalanx(formula) {
        //formula is an array of array. The first layer correspond to the OR, the second layer correspond to the AND.
        // AND is not supported yet, so basically, the template is [ ["index"], ["ring"] ]
        // verify that each string contains the following list : "thumb", "index", "middle", "ring", "pinky"
        //if not true, return false and leave a console.log message

        let result = true;
        formula.forEach((subformula) => {
            subformula.forEach((subsubformula) => {
                if (
                    subsubformula.segment != "tip" &&
                    subsubformula.segment != "middle" &&
                    subsubformula.segment != "base" &&
                    subsubformula.segment != "nail"
                ) {
                    console.log("ERROR : formula phalanx not valid : " + subsubformula);
                    result = false;
                }
            });
        });
        return result;
    }
}

module.exports = AnalyzerVariable;
