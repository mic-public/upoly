import { reactive } from 'vue'

export const store = reactive({
    conciseMode: true,
    displayIncomingMessages: true,
    manualTrigger: false,
    advancedmode: false,
})
