/*
Use this document as reference
https://socket.io/how-to/use-with-vue
*/

import { reactive } from "vue";
import { io } from "socket.io-client";

export const state = reactive({
  connected: false,
  fooEvents: [],
  barEvents: [],
});



// "undefined" means the URL will be computed from the `window.location` object
// const URL =
//   process.env.NODE_ENV === "production" ? "ws://localhost:3000" : "http://localhost:3000";

const URL = "http://localhost:3000";

console.log("*****URL:  " + URL);

// depolute console when in development of front end without server activated.
const ACTIVE_CONNECTION_WITH_SERVER = true;

export const socket = io(URL, { autoConnect: ACTIVE_CONNECTION_WITH_SERVER });



socket.on("connect", () => {
  state.connected = true;
  console.log("*****connected");
  console.log(socket.id);
});

socket.on("disconnect", () => {
  state.connected = false;
});

socket.on("foo", (...args) => {
  state.fooEvents.push(args);
});

socket.on("bar", (...args) => {
  state.barEvents.push(args);
});
