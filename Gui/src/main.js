

import { createApp } from "vue";
import App from "./App.vue";
// Vuetify
import "@mdi/font/css/materialdesignicons.css"; // Ensure you
import "vuetify/styles";
import vuetify from './vuetify'





const myV3App = createApp(App);
// Make BootstrapVue available throughout your project
myV3App.use(vuetify);

//CreateApp is vue3 method
myV3App.mount("#app");
