import { createVuetify } from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
import { aliases, mdi } from "vuetify/iconsets/mdi";

import * as components from "vuetify/components";
import * as directives from "vuetify/directives";

export default createVuetify({
    components,
    directives,
    theme: {
        defaultTheme: 'userTheme',
        themes: {
            userTheme: {
                dark: false,
                variables: {}, // ✅ this property is required to avoid Vuetify crash

                colors: {
                    //green: '#00ff00' // cannot use primary color names here, so use a custom color name (such as 'greenish')
                    greenish: '#03DAC5',

                    // Workaround: Custom colors seem to erase default colors, so we need to include the default colors (of `light` or `dark` theme)
                    background: '#EEEE00',
                    surface: '#fafdff',
                    primary: '#1976D2',
                    'primary-darken-1': '#3700B3',
                    secondary: '#ffDAC5',
                    'secondary-darken-1': '#03DAC5',
                    error: '#CF6679',
                    info: '#2196F3',
                    success: '#4CAF50',
                    warning: '#FB8C00'
                },
            },
            developerTheme: {
                dark: false,
                variables: {}, // ✅ this property is required to avoid Vuetify crash

                colors: {
                    //green: '#00ff00' // cannot use primary color names here, so use a custom color name (such as 'greenish')
                    greenish: '#03DAC5',

                    // Workaround: Custom colors seem to erase default colors, so we need to include the default colors (of `light` or `dark` theme)
                    background: '#EEEE00',
                    surface: '#fafdff',
                    primary: '#00796B',
                    'primary-darken-1': '#3700B3',
                    secondary: '#ffDAC5',
                    'secondary-darken-1': '#03DAC5',
                    error: '#CF6679',
                    info: '#2196F3',
                    success: '#4CAF50',
                    warning: '#FB8C00'
                },
            }
        }
    }
});

