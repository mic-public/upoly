# µPoly



The main focus of this project is microgesture detection and recognition, but it can be easily extended to include other gesture recognition capabilities.

This project is a collaborative research project. The architecture is based on testing, error correction, collaborative work, and research.

The aim of the project is to create a unique tool for all participants who want to test their own devices or algorithms for detection.

# Complete Documentation

[CLICK HERE TO ACCESS DOCUMENTATION](https://velvet-noise.notion.site/Documentation-poly-0912a482fdcf4374b4ea7e4cd3205e0d)

or copy paste link
https://velvet-noise.notion.site/Documentation-poly-0912a482fdcf4374b4ea7e4cd3205e0d


*Documentation made with Notion*

# Quick Getting started

To test µPoly quickly with Mediapipe. 
1. Check that Nodejs ( > 16.20) and python3 are installed

The Mediapipe driver requires python3, and is called through python3 command. If you use another python command instead, change Device/Mediapipe/Mediapipe.js

2. Clone this repository
```
npm install
npm run installextra
npm run buildgui
npm run start

3. Open a browser on http://localhost:8080

