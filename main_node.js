// The goal of this project is an server app, able to handle mutliple devices, from Arduino Gloves, to teensy HID, or even usb Camera .
// Theses devices can send raw data to the server.
// The server will analyse these data, using mutiple ways of analysing, then conclude that a result is detected.
// External app can suscribe to specific result, and will be notified when a result is detected.
// Get arguments of the command line
const args = process.argv.slice(2);

let testMode = false;
let prodMode = "prod";
args.forEach((arg) => {
    if (arg === "--test") {
        testMode = true;
    }
    if (arg === "--dev") {
        prodMode = "dev";
    }
});


//change all the import to require
const DeviceManager = require("./DeviceManager.js");
const AnalyzerManager = require("./AnalyzerManager.js");
const GestureSubscriberManager = require("./SubscriberManager.js");
const ConfigManager = require("./ConfigManager.js");
const Controller = require("./Controller/Controller.js");


//Create controller object
const controller = new Controller(prodMode); // dev or production mode
// In case of production mode, start the static server in order to open dist/index.html. And disable console.log !
if (prodMode === "prod") {
    controller.server.listen(controller.port);
    console.log("*".repeat(80));
    console.log("Welcome to the µPoly application");
    console.log("Please open a browser to this url : http://localhost:8080");
    console.log("Press Ctrl+C to stop the application")
    console.log("*".repeat(80));
    console.log = function () { };
}


// Create instance Manager
const gsm = new GestureSubscriberManager(); // Instanciate external apps according to config file
const am = new AnalyzerManager(gsm); // Instanciate analyzers according to config file
const dm = new DeviceManager(am); // Instanciate devices according to config file
const cm = new ConfigManager(dm, am, gsm); // Instanciate config manager

// controller have to know all the instance created
controller.dm = dm;
controller.am = am;
controller.sm = gsm;
controller.cm = cm;



//exit if test mode
if (testMode) {
    console.log("------------------------");
    console.log("Test mode activated");
    console.log("------------------------");
    //pause during 1sec , to be sure that everything is initialized
    setTimeout(() => {
        dm.test();
        am.test();
        process.exit();
    }, 1000);


}