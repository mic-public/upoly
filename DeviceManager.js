
const Device = require("./Device/Device.js");
const D = require("./Device/DeviceIndex.js");
const Controller = require("./Controller/Controller.js");
const RawDataEvent = require("./RawDataEvent.js");


class DeviceManager {
  constructor(amanager) {
    this.devicesList = [];
    //list in console log all the classes of devices available from import * as D
    //console.log("DeviceManager constructor");
    this.controller = Controller.getInstance();
    this.am = amanager; //analyzer manager

    //create a array, with of all the devices available, in array of string
    this.devicesAvailable = Object.keys(D);
    this.serialPortsListAvailableRaw = []; // List of all serial port available
    this.blePortsListAvailableRaw = []; // List of all ble port available
    this.portsListAvailableSorted = []; //List of all ports available, sorted by protocol
    //console.log(this.devicesAvailable);
    this.updateAvailablePortsList();


  }

  ////////////////////////////////////  ADD REMOVE UPDATE DEVICE  //////////////////////////////////////
  //todo remove name argument, because this is the child class that implement the name of the parent class
  addDevice(protocolType, DeviceName, port = null) {
    let childConstructor = D[DeviceName];
    if (childConstructor) {
      //console.log("DeviceManager addDevice : " + protocolType + " : " + DeviceName + " : " + port);
      //TODO : check the protocolType match the protocol of the device
      let deviceIndex = this.devicesList.length;
      if (port != null) {
        let device = new childConstructor(deviceIndex, this.am, port);
        this.devicesList.push(device);
      } else {
        let device = new childConstructor(deviceIndex, this.am);
        this.devicesList.push(device);
      }
      //this.controller.addDeviceUI(deviceName, name, deviceIndex);
    } else {
      console.log("DeviceManager addDevice : deviceName not found");
    }
  }

  //delete device
  deleteDevice(i) {
    //check if index is in the range of the array
    if (i >= 0 && i < this.devicesList.length) {
      //delete the device from the array
      this.devicesList[i].close();
      this.devicesList.splice(i, 1);

      //update the index of the devices
      this.devicesList.forEach((device, newIndex) => {
        device.indexDevice = newIndex;
      });
    }
  }

  deleteAll() {
    //delete all devices
    for (let i = this.devicesList.length - 1; i >= 0; i--) {
      this.deleteDevice(i);
    }
  }

  instanciateAll() {
    this.devicesAvailable.forEach((deviceName) => {
      this.addDevice(deviceName, "TestModeDevice");
    });

  }

  restartConnection(index) {
    if (index >= 0 && index < this.devicesList.length) {
      console.log("DeviceManager restartConnection : " + index);
      this.devicesList[index].restartConnection();
    }
  }

  updateDevicePort(index, port) {
    //Format of port
    // {
    //   name: '/dev/tty.usbmodemDCDA0C22EFA82',
    //   protocol: 'serial',
    //   manufacturer: 'Arduino'
    // }


    if (index >= 0 && index < this.devicesList.length) {
      console.log("DeviceManager updateDevicePort : " + index + " : " + port.name);

      //First check the port is inside the available ports
      // this.serialPortsListAvailableRaw.forEach((portAvailable) => {
      //   if (portAvailable.name == port) {
      //     // The port exists, let's check the protocol is compatible with the device
      //     let deviceProtocol = this.devicesList[index].constructor.protocol ? this.devicesList[index].constructor.protocol : undefined;
      //     if (deviceProtocol != undefined && deviceProtocol == portAvailable.protocol) {
      //       //this.devicesList[index].updatePort(port);
      //       //TODO. the device need a function called updatePort.
      //       // Hard to be inside deivce.js as it supposed the management of a port.
      //     }
      //   }
      // });

      this.devicesList[index].openPort(port.name);


    }
  }

  restartAll() {
    this.devicesList.forEach((device) => {
      device.restartConnection();
    });
  }

  configFromFile(configFile) {
    //configFile is a json file, read the key called "devices"
    //for each device in the list, call addDevice with the name of the device and the name of the device

    //1st check the "devices" key exist
    if (configFile.hasOwnProperty("devices")) {
      //2nd check if the "devices" key is an array
      if (Array.isArray(configFile["devices"])) {
        //3rd for each device in the array, call addDevice with the name of the device and the name of the device
        this.deleteAll();
        configFile["devices"].forEach((device) => {
          //console.log("CONFIG From File " + device["type"] + ":" + device["name"]);
          if (device["port"]) {
            //console.log("CONFIG From File. Port is defined " + device["port"]);
            this.addDevice(device["type"], device["name"], device["port"]);
          } else {
            this.addDevice(device["type"], device["name"]);
          }
        });
      }
    }
    //LOG
    /*
    console.log("DeviceManager configFromFile");
    //for each device of deviceList, console log the device.name
    this.devicesList.forEach((device) => {
      //console log the instance name of the device
      console.log(device.index + ":" + device.constructor.name);
    });
    */
  }


  ////////////////////////////////////  EXPORT CONFIG  //////////////////////////////////////
  exportConfig() {
    //this function is the opposite of configFromFile. it read the actuat config and return a javacript object
    //with the same structure as the config file.

    //create a empty array
    let devices = [];
    this.devicesList.forEach((device) => {
      //add the object to the array
      let obj = {
        type: device.constructor.name,
        name: device.name,
      }

      if (device.port) {
        obj.port = device.port;
      }

      devices.push(obj);

    });


    return devices;
  }



  ////////////////////////////////////  MANUAL TRIGGER  //////////////////////////////////////
  manualTriggerFromGlyph(glyphObject) {
    //the purpose of the function is to simulate a raw data event. 
    // Raw data event and glyph are not the same thing, but quite similar.
    // This particular function take a glyph and need to convert it to a raw data event.
    // TODO : think about changing raw data event to glyph event : easier to compare, easier to modify.

    // This is really a pain to convert glyph to rawdataevent, knowing that AnalyzerGlyph.analyseOnEvent() is really badly coded.
    let rawdataevent = RawDataEvent.fromGlyph(glyphObject);
    console.log("DeviceManager manualTriggerFromGlyph");

    //console.log(rawdataevent);

    this.am.emit(rawdataevent);

  }

  ///////////////////////////////////  GET AVAILABLE DEVICES  //////////////////////////////////////
  getAvailablePorts() {
    let res = [];
    this.serialPortsListAvailableRaw.forEach((port) => {
      console.log("getAvailablePorts Serial available ");
      //check if port.path contain the word USB ( macos, linux) or COM (windows)
      if (port.path.includes("usb") || port.path.includes("COM") || port.path.includes("ACM")) {
        res.push({
          name: port.path,
          protocol: Device.PROTOCOL.SERIAL,
          manufacturer: port.manufacturer,
        });
      }




      //add the same for rawhid, bluetooth, wifi

    });
    console.log("getAvailablePorts BLE available ");
    this.blePortsListAvailableRaw.forEach((port) => {
      res.push({
        name: port.path,
        protocol: Device.PROTOCOL.BLUETOOTH,
        manufacturer: port.manufacturer,
      });
    });
    return res;
  }

  getAvailablePortPerProtocol(protocol) {
    return this.portsListAvailableSorted.filter((port) => {
      return port.protocol == protocol;
    }
    );
  }


  getAvailableDevicesType() {
    let res = [];
    this.devicesAvailable.forEach((deviceType) => {
      let MyClassReference = D[deviceType];
      //create a temporary instance
      res.push({
        name: deviceType,
        protocol: MyClassReference.protocol,
        defaultPort: MyClassReference.defaultPort,
        allowPortChoice: MyClassReference.allowPortChoice
      });

    });

    return res;
  }


  async updateAvailablePortsList() {
    this.serialPortsListAvailableRaw = await Device.listSerialPorts();
    let listBlePromise = Device.listBLEPorts();
    listBlePromise.then((list) => {
      this.blePortsListAvailableRaw = list;
    }).catch((e) => {
      console.log(e);
    });

    console.log("DeviceManager updateAvailablePortsList UPDATED");
    this.portsListAvailableSorted = this.getAvailablePorts();
    console.log(this.portsListAvailableSorted);

    //TODO : for each port, add a key "available" with a boolean value, and update it if the port is used
  }



  ///////////////////////////////////  TEST  //////////////////////////////////////
  test() {
    console.log("-------------------------");
    console.log("DeviceManager test 1 : delete all");
    this.deleteAll();
    console.log("-------------------------");
    console.log("DeviceManager test 2: instanciate all");
    this.instanciateAll();
    console.log("-------------------------");
    console.log("DeviceManager test 3: restart all");
    this.restartAll();
    console.log("-------------------------");
    console.log("DeviceManager test 4 : delete all again");
    this.deleteAll();
  }
}

module.exports = DeviceManager;
