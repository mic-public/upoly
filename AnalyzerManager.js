
const Analyzer = require("./Analyzer/Analyzer.js");
//const A = require("./Analyzer/AnalyzerIndex.js");
const RawDataEvent = require("./RawDataEvent.js");
const Controller = require("./Controller/Controller.js");
const fs = require("fs");
const path = require('path');

const appPath =
  process.env.NODE_ENV === 'production'
    ? `${process.resourcesPath}/app`
    : __dirname;


class AnalyzerManager {
  constructor(gsmanager) {
    this.analyzersList = [];
    this.gestureLibrary = []; //This is a library of json files, located in the folder "gestureLibrary"
    this.gsm = gsmanager; //gesture subscriber manager
    this.controller = Controller.getInstance();
    //list in console log all the classes of devices available from import * as D
    // this.analyzersAvailable = Object.keys(A);
    this.fillGestureLibrary();
  }

  addAnalyzer(type, name, microgesture) {
    //change of paradigme. Now  we are only using the Analyze.js class.
    // This generic analyzer contain a microgesture object.
    // This class a the generic class, then we use subClasses, in order the manage the "and" and "or" logic, inside the microgesture.

    // create a variable index
    let index = this.analyzersList.length;
    console.log("add analyzer - create instance of : " + name + " " + index);
    console.log("add analyzer - microgesture : " + microgesture);
    let analyzerInstance = new Analyzer(name, index, microgesture, this.gsm);
    this.analyzersList.push(analyzerInstance);
  }

  askPreviewFromLibrary(filename) {
    //check if the filename is in the gestureLibrary
    let found = false;
    let index = 0;
    while (index < this.gestureLibrary.length && !found) {
      if (this.gestureLibrary[index].filename == filename) {
        found = true;
      } else {
        index++;
      }
    }
    if (found) {
      //create a new analyzer
      let content = fs.readFileSync(path.join(appPath, 'GestureLibrary', filename));

      let json = JSON.parse(content);
      this.controller.previewAnalyzerUI(json.microgesture);
    } else {
      console.log("addAnalyzerFromLibrary : " + filename + " not found");
    }
  }

  askPreviewFromLibrarySpecificEditor(indexEditor, filename) {
    //check if the filename is in the gestureLibrary
    let found = false;
    let index = 0;
    while (index < this.gestureLibrary.length && !found) {
      if (this.gestureLibrary[index].filename == filename) {
        found = true;
      } else {
        index++;
      }
    }
    if (found) {
      //create a new analyzer
      let content = fs.readFileSync(path.join(appPath, 'GestureLibrary', filename));
      let json = JSON.parse(content);
      this.controller.previewAnalyzerUISpecificEditor(indexEditor, json.microgesture);
    } else {
      console.log("addAnalyzerFromLibrary : " + filename + " not found");
    }
  }





  addAnalyzerFromLibrary(filename) {
    //check if the filename is in the gestureLibrary
    let found = false;
    let index = 0;
    while (index < this.gestureLibrary.length && !found) {
      if (this.gestureLibrary[index].filename == filename) {
        found = true;
      } else {
        index++;
      }
    }
    if (found) {
      this.addAnalyzerFromLibraryIndex(index);

    } else {
      console.log("addAnalyzerFromLibrary : " + filename + " not found");
    }

  }

  addAnalyzerFromLibraryIndex(index) {
    if (index < this.gestureLibrary.length) {
      let content = fs.readFileSync(path.join(appPath, 'GestureLibrary', this.gestureLibrary[index].filename));
      let json = JSON.parse(content);
      this.addAnalyzer("microgesture", this.gestureLibrary[index].name, json.microgesture);
    } else {
      console.log("addAnalyzerFromLibraryIndex : " + index + " not found");
    }
  }

  removeAnalyzer(analyzerIndex) {
    //check if the index is in the list
    if (analyzerIndex < this.analyzersList.length) {
      //remove the analyzer from the list
      this.analyzersList.splice(analyzerIndex, 1);
      //update the index of the analyzers
      this.analyzersList.forEach((analyzer, index) => {
        analyzer.updateIndexOnParent(index);
      });
    }
  }

  updateAnalyzerMicogesture(analyzerIndex, microg) {
    if (analyzerIndex < this.analyzersList.length) {
      this.analyzersList[analyzerIndex].updateMicrogesture(microg);
    }
  }

  fillGestureLibrary() {
    //look for all the json files in the folder "gestureLibrary"


    //get all the json files in the folder "gestureLibrary"
    //console.log(" final path string : ")
    //console.log(appPath);
    fs.readdirSync(path.join(appPath, 'GestureLibrary')).forEach((file) => {

      //first check if the file is a json file
      if (file.endsWith(".json")) {

        //parse the json file
        let content = fs.readFileSync(path.join(appPath, 'GestureLibrary', file));
        let json = JSON.parse(content);
        let res = {};
        res.filename = file;
        if (json.hasOwnProperty("name")) {
          res.name = json.name;
        } else {
          res.name = file;
        }
        //add the json to the gestureLibrary array
        this.gestureLibrary.push(res);
      }

    });

    //console.log("AnalyzerManager : addGesture Library " + this.gestureLibrary.length);

  }

  addVariableToAnalyzer(analyzerIndex) {
    this.analyzersList[analyzerIndex].addVariable();
    this.analyzersList[analyzerIndex].reloadRefreshFromUI();
  }

  //update the name of of specific analyzer, by index
  updateAnalyzerName(analyzerIndex, name) {
    if (analyzerIndex < this.analyzersList.length) {
      console.log("updateAnalyzerName : " + name + " index=" + analyzerIndex);
      this.analyzersList[analyzerIndex].name = name;
    }
  }


  setAnalyzerAutoReload(analyzerIndex, state) {
    if (analyzerIndex < this.analyzersList.length) {
      this.analyzersList[analyzerIndex].autoReload = state;
      //console.log("setAnalyzerAutoReload : " + state + " index=" + analyzerIndex);
    }

  }

  restartSpecificAnalyzer(analyzerIndex) {
    if (analyzerIndex < this.analyzersList.length) {
      this.analyzersList[analyzerIndex].restart();
    }
  }

  stopSpecificAnalyzer(analyzerIndex) {
    if (analyzerIndex < this.analyzersList.length) {
      this.analyzersList[analyzerIndex].stop();
    }
  }

  manualTrigger(analyzerName) {
    this.analyzersList.forEach((ana) => {
      if (ana.name == analyzerName) {
        console.log("manualTrigger : " + analyzerName);
        ana.emitGestureEvent();
      }
    });
  }

  //create a method called emit
  emit(rawDataEvent) {
    //check if the rawDataEvent is an instance of RawDataEvent
    if (rawDataEvent instanceof RawDataEvent) {
      // if it is, send it to all the analyzers
      for (let analyzer of this.analyzersList) {
        //check if the analyzer subscribes these caracteristics
        if (analyzer.state == analyzer.STATE.LISTENING) {
          if (analyzer.testMaskOnRawDataEvent(rawDataEvent)) {
            analyzer.analyzeOnEvent(rawDataEvent, {});
          }
        }

      }
      //Clean is removing the static constant thingm like this.FINGER.ANY this.PHALANX.ANY
      RawDataEvent.cleanRawDataEvent(rawDataEvent);
      this.controller.incomingRawDataEvent(rawDataEvent);

    } else {
      // if it is not, throw an error
      throw new Error("Only RawDataEvent instances can be emitted");
    }
  }


  deleteAll() {
    //remove all the analyzers from the list
    for (let i = this.analyzersList.length - 1; i >= 0; i--) {
      this.removeAnalyzer(i);
    }
  }

  addAllLibraryAsAnalyzer() {
    this.gestureLibrary.forEach((ana) => {
      this.addAnalyzerFromLibrary(ana.filename);
    });
  }

  configFromFile(configFile) {
    //configFile is a json file, read the key called "devices"
    //for each device in the list, call addDevice with the name of the device and the name of the device

    //1st check the "devices" key exist
    if (configFile.hasOwnProperty("analyzers")) {
      //2nd check if the "devices" key is an array
      if (Array.isArray(configFile["analyzers"])) {
        //3rd for each device in the array, call addDevice with the name of the device and the name of the device
        this.deleteAll();
        configFile["analyzers"].forEach((ana) => {
          this.addAnalyzer("microgesture", ana["name"], ana["microgesture"]);
        });
      }
    }

  }

  exportConfig() {
    //exact opposite of configFromFile, return a javascript object with the same structure as the config file.
    let analyzers = [];
    this.analyzersList.forEach((ana) => {
      analyzers.push({
        type: "microgesture",
        name: ana.name,
        microgesture: ana.microgesture,
      });
    });

    return analyzers;
  }

  getListOfAnalyzersName() {

    let list = [];
    this.analyzersList.forEach((ana) => {
      list.push(ana.name);
    });
    return list;
  }

  test() {
    console.log("------------------------");
    console.log("AnalyzerManager : test 1 : remove all");
    this.deleteAll();
    console.log("------------------------");
    console.log("AnalyzerManager : test 2 : add all library");
    this.addAllLibraryAsAnalyzer();
  }
}

module.exports = AnalyzerManager;
