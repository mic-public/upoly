
const Device = require("../Device.js");
const RawDataEvent = require("../../RawDataEvent.js");
const { SerialPort } = require('serialport')
const { exec } = require('child_process')
const util = require('util')
/*

36 bytes structures = 36 * 8 = 288 bits

struct SensorData
{
  uint8_t finger1; // lets encode all buton over 8 bits.
  uint8_t finger2; // lets encode all buton over 8 bits.
  uint8_t finger3; // lets encode all buton over 8 bits.
  uint8_t finger4; // lets encode all buton over 8 bits.
  uint16_t potentiometer1;
  uint16_t potentiometer2;
  uint16_t potentiometer3;
  uint16_t potentiometer4;
  char message[24]; // 24 bytes
};

*/

class FlexAndVelostat extends Device {
    //////////////////////////////////// STATIC PROTOCOL //////////////////////////////////////
    //If you use a standard protocol that require a port ( serial, bluetooth, wifi, rawhid, etc...)
    // You can define a protocol here.
    // This will allow the user to choose the port in the UI, and give it to the constructor port variable.
    // static protocol = Device.PROTOCOL.SERIAL;
    // static protocol = Device.PROTOCOL.BLUETOOTH;
    // static protocol = Device.PROTOCOL.HID;        // W.I.P
    // static protocol = Device.PROTOCOL.WIFI;       // W.I.P
    /////////////////////////////
    static protocol = Device.PROTOCOL.SERIAL;
    //add a constructor with em argument
    constructor(index, am, port = null) {
        //call the constructor of the parent class
        super(index, "Flex and Velostat", am);
        this.description = " Flex sensor on the index, et pressure velostat sensor on the same finger. Arduino-serial, Chaffangeon Caillet, PhD 2023"
        this.port = port;

        this.createSerialPort(this.port);
        this.init();


    }



    init() {


        this.dataBuffer = Buffer.alloc(0);  // Initialize an empty buffer

        // save the memory of the sensor value, in order to send only if it change
        this.listOfTouch = [[false, false, false], [false, false, false], [false, false, false], [false, false, false]];  // 4 finger, 3 phalanges
        this.listOfTouchFlagChange = [[false, false, false], [false, false, false], [false, false, false], [false, false, false]];  // 4 finger, 3 phalanges
        this.listOfAnalog = [0, 0, 0, 0];  // 4 potentiometer
        this.listOfPreviousAnalog = [0, 0, 0, 0];
        this.listOfAnalogFlagChange = [false, false, false, false];  // 4 potentiometer
        this.listOfDerivativeAnalog = [0, 0, 0, 0];  // 4 potentiometer : calculate the derivative of the analog value, in order the have what we call a signicative change
        this.listOfDerivativeAnalogFlagChange = [false, false, false, false];  // 4 potentiometer

    }

    createSerialPort(port) {
        // Create a port with error callback
        this.port = port;
        let promiseDevice = this.createSerialUsbDevice(9600);

        promiseDevice.then(() => {
            // Call back when the port is open and data are received
            this.device.on('data', (data) => {
                // Convertir les données binaires en tableau de valeurs
                //check that data is one byte long
                //get last byte of buffer into a new variable
                this.processData(data)
            });
        }).catch((err) => {
            console.log(err);
        });
    }




    processData(data) {

        // Convertir les données binaires en tableau de valeurs
        this.dataBuffer = Buffer.concat([this.dataBuffer, data]);
        let dataArray = Array.from(data);

        //console.log("data is " + this.dataBuffer);

        // Votre Arduino envoie des blocs de 36 bytes, donc vous pouvez accéder aux valeurs individuelles
        if (this.dataBuffer.length >= 36) {

            // Pour accéder à une valeur particulière, vous pouvez utiliser l'indice
            //const finger1but1 = Boolean(this.readBit(this.dataBuffer, 0));


            //update listOfAnalog. 2 bytes per value
            for (let i = 0; i < 4; i++) {
                //define byteindex
                let byteIndex = 4 + i * 2;

                //read the entire byte
                let byte = this.dataBuffer.at(byteIndex) + (this.dataBuffer.at(byteIndex + 1) << 8);
                //limit byte from 0 to 1023
                byte = Math.min(byte, 1023);
                //calculate the derivative

                if (i == 0) { // only sensor 1 , tip of index.
                    this.listOfAnalog[i] = byte;
                    let newTouch = byte > 150;
                    if (newTouch != this.listOfTouch[0][i]) {
                        this.listOfTouch[0][i] = newTouch;
                        this.listOfTouchFlagChange[0][i] = true;
                    }
                }

                if (i == 1) { // only sensor 2 are bottom of the index. and the threshold change with the position of the fingerm linked to the 3rd analog sensor.
                    this.listOfAnalog[i] = byte;
                    let newTouch = byte > Math.max(this.listOfAnalog[2] + 20, 200);
                    if (newTouch != this.listOfTouch[0][2]) {
                        this.listOfTouch[0][2] = newTouch;
                        this.listOfTouchFlagChange[0][2] = true;
                    }
                }


                //update derivative only if change is significant. remove noise
                //detect if the derative change direction
                if (i == 2) {

                    //check difference
                    let derivative = byte - this.listOfAnalog[i];
                    let analogThreshold = 10;
                    let analogThresholdEvent = 30;

                    if (Math.abs(derivative) > analogThreshold) {
                        this.listOfPreviousAnalog[i] = this.listOfAnalog[i];
                        this.listOfAnalog[i] = byte;
                        this.listOfAnalogFlagChange[i] = true;


                        //console.log("analog abs derivative " + i + " is " + Math.abs(derivative));
                        //console.log(" derivative =  " + derivative + " store= " + this.listOfDerivativeAnalog[i]);
                        let res = derivative * this.listOfDerivativeAnalog[i];
                        //this.consoleUI("derivative : " + derivative);
                        //this.consoleUI("res : " + res);
                        //this.consoleUI("memory : " + this.listOfDerivativeAnalog[i]);

                        if (res <= 0) {
                            if (Math.abs(derivative) > analogThresholdEvent) {

                                this.listOfDerivativeAnalogFlagChange[i] = true;
                                this.listOfDerivativeAnalog[i] = derivative;
                            }
                        }


                    }

                }
            }


            this.graphUI(this.listOfAnalog[0], 0);
            this.graphUI(this.listOfAnalog[1], 1);
            this.graphUI(this.listOfAnalog[2], 2);

            this.dataBuffer = Buffer.alloc(0);

            this.sendData();
        }

    }



    autoReconnect() {
        if (!this.isOpen) {
            this.restartConnection();
        }
    }

    restartConnection() {


        if (!this.isOpen) {
            this.createSerialPort(this.port);
        } else {
            this.close();
            setTimeout(() => {
                this.createSerialPort(this.port);
            }, 1000);
        }


    }



    // Function to read a specific bit at a given position
    readBit(buffer, position) {
        const byteIndex = Math.floor(position / 8);
        const bitIndex = position % 8;
        const byteValue = buffer.at(byteIndex);

        // Use bitwise shift and AND operation to read the bit
        return (byteValue & (1 << bitIndex)) !== 0;
    }


    sendData() {

        //check if there is a change in the touch
        for (let i = 0; i < 4; i++) {
            for (let j = 0; j < 3; j++) {
                if (this.listOfTouchFlagChange[i][j]) {

                    if (i == 0) {  // some debug reasons
                        //this.consoleUI("finger " + i + " phalange " + j + " is " + this.listOfTouch[i][j]);
                        this.sendTouch(this.listOfTouch[i][j], i, j);
                    }
                }
            }
        }

        //check if there is a change in the analog
        for (let i = 0; i < 4; i++) {
            if (this.listOfAnalogFlagChange[i]) {
                if (i == 2 && (Date.now() - this.timeOfCreation > 1000)) {  // some debug reasons
                    let newVal = (this.listOfAnalog[i] - 80) / 8;
                    let prevVal = (this.listOfPreviousAnalog[i] - 80) / 8;
                    this.consoleUI("potentiometer " + i + " is " + newVal);
                    this.sendDirectionInTheAir(newVal, prevVal);
                }
            }
        }

        //check if there is a change in the derivative of the analog. Only sensor 3 is watched
        for (let i = 0; i < 4; i++) {
            if (this.listOfDerivativeAnalogFlagChange[i]) {

                //check that this is not a calibration of the beginning. check that 1 sec passed after this.timeOfCreation
                if (Date.now() - this.timeOfCreation > 1000) {

                    //this.sendDirectionInTheAir(this.listOfDerivativeAnalog[i]);
                    //this.consoleUI("analog chge dir " + i + " is " + this.listOfDerivativeAnalog[i]);
                }
            }
        }


        // CLEAR ALL FLAG
        //at the end of the function, clear all flag
        for (let i = 0; i < 4; i++) {
            for (let j = 0; j < 3; j++) {
                this.listOfTouchFlagChange[i][j] = false;
            }
        }
        for (let i = 0; i < 4; i++) {
            this.listOfAnalogFlagChange[i] = false;
        }
        for (let i = 0; i < 4; i++) {
            this.listOfDerivativeAnalogFlagChange[i] = false;
        }

    }

    sendTouch(isTouching, ifinger, iphalanx) {

        let rde = new RawDataEvent();

        if (isTouching) {
            rde.context = [rde.CONTEXT.AIR, rde.CONTEXT.CONTACT];
            rde.action = rde.ACTION.FLEXION
        } else {
            rde.context = [rde.CONTEXT.CONTACT, rde.CONTEXT.AIR];
            rde.action = rde.ACTION.EXTENSION
        }

        //finger and phalanx are number here. convert them to string
        switch (ifinger) {
            case 0:
                ifinger = rde.FINGER.INDEX;
                break;
            case 1:
                ifinger = rde.FINGER.MIDDLE;
                break;
            case 2:
                ifinger = rde.FINGER.RING;
                break;
            case 3:
                ifinger = rde.FINGER.PINKY;
                break;
        }
        switch (iphalanx) {
            case 0:
                iphalanx = { segment: rde.PHALANX.SEGMENT.TIP, side: rde.PHALANX.SIDE.ANY };
                break;
            case 1:
                iphalanx = { segment: rde.PHALANX.SEGMENT.MIDDLE, side: rde.PHALANX.SIDE.ANY };
                break;
            case 2:
                iphalanx = { segment: rde.PHALANX.SEGMENT.BASE, side: rde.PHALANX.SIDE.ANY };
                break;
        }

        rde.actuator = [[rde.FINGER.THUMB]]
        rde.contact.action = rde.CONTACT.ACTION.FINGER
        rde.contact.actuator = [[ifinger]]
        rde.contact.phalanx = [[iphalanx]]


        this.emit(rde);
    }

    sendDirection(direction) {
        let v = direction == 1 ? -50 : 50;
        let rde = new RawDataEvent();
        rde.context = [rde.CONTEXT.AIR, rde.CONTEXT.AIR];
        rde.parameters.amplitude
        this.emit(rde);
    }

    sendDirectionInTheAir(newVal, prevVal) {
        let direction = newVal - prevVal
        let rde = new RawDataEvent();

        if (direction > 0) {
            rde.action = rde.ACTION.FLEXION;
        } else {
            rde.action = rde.ACTION.EXTENSION;
        }

        rde.context = [rde.CONTEXT.AIR, rde.CONTEXT.AIR]
        rde.actuator = [[rde.FINGER.INDEX]]


        rde.parameters.amplitude = {
            start: prevVal,
            end: newVal
        }





        this.emit(rde);
    }

    //override close method
    close() {
        // Close the HID device
        if (this.device) {
            // When you are done using the serial port, you can close it like this:
            if (this.device.isOpen) {

                this.device.close((err) => {
                    if (err) {
                        console.error('Error closing the serial port:', err);
                    } else {
                        console.log('Serial port closed successfully.');
                    }
                });
            }
        }
        clearInterval(this.reconnectInterval);
    }
}

module.exports = FlexAndVelostat;
