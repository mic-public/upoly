// import Device from "./Device.js";
// import RawDataEvent from "../RawDataEvent.js";
// import { SerialPort } from 'serialport'
const Device = require("../Device.js");
const RawDataEvent = require("../../RawDataEvent.js");
const { SerialPort } = require('serialport')


/*

1 byte communication protocol

this is a device composed of 3 touch sensor
each sensor is encoded one 1 bit, so only 3 bits are used in byte

*/

class IndexMiddle6Phalanx extends Device {
    //////////////////////////////////// STATIC PROTOCOL //////////////////////////////////////
    //If you use a standard protocol that require a port ( serial, bluetooth, wifi, rawhid, etc...)
    // You can define a protocol here.
    // This will allow the user to choose the port in the UI, and give it to the constructor port variable.
    // static protocol = Device.PROTOCOL.SERIAL;
    // static protocol = Device.PROTOCOL.BLUETOOTH;
    // static protocol = Device.PROTOCOL.HID;        // W.I.P
    // static protocol = Device.PROTOCOL.WIFI;       // W.I.P
    /////////////////////////////
    static protocol = Device.PROTOCOL.SERIAL;
    //add a constructor with em argument
    constructor(index, am, port = null) {
        //call the constructor of the parent class
        super(index, "Index Middle 6 phalanx", am);
        this.description = "3 buttons aligned on the index, 3 buttons aligned on the middle"

        this.openPort(port);

        // save the memory of the sensor value, in order to send only if it change
        this.listOfTouch = [false, false, false]


        //call autoReconnect every 2 seconds
        this.reconnectInterval = undefined
        // this.reconnectInterval = setInterval(() => {
        //     this.autoReconnect();
        // }, 5000);


    }


    autoReconnect() {
        if (!this.isOpen) {
            this.restartConnection();
        }
    }

    restartConnection() {


        if (!this.isOpen) {
            this.openPort(this.port);
        } else {
            this.close();
            setTimeout(() => {
                this.openPort(this.port);
            }, 1000);
        }


    }

    openPort(port = null) {
        // Create a port with error callback
        //let finalPath = port ? port : "/dev/tty.usbmodem22301";  // check if p is null. If yes, give default value
        let finalPath = port ? port : "COM3";
        console.log("try to open port : " + finalPath);
        let promiseDevice = this.createSerialUsbDevice(9600);

        promiseDevice.then(() => {

            // Événement déclenché à chaque réception de données série
            this.device?.on('data', (data) => {
                // Convertir les données binaires en tableau de valeurs
                //check that data is one byte long
                //get last byte of buffer into a new variable
                let v = data[data.length - 1];

                //console.log(v);

                for (let i = 0; i < 6; i++) {
                    let newTouch = this.readBit(i, v);  // result is a boolean
                    console.log("new touch: " + newTouch);
                    this.consoleUI(newTouch ? "touch" : "---" + i)

                    if (newTouch != this.listOfTouch[i]) {

                        this.sendTouch(newTouch, Math.trunc(i / 3), i % 3);
                        //Math.trunc means 0,1,2 in finger 0, then 3,4,5 in finger 1
                        //console.log("touch " + i + " is " + newTouch);
                    }
                    this.listOfTouch[i] = newTouch;
                }

            });

        });


    }

    // Function to read a specific bit at a given position
    readBit(bitIndex, byteValue) {
        // Use bitwise shift and AND operation to read the bit
        return (byteValue & (1 << bitIndex)) !== 0;
    }




    sendTouch(isTouching, ifinger, iphalanx) {

        let rde = new RawDataEvent();

        let ctx = isTouching ? [rde.CONTEXT.AIR, rde.CONTEXT.CONTACT] : [rde.CONTEXT.CONTACT, rde.CONTEXT.AIR];
        let action = isTouching ? rde.ACTION.FLEXION : rde.ACTION.EXTENSION;
        //finger and phalanx are number here. convert them to string
        switch (ifinger) {
            case 0:
                ifinger = rde.FINGER.INDEX;
                break;
            case 1:
                ifinger = rde.FINGER.MIDDLE;
                break;
            case 2:
                ifinger = rde.FINGER.RING;
                break;
        }
        switch (iphalanx) {
            case 0:
                iphalanx = rde.PHALANX.SEGMENT.BASE
                break;
            case 1:
                iphalanx = rde.PHALANX.SEGMENT.MIDDLE
                break;
            case 2:
                iphalanx = rde.PHALANX.SEGMENT.TIP
                break;
        }

        rde.actuator = [[rde.FINGER.THUMB]];
        rde.contact.action = "Contact";
        rde.contact.actuator = [[ifinger]];
        rde.contact.phalanx = [[{ segment: iphalanx, side: rde.PHALANX.SIDE.ANY }]]
        rde.action = action;
        rde.context = ctx;

        this.emit(rde);
    }

    //override close method
    close() {
        // Close the HID device
        if (this.device) {
            // When you are done using the serial port, you can close it like this:
            if (this.device.isOpen) {

                this.device.close((err) => {
                    if (err) {
                        console.error('Error closing the serial port:', err);
                    } else {
                        console.log('Serial port closed successfully.');
                    }
                });
            }
        }
        clearInterval(this.reconnectInterval);
    }
}

module.exports = IndexMiddle6Phalanx;
