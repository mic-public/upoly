

const Device = require("../Device.js");
const RawDataEvent = require("../../RawDataEvent.js");

// Import the mediapipe hand solution and its requirements
const child_process = require('child_process');

// This function will output the lines from the script 
// AS is runs, AND will return the full combined output
// as well as exit code when it's done (using the callback).



class Hololens extends Device {
    static protocol = Device.PROTOCOL.WEBSOCKET;
    static defaultPort = 8081;
    static allowPortChoice = false;
    //add a constructor with em argument
    constructor(index, am) {
        //call the constructor of the parent class
        super(index, "Hololens", am);
        this.description = "Hololens device";


        this.openPort();


    }

    openPort() {



        this.port = Hololens.defaultPort;

        let promiseDevice = this.createDevice()

        promiseDevice.then(() => {

            this.device.on('data', (data) => {
                this.process_glyph(data);
            });
        }).catch((e) => {
            console.error(e);
        });


    }


    restartConnection() {

    }

    close() {

        this.device.close();
    }

    process_glyph(data) {
        let rde = RawDataEvent.fromGlyph(data);
        this.consoleUI("action=" + rde.action + " | context=" + rde.context + " | actuator=" + rde.actuator + " | phalanx=" + rde.contact.phalanx);

        this.emit(rde);
    }


}

module.exports = Hololens;