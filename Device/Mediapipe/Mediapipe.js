

const Device = require("../Device.js");
const RawDataEvent = require("../../RawDataEvent.js");

// Import the mediapipe hand solution and its requirements
const child_process = require('child_process');
const Socketio = require('socket.io');

// This function will output the lines from the script 
// AS is runs, AND will return the full combined output
// as well as exit code when it's done (using the callback).



class Mediapipe extends Device {
    //add a constructor with em argument
    constructor(index, am) {
        //call the constructor of the parent class
        super(index, "Mediapipe", am);

        this.device = null;
        this.child = null;
        this.socket = null;

        this.openPort(8081);

        this.startPythonServer();
    }

    openPort(port) {

        try {
            this.device = Socketio(port);

            this.device.on('connection', socket => {
                // // either with send()
                console.log('Mediapipe python client connected !');
                this.setOpen(true); // inform the parent class that the device is connected
                this.socket = socket;

                socket.on('disconnect', () => {
                    console.log('Client disconnected !');
                    this.setOpen(false); // inform the parent class that the device is disconnected
                });

                socket.on('data', (data) => {
                    this.process_glyph(data);
                });
            });
        } catch (e) {
            console.error("Error Mediapie opening port: " + e);




        }

    }


    async startPythonServer() {
        this.consoleUI("Starting mediapipe server");
        this.run_script('python3', [__dirname + '/python/main.py', '-window', '-connect'], "Mediapipe server");
        console.log("Python script runs in background")
    }

    restartConnection() {
        if (this.child) {
            this.child.kill();
        }

        this.startPythonServer();
    }

    close() {

        if (this.child) {
            this.child.kill();
        }
        if (this.socket) {
            this.socket.disconnect(0);
        }
        if (this.device) {
            this.device.close();
        }
    }

    process_glyph(data) {
        let rde = RawDataEvent.fromGlyph(data);
        this.consoleUI("action=" + rde.action + " | context=" + rde.context + " | actuator=" + rde.actuator + " | phalanx=" + rde.contact.phalanx);

        this.emit(rde);
    }

    run_script(command, args, console_prefix = "") {
        console.log("Starting Process.");
        //console.log(process.execPath);
        //console.log(process.env.PATH)
        console.log("Command: " + command);
        console.log("Arguments: " + args);
        //try catch block to catch any errors
        // this.child = child_process.spawn(command, args, {
        //     shell: true,
        //     cwd: process.resourcesPath + "/exe",
        //     env: {
        //         PATH: process.env.PATH + ":" + process.resourcesPath + "/exe",
        //     }
        // });
        this.child = child_process.spawn(command, args, {
            shell: true,
            cwd: __dirname + '/python',
        });

        var scriptOutput = "";
        console_prefix = console_prefix + " ";

        this.child.stdout.setEncoding('utf8');
        this.child.stdout.on('data', (data) => {
            console.log(console_prefix + 'stdout: ' + data);
            this.consoleUI('OUT: ' + data);

            data = data.toString();
            scriptOutput += data;
        });

        this.child.stderr.setEncoding('utf8');
        this.child.stderr.on('data', (data) => {
            console.log(console_prefix + 'stderr: ' + data);
            this.consoleUI('ERROR: ' + data);

            data = data.toString();
            scriptOutput += data;
        });

        this.child.on('error', (error) => {
            console.log(console_prefix + '***process error: ' + error.message);
            setTimeout(() => {
                this.consoleUI('TERMINAL ERROR: ' + error.message);
            }, 2000);

        });
    }
}

module.exports = Mediapipe;