import math
import os, psutil
import cv2
import mediapipe as mp
import numpy as np
import time

import matplotlib
from mediapipe_landmarker import MediapipeWebcamLandmarker, PyMediapipeVisualizer
from micro_glyph_client import MicroGlyphClient
from micro_glyph_detector import MicroGlyphDetector

matplotlib.use('Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D 

active = True
running_windowed = False

#!/usr/bin/env python
import signal
import sys

def signal_handler(sig, frame):
    global active
    active = False
    sys.exit(0)

def main():

    microGlyphClient = MicroGlyphClient()

    if websocket_connect :
        microGlyphClient.connect()

    signal.signal(signal.SIGINT, signal_handler)

    tracker = MediapipeWebcamLandmarker()
    glyph_detector = MicroGlyphDetector()

    # ## CALIBRATION ###
    glyph_detector.calibrate(tracker)

    last_time = 0

    #while active and ((not running_windowed and microGlyphClient.connected) or running_windowed):
    while active :
        
        hands = tracker.get_hands()

        is_relevant = glyph_detector.detection_is_revelant(hands)

        if is_relevant :
            # newTime = time.time() 
            # fps = 1.0 / (newTime - last_time)
            # last_time = newTime
            # pass
            events = glyph_detector.get_micro_glyph_events(hands)

            if websocket_connect :
                if events != None : 
                    microGlyphClient.emit_events(events)
            else :
                if events != None :
                    print("")
                    print(events)
                    pass
        
        if running_windowed :
            # # Transform the hands for better visualization
            # visualizer.view_hands(hands)
            # # Visualize the tracked hands
            # visualizer.clear()
            # visualizer.setGraphLimits()
            # image = visualizer.draw_hands()
            # cv2.imshow("Video",image)

            webcam_feedback = tracker.direct_draw_landmarks(is_relevant)

            cv2.imshow("Webcam",webcam_feedback)
            cv2.waitKey(1)
    
def title_terminal(title):
    if running_windowed : # If runned in a terminal, print the title
        size = os.get_terminal_size().columns
    else: # If runned as a child_process of node.js
        size = 80
    half_column_size = int(np.floor(size/2))

    if len(title)%2==0:
        length_first = len(title)/2
        length_second = len(title)/2
    else :
        length_first = int(len(title)/2)
        length_second = int(len(title)/2)+1
        
    print(f"#"*size)
    print(f"#" + " "*(half_column_size-length_first-1) + title + " "*(half_column_size-length_second-1)+"#")
    print(f"#"*size)

if __name__ == "__main__":
    # running_windowed = True
    # running_from = psutil.Process(os.getpid()).parent().name().split(sep='.')[0]
    # if running_from == 'node':
    #     running_windowed = False
    import sys

    args = sys.argv[1:]
    if "-window" in args:
        running_windowed = True
    else:
        running_windowed = False

    if"-connect" in args:
        websocket_connect = True
    else:
        websocket_connect = False
    main()