#!/usr/bin/env python3

import socketio
from micro_glyph import MicroGlyphEvent


class MicroGlyphClient():
    def __init__(self, host='localhost', port='8081'):
        self.host = host
        self.port = port
        self.isRunning = False
        
        self.sio = socketio.Client()
        self.sio.on('connect', self.on_connect)
        self.sio.on('disconnect', self.on_disconnect)
        self.connected = False

    def on_connect(self):
        print('Connection established with the server')
        self.connected = True

    def on_disconnect(self):
        print('Disconnected with the server')
        self.connected = False
    
    def connect(self):
        self.sio.connect(f"http://{self.host}:{self.port}")
    
    def disconnect(self):
        self.sio.disconnect()
    
    #######################################################

    def emit(self, event: MicroGlyphEvent):
        self.sio.emit('data', event.to_glyph())

    def emit_events(self, events):
        for event in events:
            self.emit(event)
