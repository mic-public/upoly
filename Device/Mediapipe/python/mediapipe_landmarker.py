
import dataclasses
import math
import os
import time
import cv2
from matplotlib import pyplot as plt
import mediapipe as mp
import numpy as np

from typing import List, Mapping, Optional, Tuple, Union
from mediapipe.tasks.python.components.containers.landmark import NormalizedLandmark

# import matplotlib

# matplotlib.use('Agg')
# import matplotlib.pyplot as plt
# from mpl_toolkits.mplot3d import Axes3D

WRIST = 0
THUMB_CARPAL = 1
THUMB_METACARPAL = 2
THUMB_PROXIMAL = 3
THUMB_DISTAL = 4
INDEX_METACARPAL = 5
INDEX_PROXIMAL = 6
INDEX_INTERMEDIATE = 7
INDEX_DISTAL = 8
MIDDLE_METACARPAL = 9
MIDDLE_PROXIMAL = 10
MIDDLE_INTERMEDIATE = 11
MIDDLE_DISTAL = 12
RING_METACARPAL = 13
RING_PROXIMAL = 14
RING_INTERMEDIATE = 15
RING_DISTAL = 16
PINKY_METACARPAL = 17
PINKY_PROXIMAL = 18
PINKY_INTERMEDIATE = 19
PINKY_DISTAL = 20
INDEX_MIDDLE = 21
MIDDLE_RING = 22
RING_PINKY = 23

LEFT="Left"
RIGHT="Right"

from mediapipe import solutions
from mediapipe.framework.formats import landmark_pb2
import numpy as np

MARGIN = 10  # pixels
FONT_SIZE = 1
FONT_THICKNESS = 1
HANDEDNESS_TEXT_COLOR = (88, 205, 54) # vibrant green

def draw_landmarks_on_image(rgb_image, detection_result, is_relevant: bool = True):
  hand_landmarks_list = detection_result.hand_landmarks
  handedness_list = detection_result.handedness
  annotated_image = np.copy(rgb_image)

  # Loop through the detected hands to visualize.
  for idx in range(len(hand_landmarks_list)):
    hand_landmarks = hand_landmarks_list[idx]
    handedness = handedness_list[idx]

    # Draw the hand landmarks.
    hand_landmarks_proto = landmark_pb2.NormalizedLandmarkList()
    hand_landmarks_proto.landmark.extend([
        landmark_pb2.NormalizedLandmark(x=landmark.x, y=landmark.y, z=landmark.z) for landmark in hand_landmarks
    ])
    solutions.drawing_utils.draw_landmarks(
        annotated_image,
        hand_landmarks_proto,
        solutions.hands.HAND_CONNECTIONS,
        solutions.drawing_utils.DrawingSpec(color=(0, 255, 0), thickness=2, circle_radius=1),  # Change the color to green
        solutions.drawing_styles.get_default_hand_connections_style())

    # Get the top left corner of the detected hand's bounding box.
    height, width, _ = annotated_image.shape
    x_coordinates = [landmark.x for landmark in hand_landmarks]
    y_coordinates = [landmark.y for landmark in hand_landmarks]
    text_x = int(min(x_coordinates) * width)
    text_y = int(min(y_coordinates) * height) - MARGIN

    # Draw handedness (left or right hand) on the image.
    # cv2.putText(annotated_image, f"{handedness[0].category_name}",
    #             (text_x, text_y), cv2.FONT_HERSHEY_DUPLEX,
    #             FONT_SIZE, HANDEDNESS_TEXT_COLOR, FONT_THICKNESS, cv2.LINE_AA)
    
    if not is_relevant :
        cv2.putText(annotated_image, "Not relevant",
                    (text_x, text_y+20), cv2.FONT_HERSHEY_DUPLEX,
                    FONT_SIZE, (0, 0, 255), FONT_THICKNESS, cv2.LINE_AA)

  return annotated_image

_PRESENCE_THRESHOLD = 0.5
_VISIBILITY_THRESHOLD = 0.5
_BGR_CHANNELS = 3

WHITE_COLOR = (224, 224, 224)
BLACK_COLOR = (0, 0, 0)
RED_COLOR = (0, 0, 255)
GREEN_COLOR = (0, 128, 0)
BLUE_COLOR = (255, 0, 0)
@dataclasses.dataclass
class DrawingSpec:
  # Color for drawing the annotation. Default to the white color.
  color: Tuple[int, int, int] = WHITE_COLOR
  # Thickness for drawing the annotation. Default to 2 pixels.
  thickness: int = 2
  # Circle radius. Default to 2 pixels.
  circle_radius: int = 2

def _normalized_to_pixel_coordinates(
    normalized_x: float, normalized_y: float, image_width: int,
    image_height: int) -> Union[None, Tuple[int, int]]:
  """Converts normalized value pair to pixel coordinates."""

  # Checks if the float value is between 0 and 1.
  def is_valid_normalized_value(value: float) -> bool:
    return (value > 0 or math.isclose(0, value)) and (value < 1 or
                                                      math.isclose(1, value))

  if not (is_valid_normalized_value(normalized_x) and
          is_valid_normalized_value(normalized_y)):
    # TODO: Draw coordinates even if it's outside of the image bounds.
    return None
  x_px = min(math.floor(normalized_x * image_width), image_width - 1)
  y_px = min(math.floor(normalized_y * image_height), image_height - 1)
  return x_px, y_px

class MediapipeLandmarker():

    def __init__(self, mode=mp.tasks.vision.RunningMode.LIVE_STREAM, maxHands=2, detectionCon=0.5,presenceCon=0.5,trackCon=0.5):
        self.result = mp.tasks.vision.HandLandmarkerResult
        self.landmarker = mp.tasks.vision.HandLandmarker

        # callback function
        def update_result(result: mp.tasks.vision.HandLandmarkerResult, output_image: mp.Image, timestamp_ms: int):                    
            self.result = result
            # Mediapipe thumb position correction
            # We pull the finger position closer to its detected position
            # to avoid the thumb being detected on the fingers whereas it is not
            
            for hand_landmarks in result.hand_landmarks :  
                hand_landmarks[THUMB_DISTAL].z = hand_landmarks[THUMB_DISTAL].z - (0.0)

        # HandLandmarkerOptions (details here: https://developers.google.com/mediapipe/solutions/vision/hand_landmarker/python#live-stream)
        # Get directory of the current file
        landmarker_path = os.path.dirname(__file__) + '/HandLandmarker.task' # path to model
        options = mp.tasks.vision.HandLandmarkerOptions( 
            base_options = mp.tasks.BaseOptions(model_asset_path=landmarker_path),
            running_mode = mode, # running on a live stream
            num_hands = maxHands, # track both hands
            min_hand_detection_confidence = detectionCon, # lower than value to get predictions more often
            min_hand_presence_confidence = presenceCon, # lower than value to get predictions more often
            min_tracking_confidence = trackCon, # lower than value to get predictions more often
            result_callback=update_result)
        
        # initialize landmarker
        self.landmarker = self.landmarker.create_from_options(options)
    
    def detect_async(self, opencv_image):
        # convert np frame to mp image
        mp_image = mp.Image(image_format=mp.ImageFormat.SRGB, data=opencv_image)
        # detect landmarks
        self.landmarker.detect_async(image = mp_image, timestamp_ms = int(time.time() * 1000))

    def landmark_finder(self, opencv_image):
        imageRGB = cv2.cvtColor(opencv_image,cv2.COLOR_BGR2RGB)
        self.detect_async(imageRGB)

        # With async, the result treated here may be the one 
        # from a previous call of landmark_finder
        return self.result

    def close(self):
        # close landmarker
        self.landmarker.close()

    def draw_landmark(self, image: np.ndarray, landmark : landmark_pb2.NormalizedLandmark, color: Tuple[int, int, int] = (0, 0, 255), thickness: int = 2, radius: int = 5):
        """Draws the landmarks and the connections on the image.

        Args:
            image: A three channel BGR image represented as numpy ndarray.
            landmark: A normalized landmark proto message to be annotated on
            the image.
        Raises:
            ValueError: If the input image is not three channel BGR.
        """
        if image.shape[2] != _BGR_CHANNELS:
            raise ValueError('Input image must contain three channel bgr data.')
        image_rows, image_cols, _ = image.shape

        # if ((landmark.HasField('visibility') and
        #         landmark.visibility < _VISIBILITY_THRESHOLD) or
        #     (landmark.HasField('presence') and
        #         landmark.presence < _PRESENCE_THRESHOLD)):
        landmark_px = _normalized_to_pixel_coordinates(landmark.x, landmark.y,
                                                    image_cols, image_rows)

        # Draws landmark points after finishing the connection lines, which is
        # aesthetically better.
        drawing_spec = DrawingSpec(color=color, thickness=thickness, circle_radius=radius)
        # White circle border
        circle_border_radius = max(drawing_spec.circle_radius + 1,
                                    int(drawing_spec.circle_radius * 1.2))
        # cv2.circle(image, landmark_px, circle_border_radius, drawing_spec.color,
        #             drawing_spec.thickness)
        # Fill color into the circle
        cv2.circle(image, landmark_px, drawing_spec.circle_radius,
                    drawing_spec.color, drawing_spec.thickness)
        return image

class MediapipeWebcamLandmarker(MediapipeLandmarker):
    detected_hands = []
    hands_list = []
    max_size_hands_list = 5

    def __init__(self, maxHands=2, detectionCon=0.9, presenceCon=0.9, trackCon=0.5):
        super().__init__(mp.tasks.vision.RunningMode.LIVE_STREAM, maxHands, detectionCon, presenceCon, trackCon)
    
        self.cap = cv2.VideoCapture(0)
        if not self.cap.isOpened():
            print("Cannot open camera")
            exit()
        self.cap.set(cv2.CAP_PROP_FPS, 30)  # Set the FPS capture of the webcam to 30
        success,self.image = self.cap.read()
        self.height,self.width,c = self.image.shape
    
    def get_image_size(self):
        return self.height,self.width

    def direct_draw_landmarks(self, is_relevant: bool = True):
        annotated_image = np.copy(self.image)
        if hasattr(self.mp_results, 'hand_landmarks') and self.mp_results.hand_landmarks != [] :
            annotated_image = draw_landmarks_on_image(annotated_image, self.mp_results, is_relevant)
        # self.drawer.draw_landmarks(self.image, handLms, self.drawer.HAND_CONNECTIONS)
        return annotated_image
    
    #####################################################################
    
    def get_hands(self):
        success,self.image = self.cap.read()
        if(not success):
            print("Failed to read from camera")
            return []
        
        reduced_image = cv2.resize(self.image, (0, 0), fx=0.1, fy=0.1)

        self.mp_results = self.landmark_finder(reduced_image)
    
        if hasattr(self.mp_results, 'hand_landmarks') and self.mp_results.hand_landmarks != [] :
            self.detected_hands = self.optimize_results(self.mp_results)
        else :
            self.detected_hands = []
        return self.detected_hands
    
    def optimize_results(self, mp_results) :
        self.update_detected_hands_list(mp_results)
        # mp_results = self.compute_mean_detected_hands()
        mp_results = self.compute_mean_mp_results()
        
        return mp_results
    
    def update_detected_hands_list(self, mp_results) :
        if len(self.hands_list) == self.max_size_hands_list :
            self.hands_list.pop(0)
        self.hands_list.append(mp_results)

    def compute_mean_mp_results(self) :
        if len(self.hands_list) == 0 :
            return None
        else :
            mean_hands = []
            mp_results = self.hands_list[0]
            
            for hand_index, hand in enumerate(mp_results.hand_landmarks) :
                if hand == [] :
                    mean_hand_landmarks = [NormalizedLandmark(0, 0, 0) for i in range(21)]
                else :
                    mean_hand_landmarks = hand
    
                    # compute the mean_detected_hands
                    for i in range(1, len(self.hands_list)) :
                        for previous_hand_index, previous_hand in enumerate(self.hands_list[i].hand_landmarks) :
                            if previous_hand != [] :
                                for landmark_index, landmark in enumerate(previous_hand):
                                    mean_hand_landmarks[landmark_index].x += landmark.x
                                    mean_hand_landmarks[landmark_index].y += landmark.y
                                    mean_hand_landmarks[landmark_index].z += landmark.z
        
                                    if i == len(self.hands_list) - 1 :
                                        mean_hand_landmarks[landmark_index].x /= len(self.hands_list)
                                        mean_hand_landmarks[landmark_index].y /= len(self.hands_list)
                                        mean_hand_landmarks[landmark_index].z /= len(self.hands_list)
                mean_hands.append(mean_hand_landmarks)

            return mean_hands

class PyMediapipeVisualizer():
    already_waiting = False

    def __init__(self, height, width):
        self.fig = plt.figure()
        self.fig.set_size_inches(float(height) / self.fig.dpi, float(width) / self.fig.dpi, forward=True)
        self.ax = self.fig.add_subplot(111, projection='3d')
        self.color_hand_joints = [[1.0, 0.0, 0.0],
                                  [0.0, 0.4, 0.0], [0.0, 0.6, 0.0], [0.0, 0.8, 0.0], [0.0, 1.0, 0.0],  # thumb
                                  [0.0, 0.0, 0.6], [0.0, 0.0, 1.0], [0.2, 0.2, 1.0], [0.4, 0.4, 1.0],  # index
                                  [0.0, 0.4, 0.4], [0.0, 0.6, 0.6], [0.0, 0.8, 0.8], [0.0, 1.0, 1.0],  # middle
                                  [0.4, 0.4, 0.0], [0.6, 0.6, 0.0], [0.8, 0.8, 0.0], [1.0, 1.0, 0.0],  # ring
                                  [0.4, 0.0, 0.4], [0.6, 0.0, 0.6], [0.8, 0.0, 0.8], [1.0, 0.0, 1.0]]  # little
        
    def calibrate(self, tracker) :
        # Getting calibration positions
        index_calibration = 0
        calibrations = []
        while index_calibration < 10 : # 10 calibrations
            success,image = tracker.cap.read()
            mp_results = tracker.landmark_finder(image)

            hands = transform_mp_results(mp_results)

            if hands[LEFT]==[] and hands[RIGHT]==[] :
                if not self.already_waiting or index_calibration!=0:
                    print("Wainting for a hand to be detected")
                    self.already_waiting = True
                    index_calibration = 0
            else :
                if hands[LEFT]==[] : handedness = RIGHT
                else : handedness = LEFT
                calibrations.append(hands[handedness])
                index_calibration+=1
                print("Calibrations done : {0}".format(index_calibration))
        print("Calibration done !")
        
        # Computing calibrated landmark constants
        calibrated_hand = compute_calibrations(calibrations)
        self.hand_constantsc= compute_constants(calibrated_hand)

    def view_hands(self, hands) :
        new_hands = transform_hands(hands)
        self.hands = {LEFT : [], RIGHT : []}

        for handedness in new_hands:
            if new_hands[handedness]!=[] and self.hands[handedness]!=[] :
                new_hands[handedness] = translateSpace(new_hands[handedness])
                new_hands[handedness] = rotateSpace(new_hands[handedness], handedness)
                # new_hands[handedness] = scaleSpace(new_hands[handedness])
                new_hands[handedness] = [x.tolist() for x in new_hands[handedness]] # Converting to list
                self.hands[handedness] = [[np.round(x,2) for x in y] for y in new_hands[handedness]] # Rounding to 2 decimals

        self.hands = new_hands  
    
    def draw_hands(self) :
        for hand in self.hands:
            if self.hands[hand]!=[] : 
                self.draw_3d_skeleton(self.hands[hand], self.ax)
        
        ret = self.fig2data()  # H x W x 4
        return ret


    def draw_3d_skeleton(self, landmarks, ax):
        """
        :param landmarks: 21 x 3
        :param ax:
        :return:
        """
        marker_sz = 15
        line_wd = 2
        for joint_ind in range(len(landmarks)):
            ax.plot(landmarks[joint_ind][0], landmarks[joint_ind][1], landmarks[joint_ind][2], 
                '.', c=self.color_hand_joints[joint_ind], markersize=marker_sz)
    
    def fig2data(self):
        """
        @brief Convert a Matplotlib figure to a 4D numpy array with RGBA channels and return it
        @param fig a matplotlib figure
        @return a numpy 3D array of RGBA values
        """
        self.fig.canvas.draw()

        # convert canvas to image
        graph_image = np.array(self.fig.canvas.get_renderer()._renderer)

        # it still is rgb, convert to opencv's default bgr
        graph_image = cv2.cvtColor(graph_image,cv2.COLOR_RGB2BGR)
        return graph_image

    def clear(self) :
        self.ax.clear()
    
    def setGraphLimits(self):
        self.ax.set_xlabel('X')
        self.ax.set_ylabel('Y')
        self.ax.set_zlabel('Z')
        # self.ax.axes.set_xlim3d(left=0, right=1, auto=False) 
        # self.ax.axes.set_ylim3d(bottom=-1, top=1, auto=False) 
        # self.ax.axes.set_zlim3d(bottom=0, top=0.1, auto=False) 
        # self.ax.set_box_aspect((1, 1, 1))

################################################################# 
##################       CALIBRATION      #######################
#################################################################
        
def transform_mp_results(mp_results) :
    hands = {LEFT : [], RIGHT : []}

    if hasattr(mp_results, 'hand_landmarks') and mp_results.hand_landmarks != [] :
        # Convert to the visualization format
        for i in range(len(mp_results.hand_landmarks)) :
            if mp_results.hand_landmarks[i] != [] :
                points = []
                for landmark in mp_results.hand_landmarks[i] :
                    points.append([landmark.x, landmark.y, landmark.z])

                if mp_results.handedness[i][0].category_name == LEFT :
                    hands[LEFT].append(points)
                else :
                    hands[RIGHT].append(points)

    return hands

def transform_hands(hands) :
    new_hands = {LEFT : [], RIGHT : []}

    for hand in hands :
        if hand!=[] :
            for i in range(len(hand)) :
                landmark = hand[i]
                points = [landmark.x, landmark.y, landmark.z]
                new_hands[LEFT].append(points)

    return new_hands
        
def compute_calibrations(calibrations) :
    calibrated_hand = [[],[],[]]
    transposed_calibrations = np.array(calibrations).T
    for i in range(3) : # 3 coordinates
        for lmd in range(len(transposed_calibrations[i])) :
            calibrated_hand[i].append(np.mean(transposed_calibrations[i][lmd]))
    return np.array(calibrated_hand).T

def compute_constants(calibrated_hand) :
    constants = {WRIST : 0}
    # Compute the length of each bone of the fingers
    for bone in range(1,NB_BONES_PER_FINGER+1) :
        for finger in range(5) : # 5 fingers
            if bone!=1 :
                bone_length = calibrated_hand[bone+(finger*NB_BONES_PER_FINGER)] - calibrated_hand[bone-1+(finger*NB_BONES_PER_FINGER)]
            else :
                bone_length = calibrated_hand[bone+(finger*NB_BONES_PER_FINGER)] - calibrated_hand[WRIST]
            constants[bone+(finger*NB_BONES_PER_FINGER)] = int(np.linalg.norm(bone_length))
            
    # Compute the distance between  the basis of the fingers
    constants[INDEX_MIDDLE] = int(np.linalg.norm(calibrated_hand[MIDDLE_METACARPAL] - calibrated_hand[INDEX_METACARPAL]))
    constants[MIDDLE_RING] = int(np.linalg.norm(calibrated_hand[RING_METACARPAL] - calibrated_hand[MIDDLE_METACARPAL]))
    constants[RING_PINKY] = int(np.linalg.norm(calibrated_hand[PINKY_METACARPAL] - calibrated_hand[RING_METACARPAL]))
    
    return constants

#################################################################
########       MATH UTILS FOR PYTHON VISUALIZATION      #########
#################################################################

NB_BONES_PER_FINGER = 4

def translateSpace(positions):
    # Computes and applies the translation to the origin
    space_translation_vector = -np.array([positions[WRIST]]).T # The translation to the origin puts the wrist at (0,0,0)
    return apply_translation(positions, space_translation_vector)
    
def rotateSpace(positions, handedness):
    # Computes and applies the rotation of the hand axis
    z_ref = np.array([0,0,1]) # We take the z axis as our vertical axis reference to stick the hand axis on
    positions = apply_rotation(positions, np.array(positions[MIDDLE_METACARPAL]), z_ref)
    
    middle = np.array(positions[MIDDLE_METACARPAL]) # The middle position (and vector with the wrist after translation)
    pinky = np.array(positions[PINKY_METACARPAL]) # The middle position (and vector with the wrist after translation)
    if handedness==LEFT : palm_orientation_ref = pinky-middle
    else : palm_orientation_ref = middle-pinky
    palm_orientation_ref[2]=0 # We project the reference onto the middle z plane
    x_ref = np.array([1,0,0]) # We take the x axis as our side axis reference to stick the palm orientation with
    if palm_orientation_ref[1]!=0: 
        positions = apply_rotation(positions, palm_orientation_ref, x_ref)
    return positions
    
def scaleSpace(positions):
    # Computes and applies the scaling of the hand
    middle = np.array(positions[MIDDLE_METACARPAL])
    if middle[2]==0 : z_scale = 1
    else : z_scale = 10/middle[2]
    positions = apply_z_scaling(positions, z_scale)
    return positions

def apply_translation(positions, space_translation_vector) :
    space_translation_matrix = np.vstack((np.hstack((np.identity(3), space_translation_vector)), np.array([0, 0, 0, 1])))
    return [apply_transformation(pos, space_translation_matrix) for pos in positions]

def apply_rotation(positions, initial_vector_reference, new_vector_reference) :
    rot = rotation_matrix_from_vectors(initial_vector_reference, new_vector_reference)
    space_rotation_matrix = np.vstack((np.hstack((rot, np.array([[0, 0, 0]]).T)), np.array([0, 0, 0, 1])))
    positions = [apply_transformation(pos, space_rotation_matrix) for pos in positions]
    return [[round(y) for y in pos] for pos in positions] # removing the initial index in positions

def apply_z_scaling(positions, z_scale) :
    scaling_matrix = np.identity(3) * z_scale
    return [scaling_matrix.dot(pos) for pos in positions] # Applying transformation 

def apply_transformation(pos, space_transformation_matrix) :
    old_pos_array = np.hstack((pos, np.array([1])))
    new_pos_vector = space_transformation_matrix.dot(old_pos_array) # Applying transformation 
    return new_pos_vector[:-1]
   
def rotation_matrix_from_vectors(vec1, vec2):
    """ Find the rotation matrix that aligns vec1 to vec2
    :param vec1: A 3d "source" vector
    :param vec2: A 3d "destination" vector
    :return mat: A transform matrix (3x3) which when applied to vec1, aligns it with vec2.
    """
    a, b = (vec1 / np.linalg.norm(vec1)).reshape(3), (vec2 / np.linalg.norm(vec2)).reshape(3)
    v = np.cross(a, b)
    c = np.dot(a, b)
    s = np.linalg.norm(v)
    kmat = np.array([[0, -v[2], v[1]], [v[2], 0, -v[0]], [-v[1], v[0], 0]])
    rotation_matrix = np.eye(3) + kmat + kmat.dot(kmat) * ((1 - c) / (s ** 2))
    return rotation_matrix