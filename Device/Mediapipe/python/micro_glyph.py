
from datetime import datetime
from enum import auto
# from enum import StrEnum -> available with Python 3.11+
from strenum import LowercaseStrEnum, PascalCaseStrEnum

class MicroGlyphEvent :
    """
    Defines an ELEMENTARY microgesture in the µGlyph format.
    Meant to be used as a building block for more complex gestures.
    """

    def __init__(self, action=None, context=None, actuator=None, hand=None, parameters=None, contact=None) :
        if action is None :     self.action = self.ACTION.ANY
        else :                  self.action = action
        # Context : 
        # 1 value means no displacement, i.e. STAY in air or in contact
        # 2 values means displacement, e.g. MOVE from in air to in contact
        if context is None :    self.context = [self.CONTEXT.ANY]
        else :                  self.context = context
        # ONE MicroGlyphEvent is asociated to 
        # ONE finger in the current state of µPoly
        if actuator is None :   self.actuator = [[self.FINGER.ANY]]
        else :                  self.actuator = actuator
        if hand is None :       self.hand = self.HAND.ANY
        else :                  self.hand = hand

        if parameters is None : 
            # Ensures that the pameters are initialized for in-context events
            if len(self.context) == 1 or len(self.context) == 2 and self.context[0] == self.context[1] :
                if len(self.context) == 1 :
                    # No movement -> either pressure or pause by default, depends on the type of action
                    if self.action == self.ACTION.NO_MOVEMENT :
                        self.parameters = self.Parameters(time=self.Parameters.Time(2)) # 2 seconds pause by default
                    else :
                        self.parameters = self.Parameters(pressure=self.Parameters.Pressure(0,100))
                else :
                    # Movement in the same context -> either flexion or swipe by default, depends on the type of action
                    self.parameters = self.Parameters(amplitude=self.Parameters.Amplitude(0,100))
            else :
                # Default initialization
                self.parameters = self.Parameters()
        else :                  self.parameters = parameters
        if contact is None :    
            self.contact = self.Contact()
        else :  self.contact = contact

        self.timestamp = datetime.now()

    class ACTION(PascalCaseStrEnum) :
        FLEXION = auto()
        EXTENSION = auto()
        ADDUCTION = auto()
        ABDUCTION = auto()
        CIRCLES = auto()
        SQUARES = auto()
        TRIANGLES = auto()
        ZIGZAG = auto()
        NO_MOVEMENT = auto()
        ANY = auto()
        
        def __str__(self):
            return self.name

    class CONTEXT(PascalCaseStrEnum) :
        CONTACT = auto()
        AIR = auto()
        ANY = auto()
        
        def __str__(self):
            return self.name
    
    class HAND(PascalCaseStrEnum) :
        LEFT = auto()
        RIGHT = auto()
        ANY = auto()
        
        def __str__(self):
            return self.name

    class FINGER(LowercaseStrEnum) :
        INDEX = auto()
        MIDDLE = auto()
        RING = auto()
        PINKY = auto()
        THUMB = auto()
        ANY = auto()
        
        def __str__(self):
            return self.name
    
    class PHALANX(LowercaseStrEnum) :
        TIP = auto()
        MIDDLE = auto()
        BASE = auto()
        ANY = auto()
        
        def __str__(self):
            return self.name

    class Parameters :
        def __init__(self, pressure=None, amplitude=None, time=None) :
            if pressure is None :
                pressure = self.Pressure()
            if amplitude is None :
                amplitude = self.Amplitude()
            if time is None :
                time = self.Time()

            self.pressure = pressure
            self.amplitude = amplitude
            self.time = time

        def __str__(self) -> str:
            string = f"Parameters("
            if self.pressure!=self.Pressure() :
                string += f"action : {self.pressure}, "
            if self.amplitude!=self.Amplitude() :
                string += f"context : {self.amplitude}, "
            if self.time!=self.Time() :
                string += f"actuator : {self.time}, "
            return string[:-2] + ")"

        def __repr__(self) -> str:
            return self.__str__()
        
        def __eq__(self, o: object) -> bool:
            assert isinstance(o, self.__class__)
            return self.pressure == o.pressure and self.amplitude == o.amplitude and self.time == o.time

        class Pressure : 
            def __init__(self, start=None, end=None) :
                self.start = start
                self.end = end

            def __str__(self) -> str:
                return f"(start : {self.start}, end : {self.end})"
            
            def __repr__(self) -> str:
                return self.__str__()
            
            def __eq__(self, o: object) -> bool:
                assert isinstance(o, self.__class__)
                return self.start == o.start and self.end == o.end
            
        class Amplitude :
            def __init__(self, start=None, end=None) :
                self.start = start
                self.end = end

            def __str__(self) -> str:
                return f"(start : {self.start}, end : {self.end})"
            
            def __repr__(self) -> str:
                return self.__str__()
            
            def __eq__(self, o: object) -> bool:
                assert isinstance(o, self.__class__)
                return self.start == o.start and self.end == o.end
            
            
        class Time :
            def __init__(self, duration=None) :
                self.duration = duration

            def __str__(self) -> str:
                return f"(duration : {self.duration})"
            
            def __repr__(self) -> str:
                return self.__str__()
            
            def __eq__(self, o: object) -> bool:
                assert isinstance(o, self.__class__)
                return self.duration == o.duration
    
    class Contact :
        def __init__(self, action=None, parameters=None, actuator=None, phalanx=None) :
            self.action = action
            self.parameters = parameters

            if actuator is None :   self.actuator = [[MicroGlyphEvent.FINGER.ANY]]
            else :                  self.actuator = actuator
            if phalanx is None :    
                self.phalanx = [[{}]]
                self.phalanx[0][0]["segment"] = MicroGlyphEvent.PHALANX.ANY
                self.phalanx[0][0]["side"] = MicroGlyphEvent.PHALANX.ANY

            else :                  self.phalanx = phalanx

        def get_context(self) :
            if self.action == None and self.parameters == None and self.actuator == [[MicroGlyphEvent.FINGER.ANY]] and self.phalanx == MicroGlyphEvent.PHALANX.ANY :
                return MicroGlyphEvent.CONTEXT.AIR
            else :
                return MicroGlyphEvent.CONTEXT.CONTACT

        def __str__(self) -> str:
            string = f"Contact("
            if self.action!=None :
                string += f"action : {self.action}, "
            if self.parameters!=None :
                string += f"parameters : {self.parameters}, "
            if self.actuator!=[[MicroGlyphEvent.FINGER.ANY]] :
                act = "["+", ".join(["["+", ".join([str(c) for c in b])+"]" for b in self.actuator])+"]"
                string += f"actuator : {act}, "
            if self.phalanx!=MicroGlyphEvent.PHALANX.ANY :
                string += f"phalanx : {self.phalanx}, "

            if string == "Contact(" :
                return string + ")"
            return string[:-2] + ")"
        
        def __repr__(self) -> str:
            return self.__str__()
        
        def __eq__(self, o: object) -> bool:
            assert isinstance(o, self.__class__)
            return self.action == o.action and self.parameters == o.parameters and self.actuator == o.actuator and self.phalanx == o.phalanx

    def __str__(self) -> str:
        string = f"MicroGlyphEvent("
        if self.action!=self.ACTION.ANY :
            string += f"action : {self.action}, "
        if self.context!=[self.CONTEXT.ANY] :
            cxt = "["+", ".join([str(c) for c in self.context])+"]"
            string += f"context : {cxt}, "
        if self.actuator!=[[self.FINGER.ANY]] :
            act = "["+", ".join(["["+", ".join([str(c) for c in b])+"]" for b in self.actuator])+"]"
            string += f"actuator : {act}, "
        if self.hand!=self.HAND.ANY :
            string += f"hand : {self.hand}, "
        if self.parameters!=self.Parameters() :
            string += f"parameters : {self.parameters}, "
        if self.contact!=self.Contact() :
            string += f"contact : {self.contact}, "
        return string[:-2] + ")"
    
    def __repr__(self) -> str:
        return self.__str__()
    
    def to_glyph(self) :
        """
        A glyph is a JSON object corresponding to the following format:
        
        microgesture {
            - type : "glyph"
            - action : "Abduction"
            - context : ["Contact", "Contact"]
            - hand : "right"
            - actuator : [["index"]]
            - contact : {
                action: "Contact",
                actuator: [["index"]],
                }
            - parameters : {
                pressure : { --> if not None
                    start : low,
                    end : high
                },
                amplitude : { --> if not None
                    start : 0,
                    end : 50
                },
                time : { --> if not None
                    duration : 3
                }
            } 
        }

        Please note that the given glyph is an example
        """
        pressure = { "start" : self.parameters.pressure.start,
                     "end" : self.parameters.pressure.end }
        amplitude = { "start" : self.parameters.amplitude.start,
                      "end" : self.parameters.amplitude.end }
        time = { "duration" : self.parameters.time.duration }

        glyph = {
            "type" : "glyph",
            "action" : self.action,
            "context" : self.context,
            "hand" : self.hand,
            "actuator" : self.actuator,
            "contact" : {
                "action" : self.contact.action,
                "actuator" : self.contact.actuator,
                "phalanx" : self.contact.phalanx
            }
        }

        glyph["parameters"] = {}
        if None not in pressure.values() :
            glyph["parameters"]["pressure"] = pressure
        elif None not in amplitude.values() :
            glyph["parameters"]["amplitude"] = amplitude
        elif None not in time.values() :
            glyph["parameters"]["time"] = time
            
        return glyph
