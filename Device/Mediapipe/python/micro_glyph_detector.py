
import json
import math
import os
import numpy as np
import cv2
from mediapipe_landmarker import MediapipeWebcamLandmarker
import shapely as sh

from micro_glyph import MicroGlyphEvent
from mediapipe import solutions

HandLandmarkerResult = solutions.mediapipe.tasks.vision.HandLandmarkerResult

Mg = MicroGlyphEvent

SMOOTHING_FRAMES = 3

DISTANT = "Distant"
EXTREMELY_FARAWAY = "Extremely faraway"
VERY_FARAWAY = "Very faraway"
FARAWAY = "Faraway"
FAR = "Far"
NEARBY = "Nearby"
PROXIMATE = "Proximate"
CLOSE = "Close"
CONTACT = "Contact"

DISTANT_COLOR = 0x007BFF
EXTREMELY_FARAWAY_COLOR = 0x00FFFB
VERY_FARAWAY_COLOR = 0x00FF6F
FARAWAY_COLOR = 0x62FF00
FAR_COLOR = 0xDDFF00
NEARBY_COLOR = 0xFFFB00
PROXIMATE_COLOR = 0xFF8400
CLOSE_COLOR = 0xFF4D00
CONTACT_COLOR = 0xF00000

DEFINED_COLORS = [DISTANT_COLOR, EXTREMELY_FARAWAY_COLOR, VERY_FARAWAY_COLOR, FARAWAY_COLOR, FAR_COLOR, NEARBY_COLOR, PROXIMATE_COLOR, CLOSE_COLOR, CONTACT_COLOR]

# Define constants for eahc hand joint corresponding to mediapipe landmarks
WRIST = 0
THUMB_CMC = 1
THUMB_MCP = 2
THUMB_IP = 3
THUMB_TIP = 4
INDEX_MCP = 5
INDEX_PIP = 6
INDEX_DIP = 7
INDEX_TIP = 8
MIDDLE_MCP = 9
MIDDLE_PIP = 10
MIDDLE_DIP = 11
MIDDLE_TIP = 12
RING_MCP = 13
RING_PIP = 14
RING_DIP = 15
RING_TIP = 16
PINKY_MCP = 17
PINKY_PIP = 18
PINKY_DIP = 19
PINKY_TIP = 20

# Define constants for the flexion status of each finger
ANGLE_THRESHOLD = 120

THUMB = Mg.FINGER.THUMB
INDEX = Mg.FINGER.INDEX
MIDDLE = Mg.FINGER.MIDDLE
RING = Mg.FINGER.RING
PINKY = Mg.FINGER.PINKY

FINGERS = [INDEX, MIDDLE, RING, PINKY]

BASE = Mg.PHALANX.BASE
MIDDLE = Mg.PHALANX.MIDDLE
TIP = Mg.PHALANX.TIP

CONTACT_THRESHOLD = 0.070
CLOSE_THRESHOLD = 0.075
PROXIMATE_THRESHOLD = 0.080
NEARBY_THRESHOLD = 0.085
FAR_THRESHOLD = 0.090
FARAWAY_THRESHOLD = 0.095
VERY_FARAWAY_THRESHOLD = 0.100
EXTREMELY_FARAWAY_THRESHOLD = 0.110
DISTANT_THRESHOLD = 0.120


THRESHOLD_TICKS = 50

class MicroGlyphDetector :
    CONTACT_THRESHOLDS = { INDEX :   { TIP : CONTACT_THRESHOLD, MIDDLE : CONTACT_THRESHOLD, BASE : CONTACT_THRESHOLD },
                        MIDDLE :  { TIP : CONTACT_THRESHOLD, MIDDLE : CONTACT_THRESHOLD, BASE : CONTACT_THRESHOLD },
                        RING :    { TIP : CONTACT_THRESHOLD, MIDDLE : CONTACT_THRESHOLD, BASE : CONTACT_THRESHOLD },
                        PINKY :   { TIP : CONTACT_THRESHOLD, MIDDLE : CONTACT_THRESHOLD, BASE : CONTACT_THRESHOLD }}

    def __init__(self) :
        self.reset()

    def reset(self) :
        self.detections = []
        self.last_hand_pose = None
        self.last_hand_results = [[]]

    def detection_is_revelant(self, hands : HandLandmarkerResult) :
        """
        This function is used to determine if the detection is revelant
        It is based on the following conditions :
        - No hand detected
        - Temporization not done yet,  
            (waits a few frames after slowing down the hand)
        - The wrist moves 
            (avoids sending events when quickly moving the hand itself)
        """
        relevance = True

        # If there is no hand detected, the detection is not revelant
        no_hand_detected = ( len(hands)==0 )

        # If the temporization has not been done yet, the detection is not revelant
        # This temporization is mandatory to avoid any confusion 
        temporization_done = ( len(self.last_hand_results) > SMOOTHING_FRAMES*2 )

        # If the wrist moves, the detection is not revelant
        wrist_move = False
        if temporization_done :
            #last_hand_result = self.last_hand_results[-SMOOTHING_FRAMES]
            last_hand_result = self.last_hand_results[-SMOOTHING_FRAMES]
            n_last, n_current = len(last_hand_result), len(hands)
            if n_last > 0 and n_current > 0 and n_last == n_current :
                for i in range(n_last) :
                    if last_hand_result[i] != [] and hands[i] != [] :
                        wrist_has_changed = join_has_changed(last_hand_result[i][WRIST], hands[i][WRIST])
                        # thumb_has_changed = join_has_changed(last_hand_result[i][THUMB_CMC], hands[i][THUMB_CMC])
                        # index_has_changed = join_has_changed(last_hand_result[i][INDEX_MCP], hands[i][INDEX_MCP])
                        # middle_has_changed = join_has_changed(last_hand_result[i][MIDDLE_MCP], hands[i][MIDDLE_MCP])
                        # ring_has_changed = join_has_changed(last_hand_result[i][RING_MCP], hands[i][RING_MCP])
                        # pinky_has_changed = join_has_changed(last_hand_result[i][PINKY_MCP], hands[i][PINKY_MCP])
                        #if wrist_has_changed or thumb_has_changed or index_has_changed or middle_has_changed or ring_has_changed or pinky_has_changed :
                        if wrist_has_changed :
                            wrist_move = True
                            break

        if no_hand_detected : 
            self.reset()
            relevance = False
        elif not temporization_done :
            relevance = False
        elif wrist_move :
            relevance = False
            
        if len(self.last_hand_results) > SMOOTHING_FRAMES*2+1 :
            self.last_hand_results.pop(0)
        self.last_hand_results.append(hands)
        return relevance

    class Detection :

        def __init__(self, landmarks, thresholds) :
            self.thresholds = thresholds

            hand_pose = self.HandPose(landmarks)
            # This is used to go with Chaffangeon Caillet's work
            # We are forced to use 2D landmarks to compute 
            # the touch detection because of Mediapipe incertainties
            # but we have to correct our 2D prediction with the 
            # 3D detection to ensure there is a contact in the image depth
            contact = self.compute_thumb_contact(landmarks)

            # We estimate an event based on a "detection" made
            # of a hand pose and a set of the contacts
            # This is made possible because we do not want to detect draw events 
            self.hand_pose = hand_pose
            self.contact = contact

        def compute_thumb_contact(self, landmarks_3D) :
            landmarks_2D = compute_2D_landmarks(landmarks_3D)
            # Get the detection of the hand pose with a dictionnary
            # made of the name of the finger and the fingers it is in contact with
            contacts = []
            for finger in FINGERS :
                contact = self.thumb_in_contact(landmarks_3D, finger)
                # contact = thumb_in_contact(landmarks_2D, finger)
                if contact :
                    contacts.append(contact)

            # If the thumb is in contact with a finger or more, 
            # determine the finger that is the closest to the thumb
            if len(contacts) > 1 :
                # We evaluate contacts from different fingers,
                # involving different phalanxes
                # Detection base on one camera is not enough to determine
                # the right depth of the thumb. 
                # Hence, the priority is given to the finger that is the 
                # most opposite to the thumb
                
                # Get the most opposite finger to the thumb
                fingers_in_contact = {contact.actuator[0][0] : contact for contact in contacts}
                if PINKY in fingers_in_contact.keys() :
                    phalanx = fingers_in_contact[PINKY].phalanx[0][0]
                elif RING in fingers_in_contact.keys() :
                    phalanx = fingers_in_contact[RING].phalanx[0][0]
                elif MIDDLE in fingers_in_contact.keys() :
                    phalanx = fingers_in_contact[MIDDLE].phalanx[0][0]
                else :
                    phalanx = fingers_in_contact[INDEX].phalanx[0][0]

                # Keep only the contacts that are in the same phalanx
                contacts = [contact for contact in contacts if contact.phalanx[0][0]["segment"] == phalanx["segment"]]

            # Here we only keep ONE contact in the end. The closest.
            if len(contacts) > 0 :
                closest_finger_index = 0
                closest_contact = contacts[closest_finger_index]
                assert(isinstance(closest_contact, Mg.Contact))
                closest_finger = closest_contact.actuator[0][0]
                closest_distance = distance_to_thumb(landmarks_3D, closest_finger)[0]

                for finger_index in range(1, len(contacts)) :
                    contact = contacts[finger_index]
                    assert(isinstance(contact, Mg.Contact))
                    finger = contact.actuator[0][0] # At this point, contact.actuator = actuators. It is not as it supposed to be, but we will change it later. the real value of contact.actuator is "thumb"
                    dist = distance_to_thumb(landmarks_3D, finger)[0]

                    if dist < closest_distance :
                        closest_finger_index = finger_index
                        closest_distance = dist

                #contacts[closest_finger_index].actuator = [["thumb"]]  # If the is contact : the thumb is the receiver. In microglyph, the receiver is the actuator of the contact.
                return contacts[closest_finger_index]
            return Mg.Contact()
        
        def thumb_in_contact(self, landmarks, finger) :
            dist_part = distance_to_thumb(landmarks, finger)
            dist = dist_part[0]
            #nearest_part = dist_part[1]
            nearest_part = {}
            #create a keu-value pair for the nearest part, one called "segment", and one called "side"
            nearest_part["segment"] = dist_part[1]
            nearest_part["side"] = "any"

            # if dist < CONTACT_THRESHOLDS :
            if dist < self.thresholds[finger][dist_part[1]] :
                contact = Mg.Contact(actuator=[[finger]], phalanx=[[nearest_part]], action="Contact")   # ADD Contact, in order to be visible.
                return contact
            else :
                return False
            
        
        def get_events(self, detections) :
            events = []
            last = detections[-1]
            assert(isinstance(last, __class__))
            
            pose_before = last.hand_pose
            pose_current = self.hand_pose

            for finger in FINGERS :
                context_before = get_contact_context(last.contact, finger)
                context_current = get_contact_context(self.contact, finger)




                if context_before != context_current :
                    # The context has changed, this is a hidden flexion or hidden extension
                    # (too light to be detected by the flexion/extension detection)
                    if context_before == Mg.CONTEXT.AIR and context_current == Mg.CONTEXT.CONTACT :
                        events.append(MicroGlyphEvent(action=Mg.ACTION.FLEXION, 
                                                context=[Mg.CONTEXT.AIR, Mg.CONTEXT.CONTACT],
                                                actuator=[[THUMB]],
                                                contact=self.contact))
                    elif context_before == Mg.CONTEXT.CONTACT and context_current == Mg.CONTEXT.AIR :
                        events.append(MicroGlyphEvent(action=Mg.ACTION.EXTENSION, 
                                                context=[Mg.CONTEXT.CONTACT, Mg.CONTEXT.AIR],
                                                actuator=[[THUMB]],
                                                contact=self.contact))
                else :
                    # In this case, we ONLY look for swipes
                    if context_before == Mg.CONTEXT.CONTACT and context_current == Mg.CONTEXT.CONTACT :
                        # At this point, we need to look for the phalanx that is in contact
                        phalanx_before = last.contact.phalanx[0][0]["segment"]
                        phalanx_current = self.contact.phalanx[0][0]["segment"]

                        if phalanx_before != phalanx_current :
                            if phalanx_before == BASE and phalanx_current in [MIDDLE, TIP] or phalanx_before == MIDDLE and phalanx_current == TIP :
                                # The swipe is up
                                events.append(MicroGlyphEvent(action=Mg.ACTION.EXTENSION, 
                                                            context=[context_before, context_current],
                                                            actuator=[[THUMB]],
                                                            contact=self.contact))
                            elif phalanx_before == TIP and phalanx_current in [MIDDLE, BASE] or phalanx_before == MIDDLE and phalanx_current == BASE :
                                # The swipe is down
                                events.append(MicroGlyphEvent(action=Mg.ACTION.FLEXION, 
                                                            context=[context_before, context_current],
                                                            actuator=[[THUMB]],
                                                            contact=self.contact))

            return events
        

        def get_events_backup(self, detections) :
            events = []
            last = detections[-1]
            assert(isinstance(last, __class__))
            
            pose_before = last.hand_pose
            pose_current = self.hand_pose

            for finger in FINGERS :
                context_before = get_contact_context(last.contact, finger)
                context_current = get_contact_context(self.contact, finger)


                if pose_before.flex_states[finger] != pose_current.flex_states[finger] :
                    # The finger is flexed or extended
                    if pose_current.flex_states[finger] :
                        events.append(MicroGlyphEvent(action=Mg.ACTION.FLEXION, 
                                                    context=[context_before, context_current],
                                                    actuator=[[finger]]))
                    else :
                        events.append(MicroGlyphEvent(action=Mg.ACTION.EXTENSION, 
                                                    context=[context_before, context_current],
                                                    actuator=[[finger]]))
                else :
                    # The finger is not (only) flexed or extended
                    if context_before != context_current :
                        # The context has changed, this is a hidden flexion or hidden extension
                        # (too light to be detected by the flexion/extension detection)
                        if context_before == Mg.CONTEXT.AIR and context_current == Mg.CONTEXT.CONTACT :
                            events.append(MicroGlyphEvent(action=Mg.ACTION.FLEXION, 
                                                    context=[Mg.CONTEXT.AIR, Mg.CONTEXT.CONTACT],
                                                    actuator=[[THUMB]],
                                                    contact=self.contact))
                        elif context_before == Mg.CONTEXT.CONTACT and context_current == Mg.CONTEXT.AIR :
                            events.append(MicroGlyphEvent(action=Mg.ACTION.EXTENSION, 
                                                    context=[Mg.CONTEXT.CONTACT, Mg.CONTEXT.AIR],
                                                    actuator=[[THUMB]],
                                                    contact=self.contact))
                    else :
                        # In this case, we ONLY look for swipes
                        if context_before == Mg.CONTEXT.CONTACT and context_current == Mg.CONTEXT.CONTACT :
                            # At this point, we need to look for the phalanx that is in contact
                            phalanx_before = last.contact.phalanx[0][0]
                            phalanx_current = self.contact.phalanx[0][0]

                            if phalanx_before != phalanx_current :
                                if phalanx_before == BASE and phalanx_current in [MIDDLE, TIP] or phalanx_before == MIDDLE and phalanx_current == TIP :
                                    # The swipe is up
                                    events.append(MicroGlyphEvent(action=Mg.ACTION.EXTENSION, 
                                                                context=[context_before, context_current],
                                                                actuator=[[THUMB]],
                                                                contact=self.contact))
                                elif phalanx_before == TIP and phalanx_current in [MIDDLE, BASE] or phalanx_before == MIDDLE and phalanx_current == BASE :
                                    # The swipe is down
                                    events.append(MicroGlyphEvent(action=Mg.ACTION.FLEXION, 
                                                                context=[context_before, context_current],
                                                                actuator=[[THUMB]],
                                                                contact=self.contact))

            return events
        

        def __str__(self) -> str:
            return f"Detection({self.hand_pose}, {self.contact})"
        
        def __repr__(self) -> str:
            return self.__str__()
        
        def __eq__(self, __value: object) -> bool:
            return self.hand_pose == __value.hand_pose and self.contact == __value.contact 
        
        class HandPose :
            JOIN_THRESHOLD = { f"[{INDEX}, {MIDDLE}]" : 0.090, f"[{MIDDLE}, {RING}]" : 0.030, f"[{RING}, {PINKY}]" : 0.095 }

            def __init__(self, landmarks) :
                self.flex_states = dict()

                # Determine for each finger if its flexed or not
                self.flex_states[INDEX] = self.is_flexed(landmarks[WRIST], landmarks[INDEX_MCP], landmarks[INDEX_PIP], landmarks[INDEX_DIP], landmarks[INDEX_TIP])
                self.flex_states[MIDDLE] = self.is_flexed(landmarks[WRIST], landmarks[MIDDLE_MCP], landmarks[MIDDLE_PIP], landmarks[MIDDLE_DIP], landmarks[MIDDLE_TIP])
                self.flex_states[RING] = self.is_flexed(landmarks[WRIST], landmarks[RING_MCP], landmarks[RING_PIP], landmarks[RING_DIP], landmarks[RING_TIP])
                self.flex_states[PINKY] = self.is_flexed(landmarks[WRIST], landmarks[PINKY_MCP], landmarks[PINKY_PIP], landmarks[PINKY_DIP], landmarks[PINKY_TIP])

                self.index_middle_joined = self.are_joined(landmarks, [INDEX_TIP], [MIDDLE_TIP, MIDDLE_DIP])
                self.middle_ring_joined = self.are_joined(landmarks, [MIDDLE_TIP], [RING_TIP])
                self.ring_pinky_joined = self.are_joined(landmarks, [RING_TIP, RING_DIP], [PINKY_TIP])
                
            def get_fingers_flexed(self) :
                fingers_flexed = []
                if self.flex_states[INDEX] :
                    fingers_flexed.append(INDEX)
                if self.flex_states[MIDDLE] :
                    fingers_flexed.append(MIDDLE)
                if self.flex_states[RING] :
                    fingers_flexed.append(RING)
                if self.flex_states[PINKY] :
                    fingers_flexed.append(PINKY)
                return fingers_flexed

            def get_fingers_joined(self) :
                joined_fingers = []
                if self.index_middle_joined :
                    joined_fingers.append([INDEX, MIDDLE])
                if self.middle_ring_joined :
                    joined_fingers.append([MIDDLE, RING])
                if self.ring_pinky_joined :
                    joined_fingers.append([RING, PINKY])

            def is_flexed(self, wrist, joint1, joint2, joint3, joint4) :
                # Calculate the vector between each joint and the wrist
                segment1 = sh.Point(joint1.x-wrist.x, joint1.y-wrist.y, joint1.z-wrist.z)
                segment2 = sh.Point(joint2.x-joint1.x, joint2.y-joint1.y, joint2.z-joint1.z)
                segment3 = sh.Point(joint3.x-joint2.x, joint3.y-joint2.y, joint3.z-joint2.z)
                segment4 = sh.Point(joint4.x-joint3.x, joint4.y-joint3.y, joint4.z-joint3.z)
                # Calculate the angle between the segments
                angle1 = angle(segment1, segment2)
                angle2 = angle(segment2, segment3)
                angle3 = angle(segment3, segment4)

                # If the sum of the angles is 120° greater than the control angle, the finger is flexed
                control_angle = 0
                return angle1 + angle2 + angle3 > control_angle+ANGLE_THRESHOLD
            
            def are_joined(self, landmarks, joints1, joints2) :
                # Calculate the sum of the distances between the joints in two lists            
                dist = 0
                for joint1 in joints1 :
                    for joint2 in joints2 :
                        dist += distance(landmarks[joint1], landmarks[joint2])

                finger1 = get_finger_from_joints(joints1)
                finger2 = get_finger_from_joints(joints2)
                threshold = self.JOIN_THRESHOLD[f"[{finger1}, {finger2}]"]
                return dist < threshold
            
            def __str__(self) -> str:
                if self.flex_states[INDEX] :    index_flexed = "▼"
                else :                          index_flexed = "▲"
                if self.index_middle_joined :   index_middle_joined = ""
                else :                          index_middle_joined = " "
                if self.flex_states[MIDDLE] :   middle_flexed = "▼"
                else :                          middle_flexed = "▲"
                if self.middle_ring_joined :    middle_ring_joined = ""
                else :                          middle_ring_joined = " "
                if self.flex_states[RING] :     ring_flexed = "▼"
                else :                          ring_flexed = "▲"
                if self.ring_pinky_joined :     ring_pinky_joined = ""
                else :                          ring_pinky_joined = " "
                if self.flex_states[PINKY] :    pinky_flexed = "▼"
                else :                          pinky_flexed = "▲"

                return f"{index_flexed}{index_middle_joined}{middle_flexed}{middle_ring_joined}{ring_flexed}{ring_pinky_joined}{pinky_flexed}"
            
            def __repr__(self) -> str:
                return self.__str__()
            
            def __eq__(self, __value: object) -> bool:
                if not isinstance(__value, __class__) :
                    return False
                return self.flex_states[INDEX] == __value.flex_states[INDEX] and self.flex_states[MIDDLE] == __value.flex_states[MIDDLE] and self.flex_states[RING] == __value.flex_states[RING] and self.flex_states[PINKY] == __value.flex_states[PINKY] and self.index_middle_joined == __value.index_middle_joined and self.middle_ring_joined == __value.middle_ring_joined and self.ring_pinky_joined == __value.ring_pinky_joined

    def get_micro_glyph_events(self, hands : HandLandmarkerResult) :
        hand_events = [[], []]
        for i, landmarks in enumerate(hands) :
            if landmarks != [] :
                detection = self.Detection(landmarks, self.CONTACT_THRESHOLDS)

                # We split the detection according to the detected hand 
                # to avoid any confusion in the algorithm
                if len(self.detections) <= i :
                    self.detections.append([])
                elif len(self.detections[i]) > 1 :
                    detected_events = detection.get_events(self.detections[i])
                    hand_events[i] += detected_events
                    if len(self.detections[i]) > SMOOTHING_FRAMES :
                        self.detections[i].pop(0)

                self.detections[i].append(detection)

        events = [e for i in range(len(hand_events)) for e in hand_events[i]]
        if len(events) > 0 :
            return events
        return None

    def calibrate(self, tracker : MediapipeWebcamLandmarker) :
        # Check if calibration file exists
        dirname = os.path.dirname(__file__)
        if os.path.exists(dirname + "/calibration.json") :
            with open(dirname + "/calibration.json", "r") as file :
                self.CONTACT_THRESHOLDS = json.load(file)
                return

        print(f"Please raise your hand in front of the camera")
        hands = tracker.get_hands()
        while len(hands)==0 :
            hands = tracker.get_hands()
        print(f"Hand detected")
        # We calibrate the CONTACT_THRESHOLDS according to the tracker
        for finger in self.CONTACT_THRESHOLDS.keys() :
            for zone in self.CONTACT_THRESHOLDS[finger].keys() :
                thresholds = []
                threshold = None
                while threshold == None or len(thresholds) < THRESHOLD_TICKS:
                    threshold = get_threshold(tracker, finger, zone, len(thresholds))
                    if threshold != None : 
                        thresholds.append(threshold)
                    else :
                        thresholds = []
                mean_threshold = sum(thresholds)/len(thresholds)
                if zone == TIP :
                    mean_threshold = mean_threshold * 2.2
                elif zone == MIDDLE :
                    mean_threshold = mean_threshold * 1.7
                elif zone == BASE :
                    mean_threshold = mean_threshold * 1.3
                self.CONTACT_THRESHOLDS[finger][zone] = mean_threshold
                

        cv2.destroyAllWindows()

        print(f"Calibration done")
        # Save the calibration in a file
        dirname = os.path.dirname(__file__)
        with open(dirname + "/calibration.json", "w") as file :
            json.dump(self.CONTACT_THRESHOLDS, file)


def get_contact_context(contact : Mg.Contact, finger : str) :
    if contact.actuator[0][0] == finger :
        return Mg.CONTEXT.CONTACT
    else :
        return Mg.CONTEXT.AIR
    
# def get_contact_context(contact : Mg.Contact, finger : str) :
#     if contact.action == "Contact":
#         return Mg.CONTEXT.CONTACT
#     else :
#         return Mg.CONTEXT.AIR

def angle(vector1, vector2) :
    # Calculate the angle between two vectors
    dot_product = vector1.x*vector2.x + vector1.y*vector2.y + vector1.z*vector2.z
    magnitude1 = math.sqrt(vector1.x**2 + vector1.y**2 + vector1.z**2)
    magnitude2 = math.sqrt(vector2.x**2 + vector2.y**2 + vector2.z**2)
    return math.acos(dot_product/(magnitude1*magnitude2)) * 180/math.pi

def distance(point1, point2) :
    # Calculate the distance between two points
    return math.sqrt((point1.x-point2.x)**2 + (point1.y-point2.y)**2 + (point1.z-point2.z)**2)

def get_threshold(tracker : MediapipeWebcamLandmarker, finger, zone, tick) :
    hands = tracker.get_hands() 
    webcam_feedback = tracker.direct_draw_landmarks()

    if len(hands) == 0 :
        return None
    landmarks = hands[0]
    
    annotated_image = np.copy(tracker.image)
    finger_zone = get_finger_parts(landmarks, finger)[zone]
    color, thickness, radius = get_calibration_zone_properties(tick)
    webcam_feedback = tracker.draw_landmark(annotated_image, finger_zone, color, thickness, radius)

    cv2.imshow("Webcam",webcam_feedback)
    cv2.waitKey(1)

    # Get the distance of the tip of the thumb to the finger zone
    thumb_tip = landmarks[THUMB_TIP]
    finger_zone = get_finger_parts(landmarks, finger)[zone]
    dist =  distance(thumb_tip, finger_zone)
    if dist < FAR_THRESHOLD :
        return dist
    return None

def get_calibration_zone_properties(tick) :
    sub_tick = int(tick/(THRESHOLD_TICKS/len(DEFINED_COLORS)))%len(DEFINED_COLORS)
    color = DEFINED_COLORS[len(DEFINED_COLORS)-1-sub_tick]
    # convert to rgb
    color = (color & 0xff, (color >> 8) & 0xff, (color >> 16) & 0xff)
    thickness = 2
    radius = int(30/math.pow(sub_tick+1,0.5))-8
    return color, thickness, radius

###########################################
###         Detection Calculation         ###
###########################################

# CONTACT_THRESHOLDS = get_distance_threashold_from_name(CONTACT)

def compute_2D_landmarks(landmarks) :
    # Compute the 2D landmarks from the 3D landmarks
    landmarks_2D = []
    for landmark in landmarks :
        landmarks_2D.append(sh.Point(landmark.x, landmark.y, 0))
    return landmarks_2D

def distance_to_thumb(landmarks, finger) :
    thumb_tip = landmarks[THUMB_TIP]
    # CAUTION : a part is not a joint but the segment between two joints
    finger_parts = get_finger_parts(landmarks, finger)
    
    # Determine the nearest finger joint to the finger tip
    nearest_part = BASE
    for part in finger_parts.keys() :
        if distance(thumb_tip, finger_parts[part]) < distance(thumb_tip, finger_parts[nearest_part]) :
            nearest_part = part

    dist = distance(thumb_tip, finger_parts[nearest_part])
    return [dist, nearest_part]

def get_finger_tip(landmarks, finger) :
    # Return the tip joint of a finger
    if finger == THUMB :
        return landmarks[THUMB_TIP]
    elif finger == INDEX :
        return landmarks[INDEX_TIP]
    elif finger == MIDDLE :
        return landmarks[MIDDLE_TIP]
    elif finger == RING :
        return landmarks[RING_TIP]
    else :
        return landmarks[PINKY_TIP]

def get_finger_parts(landmarks, finger) :
    # Each part is the middle point between two joints
    if finger == THUMB :
        tip_part = get_middle_point(landmarks[THUMB_TIP], landmarks[THUMB_IP])
        base_part = get_middle_point(landmarks[THUMB_IP], landmarks[THUMB_MCP])
        return {BASE: base_part, TIP : tip_part}
    elif finger == INDEX :
        tip_part = get_middle_point(landmarks[INDEX_TIP], landmarks[INDEX_DIP])
        middle_part = get_middle_point(landmarks[INDEX_DIP], landmarks[INDEX_PIP])
        base_part = get_middle_point(landmarks[INDEX_PIP], landmarks[INDEX_MCP])
        return {BASE: base_part, MIDDLE: middle_part, TIP : tip_part}
    elif finger == MIDDLE :
        tip_part = get_middle_point(landmarks[MIDDLE_TIP], landmarks[MIDDLE_DIP])
        middle_part = get_middle_point(landmarks[MIDDLE_DIP], landmarks[MIDDLE_PIP])
        base_part = get_middle_point(landmarks[MIDDLE_PIP], landmarks[MIDDLE_MCP])
        return {BASE: base_part, MIDDLE: middle_part, TIP : tip_part}
    elif finger == RING :
        tip_part = get_middle_point(landmarks[RING_TIP], landmarks[RING_DIP])
        middle_part = get_middle_point(landmarks[RING_DIP], landmarks[RING_PIP])
        base_part = get_middle_point(landmarks[RING_PIP], landmarks[RING_MCP])
        return {BASE: base_part, MIDDLE: middle_part, TIP : tip_part}
    else :
        tip_part = get_middle_point(landmarks[PINKY_TIP], landmarks[PINKY_DIP])
        middle_part = get_middle_point(landmarks[PINKY_DIP], landmarks[PINKY_PIP])
        base_part = get_middle_point(landmarks[PINKY_PIP], landmarks[PINKY_MCP])
        return {BASE: base_part, MIDDLE: middle_part, TIP : tip_part}

def get_middle_point(point1, point2) :
    # Return the middle point between two vectors
    x = (point1.x + point2.x) / 2
    y = (point1.y + point2.y) / 2
    z = (point1.z + point2.z) / 2
    middle_point = sh.Point(x, y, z)
    return middle_point

def join_has_changed(last, current) :
    # Return True if the joint has moved by more than 10% of the distance to the center
    return abs(distance(last, current)) > abs(distance(last, sh.Point(0, 0, 0))) * 0.01

def get_finger_from_joints(joints) :
    if len(joints) == 1 :
        joint = joints[0]
        if joint in [INDEX_MCP, INDEX_PIP, INDEX_DIP, INDEX_TIP] :
            return INDEX
        elif joint in [MIDDLE_MCP, MIDDLE_PIP, MIDDLE_DIP, MIDDLE_TIP] :
            return MIDDLE
        elif joint in [RING_MCP, RING_PIP, RING_DIP, RING_TIP] :
            return RING
        elif joint in [PINKY_MCP, PINKY_PIP, PINKY_DIP, PINKY_TIP] :
            return PINKY
        else :
            raise ValueError("The given joint is not valid")
    elif len(joints) > 1 :
        a_list, b_list = [], []
        # fill half of the values in a_list and the other half in b_list
        for i, joint in enumerate(joints) :
            if i%2 == 0 :
                a_list.append(joint)
            else :
                b_list.append(joint)

        finger_a = get_finger_from_joints(a_list)
        finger_b = get_finger_from_joints(b_list) 

        if finger_a == finger_b :
            return finger_a
        else :
            raise ValueError("The given joints are not from the same finger")
    else :
        raise ValueError("The given joints are empty!")
