const Device = require("../Device.js");
const RawDataEvent = require("../../RawDataEvent.js");
/////////////////////////////// READ ME /////////////////////////////////
// This is an example of an empty device, that you can use to create your own device.
// This Class is not supposed to be instanciated, but to be used as a TEMPLATE to create your own device.
// Some generic methode FooProtocol, barMethod, have to be replaced by your own system.
// Override methods that need to be overrided.
// WEB SOCKET LIBRARY is socket.io, which is a very popular library for websockets, with additional features as auto-reconnection.
//  Get documentation https://socket.io/



class EmptyWebSocketDevice extends Device {
    //////////////////////////////////// STATIC PROTOCOL //////////////////////////////////////
    //If you use a standard protocol that require a port ( serial, bluetooth, wifi, rawhid, etc...)
    // You can define a protocol here.
    // This will allow the user to choose the port in the UI, and give it to the constructor port variable.
    // static protocol = Device.PROTOCOL.SERIAL;
    // static protocol = Device.PROTOCOL.BLUETOOTH;
    // static protocol = Device.PROTOCOL.WEBSOCKET;
    // static protocol = Device.PROTOCOL.HID;        // W.I.P
    // static protocol = Device.PROTOCOL.WIFI;       // W.I.P
    /////////////////////////////
    static protocol = Device.PROTOCOL.WEBSOCKET;
    static defaultPort = 8081; // give a value to the default port 
    static allowPortChoice = false; // Force the user to select the defaut port. ( default is true )
    //add a constructor with em argument
    constructor(index, am, port = null) {
        //call the constructor of the parent class
        super(index, "EmptyWebSocketDevice", am);
        // this.port = port; //if you use a standard protocol,and that you define the static protocol below. User will be able to choose the port in the UI
        this.description = "This is an empty device, use it as a template to create your own device"

        this.openPort(port); //try to open the serial port, even if it's null


    }

    openPort(port) {


        if (port == null || port == undefined) {
            this.port = this.constructor.defaultPort;
            return;
        } else {
            this.port = port;
        }

        //Helper function that create automatically device
        //Regarging protocol and port. Set automatically this.device with a Promise
        let promiseDevice = this.createDevice()

        promiseDevice.then(() => {
            // At this point, this.device is a socket.io object

            // Event when data is received, Only is device exist
            this.device.on('data', (data) => {
                // Manage your data here
                this.barMethod(data);
            });

        }).catch((e) => {
            console.error(e);
        });


    }


    //override this method
    restartConnection() {

        // This method is called when the connection is lost
        if (!this.isOpen) {
            this.openPort(this.port);
        } else {
            this.close();
            setTimeout(() => {
                this.openPort(this.port);
            }, 1000);
        }
    }

    //override close method
    close() {
        // When you are done using the serial port, you can close it like this:
        if (this.isOpen) {
            this.device.close();
        }
        //clear this.device as null
        this.device = null;

    }


    barMethod(data) {
        //Process Data first in the way you want.

        // OPTIONNAL : have a look at a realtime view for a sensor for debug
        this.graphUI(value, channel)
        // OPTIONNAL : had a texte message to the console for debug
        this.consoleUI(message)


        // Send rawDataEvent if needed
        let rde = new RawDataEvent();
        // Modify RawDataEvent
        rde.actuator = [[rde.FINGER.THUMB]];
        rde.action = rde.ACTION.FLEXION
        rde.context = [rde.CONTEXT.AIR, rde.CONTEXT.AIR]
        //Emit this rawdataevent
        this.emit(rde);
    }




}

module.exports = EmptyWebSocketDevice;