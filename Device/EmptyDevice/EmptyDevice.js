const Device = require("../Device.js");
const RawDataEvent = require("../../RawDataEvent.js");
/////////////////////////////// READ ME /////////////////////////////////
// This is an example of an empty device, that you can use to create your own device.
// This Class is not supposed to be instanciated, but to be used as a template to create your own device.
// Some generic methode FooProtocol, barMethod, have to be replaced by your own system.
// Override methods that need to be overrided.


class EmptyDevice extends Device {
    //////////////////////////////////// STATIC PROTOCOL //////////////////////////////////////
    //If you use a standard protocol that require a port ( serial, bluetooth, wifi, rawhid, etc...)
    // You can define a protocol here.
    // This will allow the user to choose the port in the UI, and give it to the constructor port variable.
    // static protocol = Device.PROTOCOL.SERIAL;
    // static protocol = Device.PROTOCOL.BLUETOOTH;
    // static protocol = Device.PROTOCOL.WEBSOCKET;
    // static protocol = Device.PROTOCOL.HID;        // W.I.P
    // static protocol = Device.PROTOCOL.WIFI;       // W.I.P
    /////////////////////////////
    static protocol = Device.PROTOCOL.NONE;
    // static defaultPort = 8081; // give a value to the default port ( optionnal )
    //static allowPortChoice = false; // Force the user to select the defaut port. ( default is true )

    //add a constructor with em argument
    constructor(index, am, port = null) {
        //call the constructor of the parent class
        super(index, "EmptyDevice", am);
        // this.port = port; //if you use a standard protocol,and that you define the static protocol below. User will be able to choose the port in the UI
        this.description = "This is an empty device, use it as a template to create your own device"

        openPort(port);

    }

    openPort(port) {
        //override this method
        // This method is called when a change of port is operated through the GUI

        this.device = new FooProtocol(); //use your own system here

        // create a callback when FooProtocol confirm the creation / connection
        this.device?.on('connection', () => {
            this.setOpen(true);  //Call this function when you device is ready to inform the GUI
        });

        // create a callback when FooProtocol give a an event of new data coming
        this.device?.on('data', (data) => {
            this.barMethod(data);
        });

    }



    restartConnection() {
        //override this method
        // This method is called when the connection is lost
    }

    close() {
        //override close method.
        //This method is called when the device is removed from the device manager
        // Be sure to close all the connection
        // Be sure to remove all the listeners
        // Be sur to clear all interval ( as setInterval for example )
        // Remove every data than can be stored in memory
        this.setOpen(false); //Call this function when you device is ready to inform the GUI    
    }


    barMethod(data) {

        //Process Data first in the way you want.

        // OPTIONNAL : have a look at a realtime view for a sensor for debug
        this.graphUI(value, channel)
        // OPTIONNAL : had a texte message to the console for debug
        this.consoleUI(message)



        // Send rawDataEvent if needed
        let rde = new RawDataEvent();
        // Modify RawDataEvent
        rde.actuator = [[rde.FINGER.THUMB]];
        rde.action = rde.ACTION.FLEXION
        rde.context = [rde.CONTEXT.AIR, rde.CONTEXT.AIR]
        //Emit this rawdataevent
        this.emit(rde);
    }




}

module.exports = EmptyDevice;