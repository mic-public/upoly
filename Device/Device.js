//Create a class Device : this is a kind of java interface in javascript
/*
This is an interface for devices children classes. This class is abstract, it can't be instanciated.
A device children class is basically an arduino object, with its own way of communication.
So we can imagine some SerialArduinoDevice, that works with serial communication, or HID device using teensy.
We can imagine usb device, or bluetooth device, or wifi device. 
*/


const { NONAME } = require("dns");
const Controller = require("../Controller/Controller.js");
const { SerialPort } = require('serialport')
const noble = require('@abandonware/noble');
const path = require("path");
const sio = require('socket.io');

class Device {
    //add a constructor with em argument
    constructor(i, name, amanager) {
        //add a member em - Raw Data Event Manager. Get the instance of the singleton
        this.am = amanager; //analyzer manager
        this.controller = Controller.getInstance();
        this.indexDevice = i; // used to identify the device in the UI
        this.name = name; // used to identify the device ( name of the glove)
        this.description = "";
        this.isOpen = false;
        this.timeOfCreation = Date.now();
        this.timeOfLastData = Date.now();

        this.port = null;
        this.device = null;


    }




    static PROTOCOL = {
        NONE: null,
        SERIAL: "serial",
        BLUETOOTH: "bluetooth",
        WEBSOCKET: "websocket",
        HID: "rawhid",
        WIFI: "wifi",
    };

    static protocol = Device.PROTOCOL.NONE;

    static defaultPort = '';
    static allowPortChoice = true;


    emit(event) {
        //override emit method
        event.deviceId = this.name;
        //replace rawDataEvent.data by data object gave in parameter
        event.timestamp = Date.now();
        //send it to the Analyzer Manager : MANDATORY
        this.am.emit(event);
        //display rawdataevent in the special widget of the device : OPTIONNAL
        this.rawDataEventUI(event);
    }

    openPort(port) {
        // empty method because it's an interface
        //throw an error if it's not overriden
        throw new Error("The 'restart' method must be overridden in the child class.");
    }

    restartConnection() {
        // empty method because it's an interface
        //throw an error if it's not overriden
        throw new Error("The 'restart' method must be overridden in the child class.");
    }


    close() {
        // empty method because it's an interface
        //throw an error if it's not overriden
        throw new Error("The 'close' method must be overridden in the child class.");
    }

    //UI can manage grah and console messages from device.
    graphUI(data, channel) {
        this.controller.updateDeviceChartUI(data, channel, this.indexDevice);
    }

    consoleUI(data) {
        //console.log("to UI Console : " + data );
        this.controller.updateDeviceConsoleUI(data, this.indexDevice);
    }

    rawDataEventUI(data) {
        this.controller.updateDeviceRawDataEventUI(data, this.indexDevice);
    }

    setOpen(state) {

        this.isOpen = state;
        if (state) {
            this.updateStateUI(this.isOpen)
        } else {
            this.updateStateUI(this.isOpen)
        }
    }

    updateStateUI(state) {
        this.controller.updateDeviceStateUI(state, this.indexDevice);
    }

    ////////////////////// HELPER METHODS //////////////////////

    //This is an helper method to create a device
    createDevice() {

        let promiseDevice = null;

        switch (this.constructor.protocol) {
            case Device.PROTOCOL.SERIAL:
                console.log("Protocol usb serial defined");
                return this.createSerialUsbDevice();
                break;
            case Device.PROTOCOL.BLUETOOTH:
                console.log("Protocol bluetooth defined");
                return this.createBluetoothDevice(); //returne a promise because bluetooth is always scanning
                break;
            case Device.PROTOCOL.WEBSOCKET:
                console.log("Protocol websocket defined");
                return this.createWebSocketDevice();
            default:
                console.log("No protocol defined for this device");
                this.device = null;
                return new Promise((resolve, reject) => { reject("No protocol defined for this device") });
                break;
        }

        return null;

    }


    createSerialUsbDevice(baudrate = 115200) {

        return new Promise((resolve, reject) => {
            //This function create a serial usb device, default baudrate is 115200
            if (this.port == null || this.port == undefined || this.port == "") {
                reject("Port is not defined");
            }

            this.device = new SerialPort({
                path: this.port,
                baudRate: 115200,
            }, (err) => {
                if (err) {
                    console.error('Error opening port: ', err.message);
                    this.setOpen(false);
                    // Attempt to close the port forcefully
                    try {
                        this.device.close((closeErr) => {
                            if (closeErr) {
                                console.error('Error closing port: ', closeErr.message);
                            } else {
                                console.log('Port closed successfully.');
                                // Now, try reopening the port after a short delay
                            }
                        });
                    } catch (closeErr) {
                        console.error('Error closing port: ', closeErr.message);
                    }
                }
            });

            //clear serial buffer first
            this.device.on('open', () => {
                console.log('Port open with success.');
                this.device.flush();
                this.setOpen(true);
                resolve();

            });

            //check is connection is lost
            this.device.on('close', () => {
                console.log('Port closed with success.');
                this.setOpen(false);

            });

            process.on("SIGINT", () => {
                // Close serial port properly
                this.close();
                //
                console.log("process killed by user");
                process.exit();
            });



        });
    }

    createBluetoothDevice() {
        console.log("createBluetoothDevice : " + this.port);


        return new Promise((resolve, reject) => {

            if (this.port == null || this.port == undefined || this.port == "") {
                reject("Port is not defined");
            }


            noble.startScanningAsync([], false);


            noble.on('discover', async (peripheral) => {
                if (peripheral.advertisement.localName == undefined) {
                    return;
                }

                console.log('peripheral  discovered (' + peripheral.id +
                    ' with address <' + peripheral.address + ', ' + peripheral.addressType + '>,' +
                    ' RSSI ' + peripheral.rssi + ':');
                console.log('\thello my local name is:');
                console.log('\tConnectable' + peripheral.connectable);
                console.log('\t\t' + peripheral.advertisement.localName);
                console.log('\tcan I interest you in any of the following advertised services:');
                console.log('\t\t' + JSON.stringify(peripheral.advertisement.serviceUuids));

                if (peripheral.id == this.port) {
                    console.log('Device Found ');
                    noble.stopScanning();
                    peripheral.connect((error) => {
                        console.log('connected to peripheral: ' + peripheral.uuid);
                        this.setOpen(true);
                        this.device = peripheral;
                        peripheral.on('disconnect', () => {
                            console.log('disconnected');
                            this.setOpen(false);
                        });
                        resolve();


                    });

                }
            });
        });
    }

    createWebSocketDevice() {
        console.log("createWebSocketDevice : " + this.port);
        if (this.port == null || this.port == undefined || this.port == "") {
            console.error("Port is not defined");
            return;
        }

        return new Promise((resolve, reject) => {

            try {
                this.device = sio(this.port);
            } catch (error) {
                reject(error);
            }

            this.device.on('connection', socket => {
                // // either with send()
                console.log('Client connected !');
                this.setOpen(true); // inform the parent class that the device is connected

                socket.on('disconnect', () => {
                    console.log('Client disconnected !');
                    this.setOpen(false); // inform the parent class that the device is disconnected
                });


            });


            resolve(); // Resolve even if the port is not open, the device is correctly created and can be connected later.
        });


    }

    //create a static metod that find serial port on any platform
    static async listSerialPorts() {
        // empty method because it's an interface
        //throw an error if it's not overriden
        const util = require('util');
        const exec = util.promisify(require('child_process').exec); //promisify exec . Some issue with COMMONJS module

        let serialport = [];
        SerialPort.list().then(
            ports => {
                ports.forEach(port => {
                    //console.log(`${port.path} ${port.manufacturer} ${port.serialNumber} ${port.vendorId} ${port.productId} ${port.pnpId} ${port.locationId} ${port.productId} ${port.vendorId}`);
                    let obj = {
                        path: port.path,
                        manufacturer: port.manufacturer,
                    }
                    serialport.push(obj);
                });
            },
            err => {
                console.error('Error listing ports', err);
            }
        );
        return serialport;
    }

    static async listBLEPorts() {
        return new Promise((resolve, reject) => {
            let bleport = [];
            let knownDevices = [];

            noble.on('stateChange', async (state) => {
                if (state === 'poweredOn') {
                    await noble.startScanningAsync([], false);
                } else {
                    noble.stopScanning();
                    reject(Error("Bluetooth is not powered on"));
                }
            });

            noble.on('discover', (peripheral) => {
                // Check if peripheral.id is not known and peripheral.advertisement.localName is not null or undefined
                if (!knownDevices.includes(peripheral.id) && peripheral.advertisement.localName) {
                    knownDevices.push(peripheral.id);
                    bleport.push({
                        path: peripheral.id,
                        manufacturer: peripheral.advertisement.localName
                    });
                }
            });

            // Stop scanning and resolve the promise after 5 seconds
            setTimeout(() => {
                noble.stopScanning();
                resolve(bleport);
            }, 5000);
        });


    }






    /*
    if (process.platform === 'darwin') {
      const { stdout, stderr } = await exec('ls /dev | grep cu.usb')
      if (stderr) {
        console.log("No port detected");
        return;
      }
      let serialport = stdout.split("\n");
      if (serialport.length > 0) {
        //remove the empty string element
        serialport = serialport.filter(port => port !== '');
   
        serialport = serialport.map(port => "/dev/" + port.replace(/\s+/g, ''));
      }
      return serialport;
    }
    else if (process.platform === 'linux') {
      const { stdout, stderr } = await exec('ls /dev | grep ttyACM')
      if (stderr) {
        console.log("No port detected");
        return;
      } else {
        let serialport = stdout.split("\n");
        if (serialport.length === 0 || serialport[0] === '') {
        } else {
          serialport = serialport.map(port => "/dev/" + port.replace(/\s+/g, ''));
        }
        return serialport;
      }
    }
    */

}

process.on('SIGINT', function () {
    console.log('Caught interrupt signal in Device.js');
    noble.stopScanning(() => process.exit());
});

process.on('SIGQUIT', function () {
    console.log('Caught interrupt signal  in Device.js');
    noble.stopScanning(() => process.exit());
});

process.on('SIGTERM', function () {
    console.log('Caught interrupt signal  in Device.js');
    noble.stopScanning(() => process.exit());
});

module.exports = Device;
