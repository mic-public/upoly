
const Device = require("../Device.js");
const { exec } = require('child_process')
const util = require('util')
/*

36 bytes structures = 36 * 8 = 288 bits

struct SensorData
{
  uint8_t finger1; // INDEX lets encode all buton over 8 bits.
  uint8_t finger2; // MIDDLE lets encode all buton over 8 bits.
  uint8_t finger3; // RING lets encode all buton over 8 bits.
  uint8_t finger4; // PINKY   lets encode all buton over 8 bits.
  uint16_t finger1touch1;
  uint16_t finger1touch2;
  uint16_t finger2touch1;
  uint16_t finger2touch2;

  char message[24]; // 24 bytes
};

*/



class BleNanoEsp32 extends Device {
    //////////////////////////////////// STATIC PROTOCOL //////////////////////////////////////
    //If you use a standard protocol that require a port ( serial, bluetooth, wifi, rawhid, etc...)
    // You can define a protocol here.
    // This will allow the user to choose the port in the UI, and give it to the constructor port variable.
    // static protocol = Device.PROTOCOL.SERIAL;
    // static protocol = Device.PROTOCOL.BLUETOOTH;
    // static protocol = Device.PROTOCOL.HID;        // W.I.P
    // static protocol = Device.PROTOCOL.WIFI;       // W.I.P
    /////////////////////////////

    static protocol = Device.PROTOCOL.BLUETOOTH;

    constructor(index, am, port = null) {
        //call the constructor of the parent class
        super(index, "bleNanoEsp32", am);
        this.description = " BLUETOOTH ";

        this.openPort(port); //try to open the serial port, even if it's null

        this.init();


    }

    init() {


    }

    openPort(port) {
        // Create a port with error callback
        this.port = port;

        if (port == null || port == undefined) {
            console.error("No port defined");
            return;
        }


        //Helper function that create automatically device
        //Regarging protocol and port. Set automatically this.device
        console.log("openPort on bleNanoEsp32 : create promise device");
        let promiseDevice = this.createDevice();


        promiseDevice.then(() => {

            console.log("Promise device is realized");
            this.device.discoverAllServicesAndCharacteristics((error, services, characteristics) => {
                if (error) {
                    console.log("error in discovering services and characteristics")
                    console.log(error)
                }
                else {
                    console.log("services and characteristics are found : ")
                    characteristics.forEach((characteristic) => {
                        console.log(characteristic.properties);
                    }
                    );
                    console.log("start with the first characteristic")
                    let characteristic = characteristics[0];
                    characteristic.subscribe((error) => {
                        if (error) {
                            console.log("error in subscribing")
                        } else {
                            console.log("subscribing is successful")
                            characteristic.once('notify', (state) => {
                                console.log(`notify state is ${state}`)
                            });
                            characteristic.on('data', (data, isNotification) => {
                                console.log(`data received `);
                                console.log(data.toString('utf8'));
                            }
                            );

                        }
                    });

                }
            });

        });

        promiseDevice.catch((err) => {
            console.error("Error while creating the device : " + err);
        });


    }

    processData(data) {

        // Convertir les données binaires en tableau de valeurs
        this.dataBuffer = Buffer.concat([this.dataBuffer, data]);

        // Votre Arduino envoie des blocs de 36 bytes, donc vous pouvez accéder aux valeurs individuelles
        if (this.dataBuffer.length >= 36) {

            // Pour accéder à une valeur particulière, vous pouvez utiliser l'indice
            const indexbut1 = Boolean(this.readBit(this.dataBuffer, 0, 0)); // position 0 of the first byte
            const indexbut2 = Boolean(this.readBit(this.dataBuffer, 0, 1)); // position 1 of the first byte
            const middlebut1 = Boolean(this.readBit(this.dataBuffer, 1, 0)); // position 0 of the second byte
            const middlebut2 = Boolean(this.readBit(this.dataBuffer, 1, 1)); // position 1 of the second byte
            const ringbut1 = Boolean(this.readBit(this.dataBuffer, 2, 0)); // position 0 of the third byte
            const pinkbut1 = Boolean(this.readBit(this.dataBuffer, 3, 0)); // position 0 of the fourth byte



            if (indexbut1 != this.listOfTouch[0][0]) {
                this.listOfTouch[0][0] = indexbut1;
                this.listOfTouchFlagChange[0][0] = true;
            }
            if (indexbut2 != this.listOfTouch[0][1]) {
                this.listOfTouch[0][1] = indexbut2;
                this.listOfTouchFlagChange[0][1] = true;
            }
            if (middlebut1 != this.listOfTouch[1][0]) {
                this.listOfTouch[1][0] = middlebut1;
                this.listOfTouchFlagChange[1][0] = true;
                console.log("middlebut1 " + middlebut1);
            }
            if (middlebut2 != this.listOfTouch[1][1]) {
                this.listOfTouch[1][1] = middlebut2;
                this.listOfTouchFlagChange[1][1] = true;
                console.log("middlebut2 " + middlebut2);
            }
            if (ringbut1 != this.listOfTouch[2][0]) {
                this.listOfTouch[2][0] = ringbut1;
                this.listOfTouchFlagChange[2][0] = true;
            }
            if (pinkbut1 != this.listOfTouch[3][0]) {
                this.listOfTouch[3][0] = pinkbut1;
                this.listOfTouchFlagChange[3][0] = true;
            }


            //update listOfAnalog. 2 bytes per value
            for (let i = 0; i < 4; i++) {
                //define byteindex
                let byteIndex = 4 + i * 2;

                //read the entire byte
                let byte = this.dataBuffer.at(byteIndex) + (this.dataBuffer.at(byteIndex + 1) << 8);
                //limit byte from 0 to 1023
                byte = Math.min(byte, 1023);
                //calculate the derivative


                if (this.listOfAnalog[i] != byte) {
                    this.listOfAnalogFlagChange[i] = true;
                    this.listOfDerivativeAnalog[i] = byte - this.listOfAnalog[i];
                    this.listOfDerivativeAnalogFlagChange[i] = true;
                    this.listOfPreviousAnalog[i] = this.listOfAnalog[i];
                    this.listOfAnalog[i] = byte;
                }


            }

            // PRINT RAW VALUE
            // if (this.listOfAnalog[0] < 240 || this.listOfAnalog[1] < 240) {

            //     console.log("" + this.listOfAnalog[0] + " : " + this.listOfAnalog[1]);
            // }
            //console.log(" Analog2: " + this.listOfAnalog[2] + " Analog3: " + this.listOfAnalog[3]);


            // this.graphUI(this.listOfAnalog[0], 0);
            // this.graphUI(this.listOfAnalog[1], 1);
            // this.graphUI(this.listOfAnalog[2], 2);
            // this.graphUI(this.listOfAnalog[3], 3);



            this.dataBuffer = Buffer.alloc(0);


        }

    }

    autoReconnect() {
        if (!this.isOpen) {
            this.restartConnection();
        }
    }

    restartConnection() {


        if (!this.isOpen) {
            this.openPort(this.port);
        } else {
            this.close();
            setTimeout(() => {
                this.openPort(this.port);
            }, 1000);
        }


    }



    // Function to read a specific bit at a given position
    readBit(buffer, byteIndex, bitIndex) {
        const byteValue = buffer.at(byteIndex);
        // Use bitwise shift and AND operation to read the bit
        return (byteValue & (1 << bitIndex)) !== 0;
    }














    //override close method
    close() {
        // Close the HID device
        if (this.device) {
            // When you are done using the serial port, you can close it like this:
            if (this.device.isOpen) {

                this.device.close((err) => {
                    if (err) {
                        console.error('Error closing the serial port:', err);
                    } else {
                        console.log('Serial port closed successfully.');
                    }
                });
            }
            //clear this.device as null
            this.device = null;
        }
    }







}

module.exports = BleNanoEsp32;
