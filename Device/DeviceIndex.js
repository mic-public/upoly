
const ExampleDevice = require("./ExampleDevice/ExampleDevice.js");
const UGet = require("./uGeT/uGeT.js");
const Index3Phalanx = require("./index3phalanx/index3phalanx.js");
const IndexMiddle6Phalanx = require("./indexMiddle6phalanx/indexMiddle6phalanx.js");
const NanoEsp32 = require("./nano-esp32/nano-esp32.js");
const BleNanoEsp32 = require("./ble-nano-esp32/ble-nano-esp32.js");
const FlexAndVelostat = require("./FlexAndVelostat/FlexAndVelostat.js");
const Mediapipe = require("./Mediapipe/Mediapipe.js");
const Hololens = require("./Hololens/Hololens.js");


module.exports = {
    ExampleDevice,
    UGet,
    FlexAndVelostat,
    Mediapipe,
    Hololens,
    Index3Phalanx,
    IndexMiddle6Phalanx,
    NanoEsp32,
    BleNanoEsp32
};