
const Device = require("../Device.js");
const RawDataEvent = require("../../RawDataEvent.js");
const { SerialPort } = require('serialport')
const { exec } = require('child_process')
const util = require('util')
/*

36 bytes structures = 36 * 8 = 288 bits

struct SensorData
{
  uint8_t finger1; // INDEX lets encode all buton over 8 bits.
  uint8_t finger2; // MIDDLE lets encode all buton over 8 bits.
  uint8_t finger3; // RING lets encode all buton over 8 bits.
  uint8_t finger4; // PINKY   lets encode all buton over 8 bits.
  uint16_t finger1touch1;
  uint16_t finger1touch2;
  uint16_t finger2touch1;
  uint16_t finger2touch2;

  char message[24]; // 24 bytes
};

*/

class Average {

    constructor(window_size) {
        this.index = 0;
        this.value = 0;
        this.sum = 0;
        this.average = 0;
        this.last_average = 0;
        this.window_size = window_size;
        this.actual_window_size = 0;
        this.data = new Array(window_size).fill(0);


    }



    update(newvalue) {
        let flagChanged = false;
        this.sum = this.sum - this.data[this.index];
        this.value = newvalue;
        this.data[this.index] = newvalue;
        this.sum = this.sum + newvalue;

        this.index = (this.index + 1) % this.window_size;
        if (this.actual_window_size < this.window_size) {
            this.actual_window_size++;
        }

        let newaverage = this.sum / this.actual_window_size;
        if (newaverage != this.average) {
            this.average = newaverage;
            flagChanged = true;
        }
        return flagChanged;
    }

    clear() {
        this.sum = 0;
        this.average = 0;
        this.last_average = 0;
        this.data = new Array(this.window_size).fill(0);
        this.actual_window_size = 0;
    }

    saveLastAverage() {
        this.last_average = this.average;
    }


}

class NanoEsp32 extends Device {
    //////////////////////////////////// STATIC PROTOCOL //////////////////////////////////////
    //If you use a standard protocol that require a port ( serial, bluetooth, wifi, rawhid, etc...)
    // You can define a protocol here.
    // This will allow the user to choose the port in the UI, and give it to the constructor port variable.
    // static protocol = Device.PROTOCOL.SERIAL;
    // static protocol = Device.PROTOCOL.BLUETOOTH;
    // static protocol = Device.PROTOCOL.HID;        // W.I.P
    // static protocol = Device.PROTOCOL.WIFI;       // W.I.P
    /////////////////////////////

    static protocol = Device.PROTOCOL.SERIAL;



    constructor(index, am, port = null) {
        //call the constructor of the parent class
        super(index, "NanoEsp32", am);
        this.description = " Modular Glove Device, with 4 fingers.  Index & Middle get 2 capactive touch sensor for swipe detection. Ring & Pinky get 1 capactive touch sensor for swipe detection + 2 buttons each.  Index & Middle get 1 potentiometer for the direction in the air. Ring & Pinky get 1 capactive touch sensor at the tip of the finger ";

        this.openPort(port); //try to open the serial port, even if it's null

        this.init();


    }



    init() {

        this.dataBuffer = Buffer.alloc(0);  // Initialize an empty buffer

        // save the memory of the sensor value, in order to send only if it change
        this.listOfTouch = [[false, false, false], [false, false, false], [false, false, false], [false, false, false]];  // 4 finger, 3 phalanx. They are not all used
        this.listOfTouchFlagChange = [[false, false, false], [false, false, false], [false, false, false], [false, false, false]];  // 4 finger, 3 phalanx. They are not all used
        this.listOfAnalog = [0, 0, 0, 0];  // 2 capactive touch sensor for 2 fingers
        this.listOfPreviousAnalog = [0, 0, 0, 0];
        this.listOfAnalogFlagChange = [false, false, false, false];  // 4 potentiometer
        this.listOfDerivativeAnalog = [0, 0, 0, 0];  // 4 potentiometer : calculate the derivative of the analog value, in order the have what we call a signicative change
        this.listOfDerivativeAnalogFlagChange = [false, false, false, false];  // 4 potentiometer
        this.listOfSwipePosition = [0, 0, 0, 0];  // 4 fingers, 1 swipe for each finger
        this.listOfAverageSwipePosition = [new Average(10), new Average(10), new Average(10), new Average(10)];  // 4 fingers, 1 swipe for each finger
        this.listOfSwipeTouch = [false, false, false, false];  // 4 fingers, 1 swipe for each finger
        this.initInterpolation();

    }


    openPort(port) {
        // Create a port with error callback
        this.port = port;

        if (port == null || port == undefined) {
            console.error("No port defined");
            return;
        }


        //Helper function that create automatically device
        //Regarging protocol and port. Set automatically this.device
        //console.log("which protocol is defined ?")
        let promiseDevice = this.createDevice();

        promiseDevice.then(() => {
            this.device.on('data', (data) => {
                //console.log('Data reveived');
                // Convertir les données binaires en tableau de valeurs
                //check that data is one byte long
                //get last byte of buffer into a new variable
                this.processData(data);
                this.swipeDetection();
                this.touchDetection();


            });
        });

        promiseDevice.catch((err) => {
            console.error("Error while creating the device : " + err);
        });






    }




    processData(data) {

        // Convertir les données binaires en tableau de valeurs
        this.dataBuffer = Buffer.concat([this.dataBuffer, data]);

        // Votre Arduino envoie des blocs de 36 bytes, donc vous pouvez accéder aux valeurs individuelles
        if (this.dataBuffer.length >= 36) {

            // Pour accéder à une valeur particulière, vous pouvez utiliser l'indice
            const indexbut1 = Boolean(this.readBit(this.dataBuffer, 0, 0)); // position 0 of the first byte
            const indexbut2 = Boolean(this.readBit(this.dataBuffer, 0, 1)); // position 1 of the first byte
            const middlebut1 = Boolean(this.readBit(this.dataBuffer, 1, 0)); // position 0 of the second byte
            const middlebut2 = Boolean(this.readBit(this.dataBuffer, 1, 1)); // position 1 of the second byte
            const ringbut1 = Boolean(this.readBit(this.dataBuffer, 2, 0)); // position 0 of the third byte
            const pinkbut1 = Boolean(this.readBit(this.dataBuffer, 3, 0)); // position 0 of the fourth byte



            if (indexbut1 != this.listOfTouch[0][0]) {
                this.listOfTouch[0][0] = indexbut1;
                this.listOfTouchFlagChange[0][0] = true;
            }
            if (indexbut2 != this.listOfTouch[0][1]) {
                this.listOfTouch[0][1] = indexbut2;
                this.listOfTouchFlagChange[0][1] = true;
            }
            if (middlebut1 != this.listOfTouch[1][0]) {
                this.listOfTouch[1][0] = middlebut1;
                this.listOfTouchFlagChange[1][0] = true;
                console.log("middlebut1 " + middlebut1);
            }
            if (middlebut2 != this.listOfTouch[1][1]) {
                this.listOfTouch[1][1] = middlebut2;
                this.listOfTouchFlagChange[1][1] = true;
                console.log("middlebut2 " + middlebut2);
            }
            if (ringbut1 != this.listOfTouch[2][0]) {
                this.listOfTouch[2][0] = ringbut1;
                this.listOfTouchFlagChange[2][0] = true;
            }
            if (pinkbut1 != this.listOfTouch[3][0]) {
                this.listOfTouch[3][0] = pinkbut1;
                this.listOfTouchFlagChange[3][0] = true;
            }


            //update listOfAnalog. 2 bytes per value
            for (let i = 0; i < 4; i++) {
                //define byteindex
                let byteIndex = 4 + i * 2;

                //read the entire byte
                let byte = this.dataBuffer.at(byteIndex) + (this.dataBuffer.at(byteIndex + 1) << 8);
                //limit byte from 0 to 1023
                byte = Math.min(byte, 1023);
                //calculate the derivative


                if (this.listOfAnalog[i] != byte) {
                    this.listOfAnalogFlagChange[i] = true;
                    this.listOfDerivativeAnalog[i] = byte - this.listOfAnalog[i];
                    this.listOfDerivativeAnalogFlagChange[i] = true;
                    this.listOfPreviousAnalog[i] = this.listOfAnalog[i];
                    this.listOfAnalog[i] = byte;
                }


            }

            // PRINT RAW VALUE
            // if (this.listOfAnalog[0] < 240 || this.listOfAnalog[1] < 240) {

            //     console.log("" + this.listOfAnalog[0] + " : " + this.listOfAnalog[1]);
            // }
            //console.log(" Analog2: " + this.listOfAnalog[2] + " Analog3: " + this.listOfAnalog[3]);


            // this.graphUI(this.listOfAnalog[0], 0);
            // this.graphUI(this.listOfAnalog[1], 1);
            // this.graphUI(this.listOfAnalog[2], 2);
            // this.graphUI(this.listOfAnalog[3], 3);



            this.dataBuffer = Buffer.alloc(0);


        }

    }



    autoReconnect() {
        if (!this.isOpen) {
            this.restartConnection();
        }
    }

    restartConnection() {


        if (!this.isOpen) {
            this.openPort(this.port);
        } else {
            this.close();
            setTimeout(() => {
                this.openPort(this.port);
            }, 1000);
        }


    }



    // Function to read a specific bit at a given position
    readBit(buffer, byteIndex, bitIndex) {
        const byteValue = buffer.at(byteIndex);
        // Use bitwise shift and AND operation to read the bit
        return (byteValue & (1 << bitIndex)) !== 0;
    }

    swipeDetection() {
        //Here is some calculation on analog values, in order to detect if there is a swipe movement.

        // SWIPE DETECTION : PART 1 ::::: Swipe RAW VALUE
        let newSwipePosition = [0, 0];
        let newSwipeTouch = [false, false];


        for (let i = 0; i < 2; i++) {
            let newValue
            if (this.listOfAnalog[2 * i] > this.listOfAnalog[2 * i + 1]) {
                newValue = this.listOfAnalog[2 * i] * this.listOfAnalog[2 * i + 1];
            } else {
                newValue = this.listOfAnalog[2 * i] * (- this.listOfAnalog[2 * i + 1]);
            }


            newSwipePosition[i] = this.interpolate(newValue);

        }

        // SWIPE DETECTION : PART 2  ::::: manage release and touch with average
        //After this newSwipePosition is a value between 0 and 100 if touched, or 200 if not touched
        // Let's update separately the value of the swipe and the touch
        // Update the average
        // Display value on graph
        for (let i = 0; i < 2; i++) {
            //this.consoleUI("Swipe " + newSwipePosition[i].toFixed(1));
            this.listOfAverageSwipePosition[i].average;
            if (newSwipePosition[i] < 100) {

                this.listOfAverageSwipePosition[i].update(newSwipePosition[i]);
                this.graphUI(this.listOfAverageSwipePosition[i].average * 10, i);
                newSwipeTouch[i] = true;

            } else {
                this.listOfAverageSwipePosition[i].clear();
                newSwipeTouch[i] = false;
            }

            this.listOfSwipePosition[i] = newSwipePosition[i];



        }
        // SWIPE DETECTION : PART 3  ::::: process and send swipe
        //Process value to detect swipe and touch/release
        for (let i = 0; i < 2; i++) {

            //First , have a look about the touch
            if (this.listOfSwipeTouch[i] != newSwipeTouch[i]) {
                // This is change from touch to release
                this.sendTouch(newSwipeTouch[i], i, 3, 2);
                this.listOfSwipeTouch[i] = newSwipeTouch[i];
            }

            else if (newSwipeTouch[i] && this.listOfAverageSwipePosition[i].average > 0) {
                //this is already touch, we need to send the position
                // Use the "last value" of average, save into the average object
                let startValue = Math.round(this.listOfAverageSwipePosition[i].last_average);
                let endValue = Math.round(this.listOfAverageSwipePosition[i].average);

                //Let's check if there is a significant change
                let diff = endValue - startValue;


                if (diff > 5) {
                    // Here is a special rule. The capactive sensor have a side effect, if the finger is not moving ( tap and stay), the value is moving from 0 to 40%
                    if (startValue > 0 && this.listOfAverageSwipePosition[i].average > 40 && this.listOfAverageSwipePosition[i].last_average > 20) {
                        //Indeed, actual value are wrong, since 0 and 40 are the same position.
                        let startValueCorrection = (startValue - 40) / 60 * 100;
                        if (startValueCorrection < 0) {
                            startValueCorrection = 0;
                        }
                        let endValueCorrection = (endValue - 40) / 60 * 100;
                        if (endValueCorrection < 0) {
                            endValueCorrection = 0;
                        }
                        this.sendSwipe(false, i, startValue, endValue);
                        //console.log("swipe going down. start " + startValueCorrection + " end" + endValueCorrection);
                    } else {
                        //console.log("MISS swipe going down. Average " + this.listOfAverageSwipePosition[i].average + " last average " + this.listOfAverageSwipePosition[i].last_average);
                    }


                    this.listOfAverageSwipePosition[i].saveLastAverage();
                    this.consoleUI("Swipe down ");
                }

                if (diff < -5) {
                    //this.consoleUI("diff " + diff);
                    if (startValue > 0 && this.listOfAverageSwipePosition[i].average < 60 && this.listOfAverageSwipePosition[i].last_average > 0) {
                        let startValueCorrection = 100 - ((startValue - 40) / 60 * 100);
                        if (startValueCorrection < 0) {
                            startValueCorrection = 0;
                        }
                        let endValueCorrection = 100 - ((endValue - 40) / 60 * 100);
                        if (endValueCorrection < 0) {
                            startValueCorrection = 0;
                        }
                        this.sendSwipe(true, i, startValueCorrection, endValueCorrection);
                        //console.log("swipe going up. start " + startValueCorrection + " end" + endValueCorrection);
                    } else {
                        //console.log("MISS swipe going up. Average " + this.listOfAverageSwipePosition[i].average + " last average " + this.listOfAverageSwipePosition[i].last_average);
                    }
                    this.listOfAverageSwipePosition[i].saveLastAverage();
                    this.consoleUI("Swipe up ");
                }


            }
        }






        // this.graphUI(newSwipePosition[0], 0);
        // this.graphUI(newSwipePosition[1], 1);



        //Clear flag
        for (let i = 0; i < 4; i++) {
            this.listOfAnalogFlagChange[i] = false;
        }
        for (let i = 0; i < 4; i++) {
            this.listOfDerivativeAnalogFlagChange[i] = false;
        }
    }



    swipeConversionToLinear(value, pMax, pMid, nMid, nMax) {
        // This is the actual situation
        // if it is not touched, value is over 40000
        // The first linear part is between 40000 to 25000, let's say it is 100% (40000) to 66% (25000)
        // the second part is between 25000 to -25000, let's say it is 66%(25000) to 33%(-25000)
        // the third part is between -25000 to -40000, let's say it is 33%(-25000) to 0%(-40000)
        // The final value is between 0 and 100
        // let consider 40000 as a variable max


        const positiveMax = pMax
        const positiveMiddle = pMid;
        const negativeMiddle = nMid;
        const negativeMax = nMax;
        const step1 = 66;
        const step2 = 33;
        let percentage = 0;

        if (value >= positiveMax) {
            percentage = 1000;
        }
        else if (value >= positiveMiddle && value <= positiveMax) {
            // Valeur entre positiveMiddle et positiveMax
            percentage = step1 + (value - positiveMiddle) * (100 - step1) / (positiveMax - positiveMiddle);
        } else if (value >= negativeMiddle && value < positiveMiddle) {
            // Valeur entre negativeMiddle et positiveMiddle
            percentage = step2 + (value - negativeMiddle) * (step1 - step2) / (positiveMiddle - negativeMiddle);
        } else if (value >= negativeMax && value < negativeMiddle) {
            // Valeur entre negativeMax et negativeMiddle
            percentage = 0 + (value - negativeMax) * (step2 - 0) / (negativeMiddle - negativeMax);
        } else {
            percentage = 1000;
        }

        return Math.round(percentage);



    }


    touchDetection() {

        //check if there is a change in the touch
        for (let i = 0; i < 4; i++) {
            for (let j = 0; j < 3; j++) {
                if (this.listOfTouchFlagChange[i][j]) {


                    this.consoleUI("finger " + i + " phalange " + j + " is " + this.listOfTouch[i][j]);
                    console.log("touch " + i + " " + j + " " + this.listOfTouch[i][j]);
                    this.sendTouch(this.listOfTouch[i][j], i, j);

                }
            }
        }

        // CLEAR ALL FLAG
        //at the end of the function, clear all flag
        for (let i = 0; i < 4; i++) {
            for (let j = 0; j < 3; j++) {
                this.listOfTouchFlagChange[i][j] = false;
            }
        }


    }

    sendTouch(isTouching, ifinger, iphalanxsegment, iphalanxside) {

        let rde = new RawDataEvent();

        if (isTouching) {
            rde.context = [rde.CONTEXT.AIR, rde.CONTEXT.CONTACT];
            rde.action = rde.ACTION.FLEXION
        } else {
            rde.context = [rde.CONTEXT.CONTACT, rde.CONTEXT.AIR];
            rde.action = rde.ACTION.EXTENSION
        }

        //finger and phalanx are number here. convert them to string
        switch (ifinger) {
            case 0:
                ifinger = rde.FINGER.INDEX;
                break;
            case 1:
                ifinger = rde.FINGER.MIDDLE;
                break;
            case 2:
                ifinger = rde.FINGER.RING;
                break;
            case 3:
                ifinger = rde.FINGER.PINKY;
                break;
        }

        switch (iphalanxside) {
            case 0:
                iphalanxside = rde.PHALANX.SIDE.ANY;
                break;
            case 1:
                iphalanxside = rde.PHALANX.SIDE.LEFT;
                break;
            case 2:
                iphalanxside = rde.PHALANX.SIDE.RIGHT;
                break;
        }


        switch (iphalanxsegment) {
            case 0:
                iphalanxsegment = { segment: rde.PHALANX.SEGMENT.TIP, side: iphalanxside };
                break;
            case 1:
                iphalanxsegment = { segment: rde.PHALANX.SEGMENT.MIDDLE, side: iphalanxside };
                break;
            case 2:
                iphalanxsegment = { segment: rde.PHALANX.SEGMENT.BASE, side: iphalanxside };
                break;
            case 3:
                iphalanxsegment = { segment: rde.PHALANX.SEGMENT.ANY, side: iphalanxside };
                break;
        }

        rde.actuator = [[rde.FINGER.THUMB]]
        rde.contact.action = rde.CONTACT.ACTION.FINGER
        rde.contact.actuator = [[ifinger]]
        rde.contact.phalanx = [[iphalanxsegment]]


        this.emit(rde);
    }

    sendSwipe(isGoingUp, ifinger, startvalue, endvalue) {

        let rde = new RawDataEvent();
        rde.context = [rde.CONTEXT.CONTACT, rde.CONTEXT.CONTACT];
        rde.action = isGoingUp ? rde.ACTION.EXTENSION : rde.ACTION.FLEXION;


        //finger and phalanx are number here. convert them to string
        switch (ifinger) {
            case 0:
                ifinger = rde.FINGER.INDEX;
                break;
            case 1:
                ifinger = rde.FINGER.MIDDLE;
                break;
            case 2:
                ifinger = rde.FINGER.RING;
                break;
            case 3:
                ifinger = rde.FINGER.PINKY;
                break;
        }


        let iphalanxsegment = { segment: rde.PHALANX.SEGMENT.ANY, side: rde.PHALANX.SIDE.LEFT };

        rde.actuator = [[rde.FINGER.THUMB]]
        rde.contact.action = rde.CONTACT.ACTION.FINGER
        rde.contact.actuator = [[ifinger]]
        rde.contact.phalanx = [[iphalanxsegment]]
        rde.parameters.amplitude.start = Math.round(startvalue);
        rde.parameters.amplitude.end = Math.round(endvalue);

        this.emit(rde);
    }

    sendDirection(direction) {
        let v = direction == 1 ? -50 : 50;
        let rde = new RawDataEvent();
        rde.context = [rde.CONTEXT.AIR, rde.CONTEXT.AIR];
        rde.parameters.amplitude
        this.emit(rde);
    }

    sendDirectionInTheAir(newVal, prevVal) {
        let direction = newVal - prevVal
        let rde = new RawDataEvent();

        if (direction > 0) {
            rde.action = rde.ACTION.FLEXION;
        } else {
            rde.action = rde.ACTION.EXTENSION;
        }

        rde.context = [rde.CONTEXT.AIR, rde.CONTEXT.AIR]
        rde.actuator = [[rde.FINGER.INDEX]]


        rde.parameters.amplitude = {
            start: prevVal,
            end: newVal
        }





        this.emit(rde);
    }

    //override close method
    close() {
        // Close the HID device
        if (this.device) {
            // When you are done using the serial port, you can close it like this:
            if (this.device.isOpen) {

                this.device.close((err) => {
                    if (err) {
                        console.error('Error closing the serial port:', err);
                    } else {
                        console.log('Serial port closed successfully.');
                    }
                });
            }
            //clear this.device as null
            this.device = null;
        }
    }

    //Interpolation lineaire des capteurs capacitifs
    initInterpolation() {
        this.interpolate1 = [

            { output: 0.03, input: 52243 },
            { output: 0.04, input: 43723 },
            { output: 0.05, input: 41382 },
            { output: 0.06, input: 40052 },
            { output: 0.07, input: 39493 },
            { output: 0.08, input: 38729 },
            { output: 0.09, input: 38616 },
            { output: 0.10, input: 38593 },
            { output: 0.11, input: 37665 },
            { output: 0.12, input: 36974 },
            { output: 0.13, input: 35788 },
            { output: 0.14, input: 33671 },
            { output: 0.15, input: 31985 },
            { output: 0.16, input: 32364 },
            { output: 0.17, input: 30576 },
            { output: 0.18, input: 30758 },
            { output: 0.19, input: 29677 },
            { output: 0.20, input: 29852 },
            { output: 0.21, input: 28717 },
            { output: 0.22, input: 28045 },
            { output: 0.23, input: 26620 },
            { output: 0.24, input: 26161 },
            { output: 0.25, input: 25679 },
            { output: 0.26, input: 26414 },
            { output: 0.27, input: 24609 },
            { output: 0.28, input: 25291 },
            { output: 0.29, input: 24993 },
            { output: 0.30, input: 24642 },
            { output: 0.31, input: 23443 },
            { output: 0.32, input: 22918 },
            { output: 0.33, input: 22584 },
            { output: 0.34, input: 22496 },
            { output: 0.35, input: 22525 },
            { output: 0.36, input: 22871 },
            { output: 0.37, input: 21738 },
            { output: 0.38, input: 21993 },
            { output: 0.39, input: 21687 },
            { output: 0.40, input: 21046 },
            { output: 0.41, input: 20549 },
            { output: 0.42, input: 9860 },
            { output: 0.43, input: 9501 },
            { output: 0.44, input: 8904 },
            { output: 0.45, input: 8804 },
            { output: 0.46, input: 8345 },
            { output: 0.47, input: -3396 },
            { output: 0.48, input: -3392 },
            { output: 0.49, input: -3617 },
            { output: 0.50, input: -3372 },
            { output: 0.51, input: -4035 },
            { output: 0.52, input: -4157 },
            { output: 0.53, input: -4157 },
            { output: 0.54, input: -3961 },
            { output: 0.55, input: -4763 },
            { output: 0.56, input: -5557 },
            { output: 0.57, input: -5271 },
            { output: 0.58, input: -7704 },
            { output: 0.59, input: -11762 },
            { output: 0.60, input: -15628 },
            { output: 0.61, input: -19149 },
            { output: 0.62, input: -19280 },
            { output: 0.63, input: -19647 },
            { output: 0.64, input: -20453 },
            { output: 0.65, input: -20501 },
            { output: 0.66, input: -21041 },
            { output: 0.67, input: -21012 },
            { output: 0.68, input: -21285 },
            { output: 0.69, input: -21470 },
            { output: 0.70, input: -21123 },
            { output: 0.71, input: -22181 },
            { output: 0.72, input: -22832 },
            { output: 0.73, input: -23344 },
            { output: 0.74, input: -23114 },
            { output: 0.75, input: -23323 },
            { output: 0.76, input: -23019 },
            { output: 0.77, input: -24221 },
            { output: 0.78, input: -24986 },
            { output: 0.79, input: -25280 },
            { output: 0.80, input: -25828 },
            { output: 0.81, input: -26138 },
            { output: 0.82, input: -25555 },
            { output: 0.83, input: -26308 },
            { output: 0.84, input: -28846 },
            { output: 0.85, input: -29521 },
            { output: 0.86, input: -30168 },
            { output: 0.87, input: -31012 },
            { output: 0.88, input: -31538 },
            { output: 0.89, input: -32222 },
            { output: 0.90, input: -33440 },
            { output: 0.91, input: -33781 },
            { output: 0.92, input: -34680 },
            { output: 0.93, input: -37579 },
            { output: 0.94, input: -40800 },
            { output: 0.95, input: -43579 },
            { output: 0.96, input: -45373 },
            { output: 0.97, input: -47772 },
            { output: 0.98, input: -50506 },
        ];

    }

    // Fonction pour trouver les valeurs d'interpolation
    interpolate(x) {
        for (let i = 0; i < this.interpolate1.length - 1; i++) {
            const x0 = this.interpolate1[i].input;
            const y0 = this.interpolate1[i].output;
            const x1 = this.interpolate1[i + 1].input;
            const y1 = this.interpolate1[i + 1].output;

            if (x >= x1 && x <= x0) {
                // Interpolation linéaire
                const y = y0 + ((y1 - y0) * (x - x0)) / (x1 - x0);
                return Math.round(y * 100);
            }
        }
        // Si x est en dehors de la plage des données, retourne null ou une valeur par défaut
        return 200;
    }




}

module.exports = NanoEsp32;
