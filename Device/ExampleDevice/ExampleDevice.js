const Device = require("../Device.js");
const RawDataEvent = require("../../RawDataEvent.js");

class ExampleDevice extends Device {
  //add a constructor with em argument
  constructor(index, am) {
    //call the constructor of the parent class
    super(index, "Example Device", am);
    this.description =
      "This is an example device, that generate random data, from perlin noise.";

    // Here is supposed to connect to the device, according to its protocol
    /* 
    serialport.conect(), OSC.connect(), HID.connect(), bluetooth.connect(), etc..
    */
    this.generator1 = new Simple1DNoise();
    this.generator2 = new Simple1DNoise();
    this.index = 0;

    //Simulating a device that send data every 500ms
    setInterval(() => {
      //create x, correspond to the time in milliseconds
      let x = Date.now();
      //create y, correspond to the random value
      let y1 = this.generator1.getVal(x) * 1000;
      //let y2 = this.generator2.getVal(x) * 1000;
      //create y2 that is a sinus function over time, period of 2 seconds
      let y2 = Math.sin((x / 5000) * 2 * Math.PI) * 500 + 500;
      y1 = Math.round(y1);
      y2 = Math.round(y2);

      // You don't have to emit raw data event every on every data you receive, you can filter it.
      if (this.index % 10 == 0) {
        this.emit("data", y1);
      }
      this.index++;

      //for debug purpose, you can send the data to UI
      this.graphUI(y1, 0);
      this.graphUI(y2, 1);

      //for debug purpose, you can send the data to console
      // send to console, if randome number is > 0.8
      if (Math.random() > 0.8) {
        this.consoleUI("random device sent an :" + String(y1));
      }
    }, 250);
  }

  //override emit method : this is mandatory
  emit(event, data) {
    //create a RawDataEvent
    let rde = new RawDataEvent();
    rde.deviceId = "THIS IS AN EXAMPLE DEVICE";
    rde.action = rde.ACTION.FLEXION;
    rde.context = [rde.CONTEXT.CONTACT];
    rde.actuator = [[rde.FINGER.THUMB]];
    rde.contact.action = "Contact";
    rde.contact.actuator = [[rde.FINGER.INDEX]];
    rde.contact.parameters = { pressure: data };

    //send it to the analyzer manager
    this.am.emit(rde);
    this.rawDataEventUI(rde);
  }

  close() {
    //delete generator1 and generator2
    delete this.generator1;
    delete this.generator2;
  }

  restartConnection() {
    this.close();
    this.generator1 = new Simple1DNoise();
    this.generator2 = new Simple1DNoise();
  }
}

var Simple1DNoise = function () {
  var MAX_VERTICES = 1024;
  var MAX_VERTICES_MASK = MAX_VERTICES - 1;
  var amplitude = 1;
  var scale = 1;

  var r = [];

  for (var i = 0; i < MAX_VERTICES; ++i) {
    r.push(Math.random());
  }

  var getVal = function (x) {
    var scaledX = x * scale;
    var xFloor = Math.floor(scaledX);
    var t = scaledX - xFloor;
    var tRemapSmoothstep = t * t * (3 - 2 * t);

    /// Modulo using &#038;
    var xMin = xFloor % MAX_VERTICES_MASK;
    var xMax = (xMin + 1) % MAX_VERTICES_MASK;

    var y = lerp(r[xMin], r[xMax], tRemapSmoothstep);

    return y * amplitude;
  };

  /**
   * Linear interpolation function.
   * @param a The lower integer value
   * @param b The upper integer value
   * @param t The value between the two
   * @returns {number}
   */
  var lerp = function (a, b, t) {
    return a * (1 - t) + b * t;
  };

  // return the API
  return {
    getVal: getVal,
    setAmplitude: function (newAmplitude) {
      amplitude = newAmplitude;
    },
    setScale: function (newScale) {
      scale = newScale;
    },
  };
};

module.exports = ExampleDevice;
