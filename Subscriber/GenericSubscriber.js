

const Subscriber = require('./Subscriber');
const { Server } = require("socket.io");

class GenericSubscriber extends Subscriber {

    //add a static string field to the class
    static type = "generic";

    constructor(name, index, config) {

        //call the constructor of the parent class
        super(name, index, GenericSubscriber.type);
        /*
        config = {
            port: 3000,
            subscribeToAll: false
            desiredGesture: ["gesture1", "gesture2"]
        }
        */


        this.config = config; // this has to stay as this.config, because we need to export the config to the UI
        this.socket = null;
        this.io = null;
        this.isConnected = false;



    }

    start() {

        //check is this.port is not null and > 0
        if (this.config.port != null && this.config.port > 0) {
            console.log("External APP : start on port " + this.config.port);
            this.io = new Server(this.config.port, {
                cors: {
                    origin: "http://localhost",
                    methods: ["GET", "POST"],
                    allowedHeaders: ["Content-Type", "Authorization", "X-Requested-With", "Cookie"],
                    credentials: true

                }
            });  //listen to connection event
            this.io.on("connection", (socket) => {
                console.log("External APP : a user connected");
                //listen to disconnection event
                this.socket = socket;
                this.setActive(true);

                this.socket.on("disconnect", () => {
                    console.log("External APP : user disconnected");
                    this.setActive(false);
                });
            });

        }


    }



    edit(name, config) {
        this.name = name;
        let ifNetworkChanged = this.config.port != config.port;
        this.config = config;
        if (ifNetworkChanged) {
            this.close();
            this.start();
        }
    }

    onGestureEvent(gestureEvent) {
        //console log the name of the app + the gestureEvent
        console.log(this.name + " received :  " + gestureEvent.name + "  toAll? " + this.config.subscribeToAll);
        //check is socket is not null

        if (this.config.subscribeToAll || this.config.desiredGesture?.includes(gestureEvent.name)) {

            if (this.socket != null) {

                //check if gestureEvent.name is in the desiredGestureList
                //if yes, send the gestureEvent.name to the socket
                this.socket.emit("microgesture", gestureEvent);
                //this.socket.emit("microgeste");

            }
            //update the UI even if the socket is null
            this.emitUI(gestureEvent);

        }
        else {
            console.log("External APP : socket is null");
        }
    }

    close() {
        // close the socket
        console.log("SUBSCRIBER : want to close");
        if (this.socket != null) {
            this.socket.disconnect(); // Close the socket connection using the disconnect() method.
            console.log("SUBSCRIBER : socket closed");
        }
        if (this.io != null && this.io != undefined) {
            this.io.close();
        }
    }

}

module.exports = GenericSubscriber;