/*
Keyboard use robot.js to simulate keyboard event ( and also mouse event)
https://www.npmjs.com/package/robotjs
Special doc for electron
https://github.com/octalmage/robotjs/wiki/Electron


*/


const Subscriber = require('./Subscriber');
const robot = require('@jitsi/robotjs');


class KeyboardSubscriber extends Subscriber {

    //add a static string field to the class
    static type = "keyboard";

    constructor(name, index, config) {

        //call the constructor of the parent class

        super(name, index, KeyboardSubscriber.type);
        this.config = config; // this has to stay as this.config, because we need to export the config to the UI

        if (this.config.desiredGesture != undefined) {


            //check that config a desiredGestureList key, check that it is an array, check that it is not empty
            if (this.config.desiredGesture == undefined || !Array.isArray(this.config.desiredGesture) || this.config.desiredGesture.length == 0) {
                console.log("KeyboardSubscriber : desiredGestureList not found or not valid");
            }

            //check that config a keyList key, check that it is an array, check that it is not empty
            if (this.config.desiredKey == undefined || !Array.isArray(this.config.desiredKey) || this.config.desiredKey.length == 0) {
                console.log("KeyboardSubscriber : keyList not found or not valid");
            }

            //check that desiredGestureList and keyList have the same length
            if (this.config.desiredGesture.length != this.config.desiredKey.length) {
                console.log("KeyboardSubscriber : desiredGestureList and keyList have different length");
            }

        } else {
            this.config.desiredGesture = ["", "", "", "", "", ""];
            this.config.desiredKey = ["audio_vol_down", "audio_vol_up", "up", "down", "audio_play", "audio_pause"];
        }






    }

    start() {
        console.log("KeyboardSubscriber : start");
    }

    onGestureEvent(gestureEvent) {
        //console log the name of the app + the gestureEvent
        //console.log(this.name + " received :  " + gestureEvent.name);

        //check if gestureEvent.name is in the desiredGestureList and return the index
        let index = this.config.desiredGesture.indexOf(gestureEvent.name);
        //console.log("index : " + index);
        //if index is not -1, then we found the gestureEvent.name in the desiredGestureList
        if (index != -1) {
            //check if the keyList[index] is not empty
            if (this.config.desiredKey[index] != "") {
                //console.log("keyboard : " + this.config.desiredKey[index]);
                // Check of formula contain a modifier key , ex : ctrl+a, ctrl+shift+a
                //modifiers can be : Accepts alt, command (win), control, and shift.
                let modifiers = [];
                let key = "";
                let keyDivide = this.config.desiredKey[index].split("+");
                console.log("keyDivide : " + keyDivide);
                if (keyDivide.length > 1) {
                    //only last element is the key
                    //all other elements are modifiers, listed in array, modifier
                    key = keyDivide[keyDivide.length - 1];
                    modifiers = keyDivide.slice(0, keyDivide.length - 1);
                    //double check that modifiers contain only ctrl, alt, shift, command
                    modifiers.forEach((modifier) => {
                        if (modifier != "control" && modifier != "alt" && modifier != "shift" && modifier != "command") {
                            console.log("keyboardSubscriber error : modifier not valid");
                            modifiers = [];
                        } else {
                            console.log("keyboardSubscriber : modifier : " + modifier);
                        }
                    });

                    robot.keyTap(key, modifiers);


                } else {
                    key = this.config.desiredKey[index];
                    console.log("keyboard tap : " + key);
                    // divide key into array if it con
                    robot.keyTap(key);
                }

                this.emitUI(gestureEvent)

            }

        }
    }

    edit(name, config) {
        this.name = name;
        this.config = config;
        if (this.config.desiredGesture == undefined || !Array.isArray(this.config.desiredGesture) || this.config.desiredGesture.length == 0) {
            console.log("KeyboardSubscriber : desiredGestureList not found or not valid");
        }

        if (this.config.desiredKey == undefined || !Array.isArray(this.config.desiredKey) || this.config.desiredKey.length == 0) {
            console.log("KeyboardSubscriber : keyList not found or not valid");
        }

        if (this.config.desiredGesture.length != this.config.desiredKey.length) {
            console.log("KeyboardSubscriber : desiredGestureList and keyList have different length");
        }

    }





    close() {
        // close the socket
        console.log("KEYBOARD SUBSCRIBER : want to close");

    }

}

module.exports = KeyboardSubscriber;