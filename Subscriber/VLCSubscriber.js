/*


Use of VLC client :

https://www.npmjs.com/package/vlc-client

Probably need to activate server in vlc, it not activated by default

How to change default port. Settings, inputs, codecs , network parameter. 



.play()
Resume the playback if paused

.pause()
Pause the playback

.togglePlay()
Pauses if playing, and resumes the playback if paused

.stop()
Stops the playback

.next()
Play the next media in playlist

.previous()
Play the previous media in playlist

.setTime(time: number)
Set the time of playback. Time arg should be an int.

.setProgress(progress: number)
Set progress of media playback 0-100 range. Progress can be a number with decimals.

.setVolume(volume: number)
Set the volume range 0-100.

.setVolumeRaw()
Set volume as VLC represents it 0-512

.setFullscreen(val: boolean)
Set if VLC should be fullscreen


*/


const Subscriber = require('./Subscriber');
const VLC = require("vlc-client");


class VLCSubscriber extends Subscriber {

    //add a static string field to the class
    static type = "vlc";

    constructor(name, index, config) {

        //call the constructor of the parent class

        super(name, index, VLCSubscriber.type);
        this.config = config; // this has to stay as this.config, because we need to export the config to the UI

        this.vlc = new VLC.Client({
            ip: "localhost",
            port: 9090,
            username: "", //username is optional
            password: "upoly"
        });

        if (this.config.desiredGesture != undefined) {



            //check that config a desiredGestureList key, check that it is an array, check that it is not empty
            if (this.config.desiredGesture == undefined || !Array.isArray(this.config.desiredGesture) || this.config.desiredGesture.length == 0) {
                console.log("KeyboardSubscriber : desiredGestureList not found or not valid");
            }

            //check that config a keyList key, check that it is an array, check that it is not empty
            if (this.config.desiredAction == undefined || !Array.isArray(this.config.desiredAction) || this.config.desiredAction.length == 0) {
                console.log("KeyboardSubscriber : keyList not found or not valid");
            }

            //check that desiredGestureList and keyList have the same length
            if (this.config.desiredGesture.length != this.config.desiredAction.length) {
                console.log("KeyboardSubscriber : desiredGestureList and ActionList have different length");
            }

        } else {
            this.config.desiredGesture = ["TAP", "DOUBLE_TAP"];
            this.config.desiredAction = ["play", "pause"];
        }



    }

    start() {
        console.log("VLC Subscriber : start");
    }

    onGestureEvent(gestureEvent) {
        //console log the name of the app + the gestureEvent
        //console.log(this.name + " received :  " + gestureEvent.name);

        //check if gestureEvent.name is in the desiredGestureList and return the index
        let index = this.config.desiredGesture.indexOf(gestureEvent.name);
        //console.log("index : " + index);
        //if index is not -1, then we found the gestureEvent.name in the desiredGestureList
        if (index != -1) {
            //check if the keyList[index] is not empty
            let action = this.config.desiredAction[index];
            console.log("VLC SUBSCRIBER : action : " + action);

            switch (action) {
                case "play":
                    this.vlc.play();
                    break;
                case "pause":
                    this.vlc.pause();
                    break;
                case "stop":
                    this.vlc.stop();
                    break;
                case "next":
                    this.vlc.next();
                    break;
                case "previous":
                    this.vlc.previous();
                    break;
                case "fullscreen":
                    this.vlc.setFullscreen(true);
                    break;
                case "volumeUp":
                    this.vlc.setVolume(100);
                    break;
                case "volumeDown":
                    this.vlc.setVolume(0);
                    break;
                default:
                    console.log("VLC SUBSCRIBER : action not found");
                    break;

            }



            this.emitUI(gestureEvent)


        }




    }

    edit(name, config) {
        this.name = name;
        this.config = config;
        if (this.config.desiredGesture == undefined || !Array.isArray(this.config.desiredGesture) || this.config.desiredGesture.length == 0) {
            console.log("KeyboardSubscriber : desiredGestureList not found or not valid");
        }

        if (this.config.desiredAction == undefined || !Array.isArray(this.config.desiredAction) || this.config.desiredAction.length == 0) {
            console.log("KeyboardSubscriber : keyList not found or not valid");
        }

        if (this.config.desiredGesture.length != this.config.desiredAction.length) {
            console.log("KeyboardSubscriber : desiredGestureList and keyList have different length");
        }


    }



    close() {
        // close the socket
        console.log("VLC SUBSCRIBER : want to close");

    }

}

module.exports = VLCSubscriber;