/*

This is a virtual interface,  like device.js
This class CAN NOT be instanciated. 
The default implementation is GenericSubscriber.js which is a websocket server.

{
        name: name,
        index: index,
        type: type,
        config: config,
      }

config is specific to each type of subscriber.
*/

const Controller = require("../Controller/Controller.js");


class Subscriber {
  constructor(name, index, type) {
    this.name = name;
    this.index = index;
    this.type = type;
    this.active = false;
    this.controller = Controller.getInstance();


  }

  onGestureEvent(gestureEvent) {
    // empty method because it's an interface
    //throw an error if it's not overriden
    throw new Error("The 'onGestureEvent' method must be overridden in the child class.");
  }

  start() {

    throw new Error("The 'start' method must be overridden in the child class.");
  }

  close() {
    // empty method because it's an interface
    //throw an error if it's not overriden
    throw new Error("The 'close' method must be overridden in the child class.");

  }

  edit(name, config) {
    // empty method because it's an interface
    //throw an error if it's not overriden
    throw new Error("The 'edit' method must be overridden in the child class.");

  }

  setActive(isActive) {
    this.active = isActive;
    this.controller.updateSingleSubscriberConnection(this.index, this.active);
  }

  emitUI(event) {

    // notify UI that the event is emitted
    this.controller.updateSpecificSubscriberOnGesture(this.index, event);

  }

}

module.exports = Subscriber;
