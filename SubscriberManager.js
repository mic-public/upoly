/*

This is a generic subscriber. Config is specific to each type of subscriber.
{
        name: name,
        index: index,
        type: type,
        config: config,
      }


*/


const Subscriber = require("./Subscriber/Subscriber.js");
const GenericSubscriber = require("./Subscriber/GenericSubscriber.js");
const KeyboardSubscriber = require("./Subscriber/KeyboardSubscriber.js");
const VLCSubscriber = require("./Subscriber/VLCSubscriber.js");
const GestureEvent = require("./GestureEvent.js");
const Controller = require("./Controller/Controller.js");


class GestureSubscriberManager {
  constructor() {
    this.gestureSubscriberList = [];
    this.availableGestureList = [];
    this.controller = Controller.getInstance();
  }

  addSubscriber(name, type, config) {
    //create a new GestureSubscriber and add it to the list

    //TODO let check that desiredGestureList is ok with the actual list of analyzers.
    //Problem ? maybe the list is not up to date, because we cant access to analyzers directly from here.
    let index = this.gestureSubscriberList.length;
    let newSubscriber = undefined;
    switch (type.toLowerCase()) {
      case GenericSubscriber.type:
        newSubscriber = new GenericSubscriber(name, index, config);
        //if config is empty object
        if (!config.desiredGesture) {
          newSubscriber.config.desiredGesture = this.availableGestureList;
        }
        break;
      case KeyboardSubscriber.type:
        newSubscriber = new KeyboardSubscriber(name, index, config);
        break;
      case VLCSubscriber.type:
        newSubscriber = new VLCSubscriber(name, index, config);
        break;

      default:
        console.log("GestureSubscriberManager : type not found");
        break;
    }

    if (newSubscriber != undefined) {
      this.gestureSubscriberList.push(newSubscriber);
      newSubscriber.start();
    }


  }

  editSubscriber(index, name, config) {
    //edit the subscriber at the index with the new data
    this.gestureSubscriberList[index].edit(name, config);

  }

  deleteSubscriber(i) {

    //check if index is in the range of the array
    if (i >= 0 && i < this.gestureSubscriberList.length) {
      //delete the device from the array
      this.gestureSubscriberList[i].close();
      this.gestureSubscriberList.splice(i, 1);

    }

    //update the index of the devices after removing one
    this.gestureSubscriberList.forEach((sub, newIndex) => {
      sub.index = newIndex;
    });

  }

  deleteAll() {
    //delete all subscribers
    //console.log("delete all subscribers");
    for (let i = 0; i < this.gestureSubscriberList.length; i++) {
      this.deleteSubscriber(i);
    }
  }


  configFromFile(configFile) {
    //configFile is a json file, read the key called "devices"
    //for each device in the list, call addDevice with the name of the device and the name of the device

    //1st check the "devices" key exist
    if (configFile.hasOwnProperty("subscribers")) {
      //2nd check if the "devices" key is an array
      if (Array.isArray(configFile["subscribers"])) {
        this.deleteAll();
        //3rd for each device in the array, call addDevice with the name of the device and the name of the device
        configFile["subscribers"].forEach((sub) => {
          this.addSubscriber(sub["name"], sub["type"], sub["config"]);
          //console log this subscriber
          //console.log(`subscriber manager : client added ${sub}`);
        });
      }
    }

    //console.log("GestureSubscriber configFromFile done");
    //console.log(this.gestureSubscriberList);
  }

  exportConfig() {
    //exact opposite of configFromFile, return a javascript object with the same structure as the config file.

    let subscribers = [];
    this.gestureSubscriberList.forEach((sub) => {
      subscribers.push({
        name: sub.name,
        index: sub.index,
        type: sub.type,
        config: sub.config
      });
    });

    return subscribers;
  }

  emit(gestureEvent) {
    //Console log : GestureEventManager : receive gesture event
    console.log("GestureEventManager : receive gesture event : " + gestureEvent.name);
    //console.log(gestureEvent);
    //first check if the gestureEvent is an instance of GestureEvent

    if (gestureEvent instanceof GestureEvent) {
      //console.log("GestureEventManager : emitting gesture event OK");
      this.controller.notifyOnGestureEmit(gestureEvent); //update the ui
      for (let sub of this.gestureSubscriberList) {
        // send event to all subscribers. Each subscriber will check if it is concerned by this event.
        console.log("GestureEventManager : app found : " + sub.name);
        sub.onGestureEvent(gestureEvent);
        //this.controller.updateSpecificSubscriberOnGesture(sub.index, gestureEvent)
      }
    } else {
      throw new Error("Only GestureEvent instances can be emitted");
    }
  }


  updateGestureList(list) {
    //copy list into gestureList
    this.availableGestureList = [...list];
    this.controller.updateGestureListUI(this.availableGestureList);
  }





}

module.exports = GestureSubscriberManager;
