class GestureEvent {
  //add constructor with all arguments without timestamp
  constructor(type, name, index, deviceId, raw) {
    this.type = type;
    this.name = name;
    this.analyzerIndex = index;
    this.data = 0;
    this.deviceId = deviceId;
    this.timestamp = Date.now();
    this.raw = raw;
  }

  //add a method to convert the GestureEvent to a JSON string
  // toJSON() {
  //   return JSON.stringify(this);
  // }
}

module.exports = GestureEvent;
